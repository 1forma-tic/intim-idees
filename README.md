# [Version **test** du site prête à publier (non référencée)](https://1forma-tic.frama.io/intim-idees/) [![Pipeline](https://framagit.org/1forma-tic/intim-idees/badges/master/pipeline.svg)](https://framagit.org/1forma-tic/intim-idees/pipelines)

Pour publier :
1. vérifie que les changements s'affichent bien sans casser le reste du site dans la version `test`, puis :
2. sur la page [Pipeline](https://framagit.org/1forma-tic/intim-idees/pipelines),
   première ligne, clic sur `≫` dans la série `✓ ✓ ≫ ✓` puis à droite de `OK_publish` sur `▶` et patiente quelques minutes.
3. vérifie que tout va bien en production, si ce n'est pas le cas, re-publie une ancienne version et contact-moi si besoin.

S'il y a une `✕` dans la ligne, contacte moi (à moins que tu te doute de pourquoi et que tu puisse corriger toi même).

# Version du site **en production** : [intim-idees.fr](https://intim-idees.fr/)

