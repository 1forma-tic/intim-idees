const fs = require("fs");
const yaml = require('js-yaml');
const md = require('markdown-it')({
  html:         true,        // Enable HTML tags in source
  xhtmlOut:     true,        // Use '/' to close single tags (<br />).
  breaks:       false,       // Convert '\n' in paragraphs into <br>
  linkify:      true,        // Autoconvert URL-like text to links
  typographer:  true,
}).use( require("markdown-it-anchor"));
const ejs = require('ejs');


const commonConfig = JSON.parse(fs.readFileSync('generated/config.json', 'utf8'));
commonConfig.now = (new Date).toISOString();
const langConfigs = {};

fs.readdirSync(`content/pages/`).forEach((f)=>buildPage(`content/pages/${f}`));
// build sitemap
const rawSitemap = fs.readFileSync(`content/sitemap.xml.ejs`, 'utf8');
const sitemapContent = ejs.render(rawSitemap,commonConfig,{filename:`content/sitemap.xml`});
fs.writeFileSync(`generated/toPublish/sitemap.xml`,sitemapContent,"utf8");

function buildPage(filePath) {
  const rawFile = fs.readFileSync(filePath, 'utf8');
  const splitFile = rawFile.replace(/(\r\n|\r|\n)\s*---+\s*(\r\n|\r|\n)/,'---***---').split('---***---');
  let specificOpt = {};
  if(splitFile.length===2) specificOpt = yaml.load(splitFile.shift());
  let fileContent = splitFile.shift();
  const opt = merge(commonConfig,specificOpt);
  opt.fileExt = filePath.split('.').pop();
  opt.fileName = filePath.split('/').pop().split('.').shift();

  if(opt.fileExt==='md') fileContent = `<section>${md.render(ejs.render(fileContent,opt,{filename:filePath}),opt)}</section>`;
  if(opt.fileExt==='ejs') fileContent = ejs.render(fileContent,opt,{filename:filePath});

  const templatePath = `content/${opt.template}.ejs`;
  const template = fs.readFileSync(templatePath, 'utf8');
  fileContent = template.split('{{{mainContent}}}').join(fileContent);
  fileContent = fileContent.replace(/{{([^}]+)}}/g,(match,keyword)=>opt[keyword]);

  fileContent = ejs.render(fileContent,opt,{filename:templatePath});

  let outputPath = filePath.split(`/`).pop();
  outputPath = `generated/toPublish/${outputPath}`;
  outputPath = fileNameExt2Html(outputPath,opt.fileExt);
  fileContent = fileNameExt2Html(fileContent,opt.fileExt);

  fs.writeFileSync(outputPath,fileContent,"utf8");

  // prepare for sitemap
  if(!commonConfig.pages) commonConfig.pages = [];
  commonConfig.pages.push({
    fileName:outputPath.split(`/`).pop()
  });
}

function fileNameExt2Html(str,orignalFileExt){
  return str.replace(new RegExp(`\\.${orignalFileExt}`,'g'),'.html');
}

function clone(json) {
  return JSON.parse(JSON.stringify(json))
}

function merge(baseJson, overwritingJson) {
  if(typeof baseJson !== "object") return clone(overwritingJson);
  let res = clone(baseJson);
  for (let key in overwritingJson) {
    if (typeof overwritingJson[key] === "object" && typeof res[key] !== 'undefined') {
      res[key] = merge(res[key], overwritingJson[key]);
    }
    else res[key] = overwritingJson[key];
  }
  return res;
}
