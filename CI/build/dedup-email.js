const fs = require("fs");
const emailFile = "emails.txt";
const emails = fs.readFileSync(emailFile, 'utf8').split('\n');
const dedup = {};
const dup = {};
emails.forEach(l=>l.replace(/([^ |]+@[^ |]+\.[^ |]+)/,email=>{
  email = email.toLowerCase();
  if(dedup[email] && dup[email]) dup[email]++;
  else if(dedup[email]) dup[email]=1;
  dedup[email]=l
}));
const sortedEmails = Object.keys(dedup).sort();
const sortedDedup = [];
sortedEmails.forEach(e=>sortedDedup.push(dedup[e]));
fs.writeFileSync(emailFile,sortedDedup.join('\n'));
console.log(dedup, Object.keys(dedup).length, dup, Object.keys(dup).length);
