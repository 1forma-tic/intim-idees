const fs = require("fs");
const yaml = require('js-yaml');

const commonConfig = yaml.load(fs.readFileSync('content/config.yml', 'utf8'));
commonConfig.now = (new Date).toISOString();
fs.writeFileSync('generated/config.json', JSON.stringify(commonConfig), 'utf8');
