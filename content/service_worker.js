/**
 * html from network, fallback on cache
 * assets only from cache
  */

const CACHE = 'opti';

self.addEventListener('install', function (event) {
  console.log('Installation du service worker swHashVersion (accélération du site et consultation hors ligne).');
  event.waitUntil(async ()=>{
    let newEntry =[
      {build:fileToCache}
    ];
    const toCache = array2hashMap(newEntry);

    const cache = await caches.open(CACHE);
    const oldCacheUrls = await cache.keys();
    if(oldCacheUrls) {
      oldCacheUrls.filter(req => !toCache(req.url)).forEach(req => cache.delete(req));
      oldCacheUrls.filter(req => toCache(req.url)).forEach(req => delete toCache[req.url]);
      toCache['full_site.html']='full_site.html';
      newEntry = Object.keys(toCache)
    }
    await cache.addAll(newEntry);
  });
});
self.addEventListener('installed', function (event) {
  self.skipWaiting();
});
function array2hashMap(tab){
  const hashMap = {};
  tab.forEach(v=>hashMap[v]=v);
  return hashMap;
}
self.addEventListener('activate', function () {
  console.log('Service worker swHashVersion opérationnel.');
  return self.clients.claim();
});
self.addEventListener('fetch', event => {
  const url = event.request.url+'';
  event.respondWith(async function () {
    if(url.slice(-5)==='.html') return await network2cache('full_site.html');

    const cacheRes = await caches.match(url);
    if (cacheRes) return cacheRes;
    else return await network2cache(url);
  }());
});
async function network2cache(url){
  const res = await fetch(url);
  if(!res) console.log('not res');
  if(res && res.status !== 200) console.log('res status : ',res.status);
  if(res && res.status === 200){
    const store = await caches.open(CACHE);
    await store.put(url, res);
  }
  return caches.match(url);
}
