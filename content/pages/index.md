title: intim'idées
-------------------

![frontpage](img/intim-idees-banniere-noir.png)

**Intim’idées est un collectif bordelais qui a pour objectif de s’entre-aider à s’aimer (soi et les autres)
en prenant le soin d’offrir la sécurité nécessaire à l’exploration libre des possibles.**

Nous prônons et organisons des événements permettant l’émancipation et le développement personnel
et collectif avec humanité et ouverture, notamment en :

- Proposant des outils à la croisée entre communication non-violente (CNV),
  féminisme et sexualité (sex-positif) dans un esprit d’amusement,
  d’[inclusivité](inclusif.html), d’auto-responsabilité, d’entraide et de liberté ;
- Aidant à la prise de conscience des conditionnements, privilèges, normes,
  injonctions et oppressions auxquelles nous sommes tous⋅tes, plus ou moins,
  exposé⋅e⋅s et que nous avons intériorisé ;
- Accompagnant des réflexions, des déconstructions en ayant un esprit critique et bienveillant.

---
### 🌈 Nos sources d'inspiration :

- [Burner (Nowhere & Burning Man)](https://frenchburners.assoconnect.com/page/1544293-les-10-principes)
- [Fémi](https://www.unwomen.org/fr/news/stories/2014/9/emma-watson-gender-equality-is-your-issue-too)n[isme](https://medium.com/@leilla/quelle-culture-f%C3%A9ministe-voulons-nous-102141a63830)
- [Collectif Fracas](https://www.collectif-fracas.fr/)
- [Cercles & systèmes restauratifs](https://cerclesrestauratifs.org/wiki/Syst%C3%A8mes_et_Cercles_Restauratifs)
- [Justice transformatrice](https://zine-le-village.fr/)
- [Sexualité positive (S+)](https://fr.wikipedia.org/wiki/Mouvement_sexpositif)
- [Chatonnade](https://docs.chatonnade.org/)
- [Felina(d)e](https://www.facebook.com/felinades.montpellier.1/posts/104070411220914)
- [La Bulle Sexpo](https://www.instagram.com/labullesexpo/)
- [Auto-responsabilité](http://lesgouttesdo.net/glossaire/) des [gouttes d'O](https://www.changer-de-paradigme.fr/interview-jeremy-bemon-culture-du-desir/)
- [Hola](https://fr.wikipedia.org/wiki/Holacratie)cra[tie](https://labdsurlholacracy.com/bande-dessinee-holacracy/)
- [LGBTQIAP+](https://fr.wikipedia.org/wiki/Portail:LGBT)
- [Rainbow Gathering](https://fr.wikipedia.org/wiki/Rainbow_Gathering)
- [CNV](https://orianeboyer.com/cnv/)
- [Tantra](https://youtu.be/VQZw5n3_VEA) & [Slow Sex](https://youtu.be/lb91KF9Ww9I)
- [Kinky](https://fr.wikipedia.org/wiki/Sexualit%C3%A9_tordue) & [BDSM](https://youtu.be/JDL-xCNZlBo)
- [Philosophie du partage & logiciel libre](https://contributopia.org/fr/)
- [Écologie & permaculture](https://permacultureprinciples.com/fr/fr_ethics.php)
- [solarpunk](http://www.re-des.org/un-manifest-solarpunk-francais/)

### 🌠 Ce que vous pourrez trouver chez nous :

- liberté d’être
- liberté des corps
- liberté d’expression
- remises en questions
- émotions fortes
- écoute, soutien émotionnel
- confrontation à ses blessures
- cadre sécurisant propice à l’introspection
- diverses formes d’intimités
- acculturation au consentement
- échanges profonds et authentiques
- inclusivité
- conscience des oppressions systémiques
- bousculer ses conditionnements, ses privilèges, les normes et injonctions sociétales…
- gouvernance partagée
- outils de résolution de conflit
- prix étudiés pour n’exclure personne
- une réflexion sur le rapport à l’argent

Tenté·e·s par l’intim'aventure ?

Dites-le nous !
contact@intim-idees.fr

## Qui sommes-nous ?

Actuellement l'équipe d'organisation c'est :

### Céliane

Passionné.e par la communication et les émotions depuis plus de 10 ans, j’ai choisi la psychologie comme ouverture au monde.

J’arpente les espaces féministes intersectionnels et d’éducation populaire depuis 4 ans, ça a été ma porte d’entrée vers les réflexions sur mon rapport aux normes sociales. J’ai découvert les relations non-exclusives, et cheminé encore plus sur la qualité de communication et les questions de consentement.
Dans mes explorations de dynamiques collectives, s’est cristallisée la part de moi qui est bergère des émotions et du bien-être du groupe, par la facilitation de cercles restauratifs, par les pratiques de Communication non Violente…

J’ai fait la connaissance du milieu sexpositif en 2020 en Summer Kittens, et depuis j’œuvre à créer davantage d’espaces de bienveillance et d’épanouissement autour de moi.
Dans cette communauté, mon rêve serait de créer des groupes en non-mixité choisie pour l’empowerment et la créativité qu’ils ouvrent.

### Loïc
Présentation à venir.
C'est, semble-t-il, le plus intimidé de la team'idées.
En attendant ses propres mots, merci à lui de mettre discrètement de l'huile dans les rouages en interne
(logistique, animation de réunion, gouvernance, identité graphique...)

### Millicent

Socialisé homme blanc quasi-cis quasi-hétéro, j'en hérite de nombre de privilèges et angles morts.
Au regard des injonctions de genre, je me retrouve bien davantage aujourd'hui dans le spectre des non-binarités.
Avec un peu d’aide, j’ai acquis une confiance bienvenue pour oser communiquer publiquement ou recadrer des égos envahissants lorsque je facilite.
Je pars de très loin coté (in)aptitude sociale, ce qui m’aide à faire preuve d’empathie pour les maladresses inconscientes et à faire de la pédagogie dessus.

Informaticien passionné depuis l’enfance, j’aime soutenir ce qui a du sens à mes yeux grâce à mes compétences techniques.

Depuis 2012, je m’intéresse à la communication entre êtres humains (émotions, CNV, rapport au consentement…), aux façons de relationner (polyamour, anarchie relationnelle),
aux façons de vivre ensemble et de faire société (autogestion, intelligence collective, mode de gouvernance, rapports de pouvoir et de privilèges…).
Entre 2018 et 2019, je découvre les cercles et postures restauratives, ainsi que l’écoute empathique.
J’ai participé à des évènements et fréquenté des lieux à tendance autogérée, certains avec un cadre explicite pour faciliter l’autogestion (forum ouverts, Agile Open France, nouvel an SexPo au 30ème ciel)
d’autre avec un cadre plus flou : geek camp, café poly, RADE (rencontre avec des enfants), rainbow gathering, squat et ZAD.
En 2021, je découvre avec intérêt la culture d'auto-responsabilité radicale du réseau des gouttes d'O et de la collective de Chalvagne et je m'en inspire depuis.

Hors informatique, j’ai l’impression d’être coutumier des acculturations informelles aux milieux qui m’intéressent,
bien plus de pratique CNV que de stage de CNV, pas mal d'évènement S+ mais bien plus de temps à vivre avec des personnes
qui respirent cette culture… Je me sens proche des valeurs burner sans avoir participé à plus que des apéros burner…
Bref, entre incarnation et imposture, je navigue de mon mieux entre prendre la place de contribuer et laisser voir protéger la place de celleux qui peinent à en prendre.

### Myrha
Présentation à venir.

### Sam
<!-- ![left circle small](img/team/Sam-large.jpg) -->
Je suis une personne genderfluid sociabilisé.e homme, qui fréquente le milieu libertin depuis 10 ans et qui au fur et à mesure de mes déconstructions et réalisations, ne supportais plus les injonctions et biais que je percevais dans ce milieu et dans la société en général, sans rien faire pour changer tout cela.

Maintenant j'agis pour que nous nous entraidions tou.s.tes à s'aimer plus sainement.

## Évènements

<!--
Un [code couleur 💚​💙​❤​🖤 ](https://docs.chatonnade.org/chatonnade/cadre/code-couleur)
et [thématique  🍑​🎶​🌗​👩💻 📚 🍻](https://docs.chatonnade.org/chatonnade/cadre/code-thematique)
est utilisé pour vous aider à repérer les évènements correspondant à ce que vous souhaitez vivre.
-->
Note :
après chaque événement (voir avant pour vous permettre d'anticiper) nous publions :
- Les [statistiques](liste-stats.md) (nombre d'inscriptions, de participant⋅e, genre, fourchettes d'ages...)
- Les [prévisionnels et bilans financiers](liste-compta.md)


### Futurs

Bientôt :)

### Passés
- 11 septembre 2022 : 💚 [Après-midi *méta'relation*](2022-09-11-meta-relation.md)
- 10 septembre 2022 : 💚 [Après-midi *idées'couvrir*](2022-09-10-idees-couvrir.md) puis ❤ [Soirée *idées'culottées*](2022-09-10-idees-culottees.md)
- 09 septembre 2022 : 💚 [Soirée *jeux'thèmes* : authenticité et émotions](2022-09-09-jeux-themes.md)
- 29 juin 2022 : 💚 [Soirée Jeux'thèmes : rapport à l'argent](2022-06-29-jeux-themes.md)
- 02 juin 2022 : 💚 [Soirée Jeux'thèmes : esprit critique](2022-06-02-jeux-themes.md)
- 8 mai 2022 : 💚 [Après-midi idées'couvrir](2022-05-08-idees-couvrir.md) puis [soirée idées'connectées](2022-05-08-idees-connectees.md)
- 30 avril 2022 : 💚 [Journée aire'éthique](2022-04-30-journee-aire-ethique.md) puis ❤ [soirée aire'éthique](2022-04-30-soiree-aire-ethique.md)
- 21 avril 2022 : 💚 [Soirée Jeux'thèmes : fresque du climat](2022-04-21-jeux-themes.md)
- 09 avril 2022 : 💚 [après midi idées'couvrir & soirée idées'connectées](2022-04-09-idees-couvrir.html)
- 24 mars 2022 : 💚 [Conférence sur les dynamiques relationnelles (monogamie, polyamour et autres non-monogamies éthiques)](2022-03-24-conference-dynamiques-relationnelles-ethiques-polyA.html)
- 23 mars 2022 : 💚 [Soirée Jeux'thèmes](2022-03-23-jeux-themes.html)
- 5 mars 2022 : ❤ [Laborizontale](2022-03-05-laborizontale.md)
- 19 février 2022 : 💚 [Évènement tiers : Introduction au S+ Chatonnade par Marjorie et Ramïn](https://www.facebook.com/events/984466889157008)
- 19 juin 2021 : 💚 [Wel'Cat](2021-06-19-welcat.html)


## Ressources & contacts

- [Le forum de la communauté intim'idées](https://forum.intim-idees.fr/) (accès sur invitation)
- [**Groupe Mobilizon**](https://mobilizon.fr/@intim_idees)
- [**Page facebook**](https://www.facebook.com/intim.idees)
- E-mail : [**contact@intim-idees.fr**](mailto:contact@intim-idees.fr)
