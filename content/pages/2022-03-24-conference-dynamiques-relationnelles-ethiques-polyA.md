title: "Les dynamiques relationnelles : monogamie, polyamour et autres non-monogamies éthiques"
-------------------

![bannière d'illustration](img/bannieres/conf-dynamiques-relationnelles-polyA.jpg)

# Les dynamiques relationnelles : monogamie, polyamour et autres non-monogamies éthiques
_**Jeudi 24 Mars à 19h**_

Venez écouter, discuter et débattre avec les collectifs [Polyamour Bordeaux](https://www.facebook.com/polyamour.bordeaux) et [intim’idées](https://intim-idees.fr/) au sujet des relations amoureuses.

La conférence aura lieu dans l’**amphithéâtre Fabre de la faculté de la Victoire** (3 ter, place de la victoire).
De ce fait, le **pass sanitaire** sera nécessaire pour participer en présentiel.

Sans pass sanitaire, l'enregistrement audio sera disponible sur [la chaîne Youtube de l'Université Populaire de Bordeaux](https://www.youtube.com/channel/UCqFGeIW2GXy7IGg_Q2a6BEg).

Nous ajouterons sur le site intim'idées les supports de présentation avec lien clickable et enregistrement vidéo s'il y a.


## Synopsis :

Vous vous questionnez sur les normes et injonctions sociales concernant les relations ? Tant mieux ! Nous aussi !
Nous avons osé vivre d’autres relations que le modèle traditionnel des contes de fée,
et nous avons plein de choses à raconter !
Nous vous parlerons d’amours, pluriels ou exclusifs, d’éthique, de jalousie, de compersion,
des changements de regard sur le monde qu’entraînent les changements de regard et de vie dans l’intimité, etc…
Et bien sûr, nous parlerons de communication, d’émotion et des merveilles qu’on peut faire avec !
Venez découvrir l’étendue des possibles, afin que vous puissiez concevoir votre chemin relationnel sur mesure,
comme le résultat de vos choix.

Cette conférence est donnée par les collectifs [Polyamour Bordeaux](https://www.facebook.com/polyamour.bordeaux)
et [intim’idées](https://intim-idees.fr/) en partenariat avec l’[Université Populaire de Bordeaux](https://upbordeaux.fr/).

-----

Crédit image : [Kimchi cuddle](https://kimchicuddles.com/post/116646961755/updated-the-polycule-characters-page)
