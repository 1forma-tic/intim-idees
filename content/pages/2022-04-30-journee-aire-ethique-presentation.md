title: Journée aire'éthique
-------------------
# Journée aire’éthique
Samedi 30 avril de 9h30 à 19h

À l’occasion de Beltaine, fête de la fertilité, s’entremêlerons ateliers d’autodéfense intellectuelle et rituels d’inspiration païenne. Nous espérons fertile, l’émulsion de ces deux mondes.

## Objectifs

- **Célébrer la beauté de la vie** de chaque être vivant, de chaque personne, de chaque partie à l'intérieur de nous...
- Faire goûter l'expérience d’être authentiquement écouté⋅e et pleinement accueilli⋅e telle que nous sommes
- Partager ce qui nous relie toustes quel que soit votre rapport au monde
- Dépasser les a priori et incompréhensions pour apprivoiser les complémentarités de regards
- Conscientiser différents chemins d'exploration de nos vérités et leurs limites (spirituel, ésotérique, scientifique, athée...)

## Présentation de la journée
Bienvenue à toustes dans l’univers du collectif « Intim'idées » que nous associons pour la première fois à l'univers [« Miss Permaculture »](https://misspermaculture.com) !
Nous sommes heureux⋅ses de vous inviter à la première édition d'une journée folle où nous mêlerons ces deux cultures qui ne demandent qu'à mieux se connaître mutuellement !

### 💡 Qu’est-ce que le collectif « Intim’idées » ? 💡

Le but du collectif est de s’entraider à s’aimer (soi et les autres) en prenant le soin d’offrir la sécurité nécessaire à l’exploration libre des possibles.
Nous prônons et organisons des événements permettant l’émancipation et le développement personnel et collectif avec humanité et ouverture.
Plus spécifiquement :

- Proposer des outils à la croisée entre communication non violente (CNV), féminisme et sexualité (sex-positif) dans un esprit d’amusement, d’inclusivité, d’auto-responsabilité, d’entraide et de liberté.
- Aider la prise de conscience des conditionnements, privilèges, normes, injonctions et oppressions auxquelles nous sommes toustes, plus ou moins, exposé⋅e⋅s et que nous avons intériorisées ;
- Accompagner des réflexions, des déconstructions en ayant un esprit critique et bienveillant ;


### 🥰 Qu’est-ce que l'univers « Miss Permaculture » ? 🥰
Loin de n'être que du jardinage, la permaculture cherche à donner des voies pour que les sociétés humaines puissent répondre à leurs besoins tout en régénérant les écosystèmes dont elles dépendent. Comment manger, s'abriter, socialiser, s'éduquer, s'occuper de sa santé ; avec la terre, la lumière, la pluie, les matériaux, les connaissances, les compétences qui sont autour de moi...?
Et comment concevoir une société qui vive en harmonie avec son environnement si elle voit l'être humain comme détaché du Vivant...?
Toute la philosophie de Julia est de proposer une approche de la permaculture avec une saveur éco-féministe, de sensibilité, d'intuition, d'écoute et de fantaisie.

#### Pourquoi faire les célébrations de la Roue de la Vie ?
Célébrer les 8 fêtes de la Roue de la Vie est un moyen pour se rappeler que nous sommes toustes interdépendant⋅e⋅s et connecté⋅e⋅s à la nature qui nous héberge depuis toujours et dont nous faisons partie. C'est aussi prendre soin de notre besoin de spiritualité. Entendons-nous : la spiritualité dans le sens de se sentir connecté⋅e à quelque chose qui nous dépasse, comme lorsqu'on regarde les étoiles.
Les célébrations que propose Julia sont des propositions inspirées particulièrement par le néo-paganisme, les sorcières et traditions celtes (Starhawk) mais sont aussi infusées de multiples courants religieux et spirituels, du travail qui relie de Johanna Macy, ainsi que d'approches issues de psychologie, développement personnel, féminin sacré, et tout ce qui peut alimenter sa quête de spiritualité et de sagesse. Sans se prétendre la Grande-Prêtresse-Qui-Sait-Tout, l'accent est mis sur la recherche de sens derrière les actes que nous posons, de facon à la fois profonde et ludique. Marquer le rituel d'une saison, c'est en comprendre le sens symbolique, et y réfléchir pour en appliquer ce qui nous parle à l'échelle personnelle, inter-personnelle et collective. C'est prendre le temps de se poser, 8 fois dans l'année, la question du sens de sa vie, faire des choix conscients et poser des actes (Et pas le faire uniquement le 1er janvier - et encore !). C'est faire le choix et prendre le temps de devenir acteur⋅ice de sa vie plutôt que de la subir.

#### Qu'est-ce que Beltane ?
Beltane est la fête entre l'équinoxe de printemps et le solstice d'été. C'est le moment où l'on honnore la fabuleuse poussée de fécondité de la nature, de fleurs, de sève, de lumière, de libido, la force de vie, la puissance de création après le repos de l'hiver.
Ce qui est mis à l'honneur, c'est la beauté de l'union de toutes ces composantes de la vie.

### Pourquoi faire de la zététique et de l'esprit critique ?
Notre cerveau est bien connu pour sa capacité à nous jouer des tours... sans que nous nous en rendions compte ! Cela peut donc fausser notre perception de la réalité et influencer nos actions contre notre réel intérêt. S'outiller pour gagner en vigilance peut nous épargner des écueils et des mauvaises interprétations. Le domaine de la spiritualité qui est basé sur l'expérience personnelle peut être très déroutant à naviguer et il peut être intéressant de garder une certaine mesure de doutes et de convictions...


### 🦄 Qui peut participer ?☝️
Toute personne qui souhaite découvrir l'ésotérisme/spiritualité et/ou la zététique/l'esprit critique dans un cadre sécurisant et bienveillant.

Toute personne qui veut progresser sur ses capacités à communiquer avec autrui même si ses propos la font réagir.

Enfin et surtout : toute personne qui est prête à vivre une expérience conviviale mêlant zététique et ésotérisme !

### Où ?
Cet évènement aura lieu dans une maison à Bègles, à 300 mètres de l’arrêt La Belle Rose du tram C.

L'adresse précise sera indiqué après validation de l'inscription.

### 🕟 Le déroulement 🕟
- 9h30 : ouverture et accueil des participant⋅e⋅s
- 10h : fermeture des portes et début du cercle d'ouverture
- ≈ 10h30 : exercer sa qualité d'écoute
- ≈ 11h00 : Pourquoi s'intéresser aux autres manières de penser et voir le monde ?
- ≈ 11h30 : Comment débatre d'un sujet complexe en 3 phases
- ≈ 13h : déjeuner en conscience
- ≈ 14h30 : cercles de paroles ( Quelles valeurs me guident et guident ma pratique ? De quelle manière ces valeurs se manifestent dans ma pratique ? Qu'est-ce que cela m'apporte ? )
- ≈ 15h15 : Rituel :
  - cérémonie d’ouverture “accueil des éléments”
  - méditation guidée
  - ateliers créatifs (fabrication de couronnes de lierre, dessin, peinture, collages…)
  - clôture cérémonie
- ≈ 16h30: danses trad guidées
- ≈ 17h:  pause gouter & stand énigmes zététiques, (dé)jouons les illusions, (senses, experiences, analyse)
- ≈ 17h30 : philosophie de la vérité
- ≈ 17h45 : conte viking
- ≈ 18h30 : cercle de cloture
- 19h : fin de la journée, départ pour les un⋅e⋅s, repas pour celleux qui restent à la soirée
- 20h : début de la [soirée aire'éthique (18+)](2022-04-30-soiree-aire-ethique.md)

Fil rouge soutien : accueillir et dissiper les crispations résiduelles et autres images ennemies. (Tout au long de la journée, au stand soutien).

### Quoi apporter ?

- À manger et à boire (sans alcool) :
  pour l’auberge espagnole du déjeuner,
  nous vous demandons d’amener un plat préparé, salé ou sucré, à partager.
  Pour être en cohérence avec nos valeurs écologiques,
  nous vous encourageons à privilégier des ingrédients locaux, de saisons, sans emballages jetables et bios
  (si c'est compliqué coté budget, vous pouvez nous contacter, on pourra vous partager des astuces).

- Venez paré⋅e⋅s de votre plus belle tenue, où vous vous sentez pleinement incarner vous-même : sentez-vous sublimes et rayonnant⋅e⋅s !

Prennez aussi si vous avez :

- Du lierre et autre matériaux pour faire des couronnes de lierre
- Des décorations pour le lieu (bouquets de fleur, bougies, pierres, encens, guirlande, etc), plus particulièrement si vous pouvez venir la veille pour aider à installer.

### 👥 Qui animera l'évènement ? 👥

- Millicent (intim'idées) à la coordination et en animation de certains passages
- Julia ([Miss Permaculture](https://miss-permaculture.com)) animera la cérémonie de Beltane et ses ateliers ésotériques
- Jean-Christophe sera notre [conteur de légendes](https://www.facebook.com/Soufflesonge)
- David ([Dubitaristes](https://dubitaristes.fr/)) animera les ateliers orienté esprit critique et zététique
- Charpi sera notre traducteur philosophe
- Leila sera notre traductrice queer
- Notre équipe de soutien émotionnel intim'idées habituelle sera renforcée pour accueillir avec soin les rationalistes autant que les mystiques dans leurs vécus.
  Psychothérapeutes, praticien⋅ne⋅s CNV, habitué⋅e⋅s des systèmes et cercles restauratifs, animataire d'éduc'pop, et autre profils expérimentés.

### 💸 Participation financière 💸

Sur cet événement, plusieurs personnes viennent en renfort de l’équipe habituelle d’animation. Tout en ayant des moyens limités, 3 personnes ont aussi des frais de déplacements (deux viennent de Toulouse, une de Dordogne).
Ainsi, durant toute la journée, il y aura au moins 6 personnes entre animation et soutien au bon déroulé, de 9h à 19h (soit 60h à votre service, soirée non comprise). Au regard des précédents événements, le temps de préparation et de bilan hors événement est 2 fois supérieur à celui sur place, soit 120h de plus pour permettre cette journée. Oui, on vous bichonne !

Nous souhaitons a minima défrayer les personnes organisatrices et animatrices qui en ont besoin, et autant que possible soutenir financièrement celles qui n'ont pas d’autre source de revenu stable.
En outre, nous souhaitons aussi contribuer aux frais du lieu d’accueil, ainsi qu’aux assos qui créent ou gèrent les outils libres que nous utilisons. Bref, contribuer à faire fleurir ce qui contribue à cette journée !

Nous visons donc pour la journée un budget de 1000 € à 1500 € reposant sur 20 à 30 participations financières, avec en tête que pour permettre aux personnes précaires de participer à la hauteur de leurs moyens, une part significative de notre objectif budgétaire repose sur les rares foyers aisés participants.

Pour information, tu peux consulter <a href="https://intim-idees.fr/liste-compta.html" target="_blank">les bilans financiers des précédents événements</a>.

Si tu ne sais pas combien mettre pour <strong>la journée aire'éthique</strong>, voici quelques fourchettes de prix suggérés  :

- PaF solidaire : 1€ à 30€
- Équilibre 30€ à 60€
- Soutien 60€ à 120€
- Justice sociale & soutien des futurs évènements 120 € à 240€ ou plus.

Le prix que vous pouvez mettre ne fait pas partie des critères de sélection.
La transparence sur le budget de l'événement sera disponible sur le site Intim'idées dans les jours qui suivent l'évènement.


### 😷 Information COVID 😷

Nous tenons à rappeler que le virus est toujours parmi nous et que venir à cet événement comporte des risques, d’autant plus que le cadre ne permettra pas le respect des gestes barrière.
Le port du masque ne sera pas obligatoire.
Avant la tenue de la soirée :

Nous vous recommandons d’avoir un schéma vaccinal complet, mais nous ne l’exigeons pas.
Il est également demandé de ne pas venir en cas de symptôme ou de cas contact.
Que vous soyez vacciné⋅e ou non, nous vous demanderons de nous fournir le résultat d’un **test antigénique** négatif à votre nom, datant de **moins de 24h** avant le début de l’évènement ou, si impossibilité de faire un test à l'avance, de venir avec un autotest à faire sur place.

Après :

Merci de prévenir l’orga si des symptômes apparaissaient dans les 7 jours après l’évènement afin que chaque participant⋅e puisse se faire tester et s’isoler si nécessaire.


## ✉️ Comment je m’inscris ? ✉️

Ça y est, le formulaire de pré-inscription est enfin disponible :

https://framaforms.org/pre-inscription-journee-aireethique-du-30-avril-2022-1650547752

Nous aurons beaucoup à faire durant les 48 heures précédant l’événement.
Il n’est pas sûr que nous ayons le temps d’étudier ta pré-inscription si tu nous l’envoi seulement en dernière minute.
On fera au mieux.

Si vous rencontrez des difficultés pour remplir ce questionnaire, n’hésitez pas à nous contacter :
contact@intim-idees.fr ou par sms : 0 770 772 770

Il est possible que tu ne sois pas retenu⋅e pour participer cette fois-ci car :

- Le groupe sera constitué de 30 personnes maximum, afin de fournir la qualité de cadre et d’animation à laquelle nous tenons.
- Nous choisirons les personnes afin de favoriser un équilibre et une cohérence entre les personnes présentes lors de l’événement, avec une vigilance à l’inclusivité des personnes appartenant à des groupes discriminés.

Si tu es sur liste d'attente ou que l'événement est complet, nous tâcherons de te prévenir au plus tôt.


## ❓ Des questions ❓

Tu peux bien évidemment nous contacter ! Notre préférence : par email (contact@intim-idees.fr), sur notre page Facebook « Intim’idées » ou sur nos autres réseaux sociaux.
Merci d’avoir lu toutes ces informations nécessaires et à très bientôt !

La team’idées ❤
