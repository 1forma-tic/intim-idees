title: Faire du consentement un outil de bienêtre au service des concerné⋅e⋅s (en évitant de l'ériger en dogme traumatisant)
-------------------
# Faire du consentement un outil de bienêtre au service des concerné⋅e⋅s (en évitant de l'ériger en dogme traumatisant)

## Le consentement n'est pas une fin en soi, mais un concept utile. (au bienêtre des personnes qui souhaitent interagir harmonieusement)

### Les impasses du consentement absolu

### Les ravages du non consentement

### La culture du viol
Désaprentissage du consentement
Internalisation d'une normalité déséquilibrée

### Le consentement situé


## Quelques guides du consentement comme filet de sécurité des relations humaines

Insérer ici des infographie avec accronyme pour un consentement idéal issue des travaux de différents collectifs féministes.

## Plutôt qu'un dogme du bon consentement, un cartographie dans laquelle naviguer avec conscience et créativité

### Types d'intéractions (échelle d'implication)
- Co-présence (sur un territoire, un lieu, un évènement, une pièce, autour d'une table, à proximité dans une foule)
- regard (fugace, partagé, appuyé...)
- verbal léger (bavardage à vide / small talk, info logistique)
- verbal intellectuel, analytique, politique, conceptuel
- verbal émotionnel, personnel, empathique
- corporel sensoriel (découverte, expérmientation, stim autisitque, touchés sans intension de plaisir)
- corporel sensuel (interaction impliquant les sens avec intension de plaisir)
- corporel sexuel (interaction avec intension d'excitation sexuelle)
- corporel génital (contacte avec les parties génitales : anus, penis, vulve | selon référentiel : bouche, fesses, seins)
- corporel pénétrant (objet ou appendice dans orifice)

Chaque type d'interaction implique généralement les précédents, mais il peut y avoir des exceptions.
- chaque type d'intéraction verbale n'implique pas les autres
- en s'aidant de technologie (masque, téléphone, jouet connecté...), il est possible d'intéragir corporellement ou verbalement sans être en présence ou se voir.

Le degré d'implication de chaque type peu varier du vécu d'une personne à l'autre, ce n'est pas une échelle universelle. (Par exemple, parler émotion peut être plus impliquant que de partager de la sexualité)

### Rituels / modalités de consentement (echelle de sécurité)
1. **la réponse est non, même à entendre la question** (ne pas déranger, cette personne viendra vers vous si elle souhaite intéragir avec vous, ou signifiera un changement de posture/rituel de consentement à son égard)
2. **demande si tu peux demander** (verbalement, d'un signe ou en laissant une trace (post-it), si oui, continue avec les lignes 3. ou 4.)
3. **propose/demande, puis laisse l'autre revenir vers toi** (en option fournir un papier et un stylo ou jeton pour signifier : statut inital "en réflexion donc non pour l'instant", tranché pour "oui", tranché pour "non")
4. **propose/demande, puis attends la réponse sur place** (si groupe, et qu'un seul "non" vaux pour véto : se tenir par les mains, puis attendre 2 grandes respiration. Si je veux signifier un non, je presse la main de mes voisins. Si on me presse la main, je transmet en pressant mon autre main. Ainsi s'il y a un non, tout le monde le saura sans que personne ne sache qui en est à l'origine. Si après ces 2 grandes respiration il n'y a pas eu de main pressée, c'est que c'est oui pour tout le monde.)
5. **agi/fait/ose, puis demande comment s'était** (pas ok, ok mais désagréable, ok bienvenue, enthousiate continue ! ...)
6. **agi/fait/ose, charge à l'autre de dire non** (identifier, assumer et exprimer ses limites)
7. **agi/fait/ose, charge à l'autre de défendre et faire respecter ses limites** (rapport de pouvoir plutôt qu'entraide au vivre ensemble -> on sort du concepte de consentement sauf à pratiquer cela en tant que jeu avec modalité d'entrée et de sortie (safeword) consenti en amont via d'autres modalité de consentement que celle-ci)

### Rituels d'association / d'élaboration de droit / Rituels de changement de rituel dans un cadre donné (échelle de confiance)
- **explicite et spécifique** (proposition de changement de modalité de consentement sur certaines actions précisées)
- **explicite et peu spécifique** CNC (Consensual non consent) : passage, généralement cadré dans le temps, à des modalités de consentement à posteriori (5, 6 ou 7) avec réajustement à l'occasion de rituel (aftercare, debrief, point/température relationnelle...)
- **implicite et non spécifique** par habitude, familiarité, imitation, essai erreur, rapport de pouvoir... Ce qui se passe quand on ne met pas de la conscience à faire autrement. Ça peu se passer bien, mais ça reste l'approche la plus risquée et propice à l'émergence de dynamique toxiques.

### Les normes occidentales implicites cartographiées explicitement
Cette démarche nous semble salutaire, que ce soit pour expliciter quand on souhaite faire autrement,
ou pour être vigilant⋅e aux héritages réflexes et inconscient que nous risquont de reproduire malgré nous.

- Pour la co-présence, dans les espaces publics, l'usage est généralement à la modalité 7, parfois 6.
- Pour la co-présence, dans les espaces privés sans connaitre le propriétaire, l'usage est 1.
- Pour la co-présence, dans les espaces privés en connaissant le propriétaire, l'usage fluctue généralement entre 3 et 4 avec beaucoup de demande cachée derrière des tournures implicites, et selon les biais de pouvoir et différence de classe, tout le spectre (de 1 à 7) peut avoir lieu.
- Pour les interactions verbales précédant l'émotionnel, l'usage fluctue entre 6 et 7.
- Pour le verbal émotionnel, 1 entre hommes, sauf à avoir créer un lien privilégié de manière implicite. Avec des femmes, c'est entre 2 et 6 voir 7, en fonction de la proximité et de la classe sociale.
- Pour le corporel non pénétrant, l'usage est d'agir lentement, et de s'ajuster en fonction des réactions (donc entre 4 et 7 selon l'attention aux réactions et à quel point l'action est posé comme une demande implicite ou imposée comme un rapport de force)
- Pour le corporel pénétrant, l'usage est au rituel 4, qui glisse souvent implicitement vers 7 dans les relations de couple établies

### La cartographie intim'idées

Hors cas particuliers listé plus bas, lorsque vous souhaitez interagir avec autrui dans un évènement intim'idées,
voici les usages que nous vous invitons à suivre.

TODO

#### Les Cas particuliers

- bracelet
TODO
- lieu / priorité des espaces
TODO

- **Vos accords interpersonnels explicites priment sur ceux du collectif**
tant que toutes les personnes directement impliquées ou indirectement impactées
ont explicitement et le plus librement possible consenti à vos accords interpersonnels.
Nous recommandons pour l'établissement de ces accords interpersonnel le rituel 3, par écrit.
L'invitation à l'écrit n'est idéalement pas pour juger et reprocher à l'autre de ne pas avoir respecté les clauses du contrat.
Cette invitation à l'écrit est davantage pour vous rendre compte des évolutions implicite qui ont lieu (que ce soit chez vous ou chez les autres)
et les questionner avec les concerné⋅e⋅s pour choisir explicitement soit de réaffirmer ce qui avait été explicité et d'y réajuster vos comportements,
soit d'entériner (acter explicitement) l'évolution implicite de vos accords comme étant votre nouvelle préférence
(de rituel de consentement, pour les types d'intéractions concernés, entre les personnes qui consentent à ce nouvel accord).


#### Entre inconsistance du cadre et dogme rigide, avoir conscience des risques

Plus vous vous éloignez des usages que nous vous invitons à respecter lors des évènements intim'idées,
plus vous prenez le risque de mettre en difficulté autrui (de la simple gêne au choc traumatique)
et que le collectif réagisse pour restaurer le lien de confiance et de sécurité
nécessaire aux libertés d'être, d'agir, d'expérimenter qui sont l'essence de notre communauté.
Selon l'ampleur de la perte de confiance, la réaction peut être une simple discussion,
aussi bien qu'un chemin d'accompagnement passant par l'exclusion jusqu'à ce que nous ayons les ressources
pour vous ré-inclure en maintenant un degré de sécurité suffisant pour ce que nous souhaitons pouvoir vivre en votre présence
(et dans ce cas vous pouvez nous aider en demandant au cercle inclusion ce dont il aurait besoin pour faciliter votre ré-inclusion).
Entre les deux, il peut y avoir médiation, cercle ou autre processus restauratifs,
préalable de pratiquer certains ateliers avant de retourner à des évènements incluant certaines possibles...

Tout cela veux dire qu'il est possible de prendre quelques libertés avec les rituels de consentement que nous vous invitons à adopter,
mais que l'amplitude de ces libertés peut vous jouer des tours si vous surévaluez votre capacité à faire à votre manière
tout en offrant une sécurité, une confiance, une qualité de consentement suffisantes aux autres.
