title: Cadre idées'couvrir
-------------------
![bannière d'illustration](img/bannieres/idees-couvrir.png)

# 📜 Cadre idées'couvrir 📜


✅ Consentement
Dans un idéal de consentement Conscient, Révocable, Libre, Authentique, Spécifique :
- Si tu reçois un "non", l'invitation est de remercier la personne qui s'est écoutée
- Observer une personne ou un groupe est aussi une interaction qui impliqué le consentement des personnes regardées
- Pour rejoindre un groupe, invitation à t'assurer du consentement explicite de chaque personne du groupe (avec des outils pour éviter le sentiment de pression de groupe)
- Être attentif⋅ve aux biais de consentement :
⋅ les rapports de pouvoir réels ou ressentis (liés au statut, à l'expérience, au genre, à l'âge, à l'introversion/extraversion, à la posture orga/participant⋅e...)
⋅ l'effet de groupe ou le rapport à soi (se sentir inclus⋅e, faire comme les autres, se prouver quelque chose...)
⋅ les biais liés aux relations antérieures (avoir déjà vécu une interaction similaire avec quelqu'un⋅e, avoir une relation intime ou amoureuse, avoir déjà dit oui précédemment...)

✅ Expression Personnelle
La liberté de choisir est au centre :
- Tu peux te retirer d’un exercice / atelier à tout moment, tu peux rester en retrait sur une partie et être plus actif⋅ve à d’autres moments
- Tu peux quitter l’événement quand tu le souhaites
  - La liberté d’être seul⋅e prime sur la liberté d’être en lien
- Les rires, les larmes et toute autre forme d'expression émotionnelle sont bienvenues. Tu es libre d'exprimer toutes les émotions qui te traversent.

Chaque personne vit l'événement différemment et il n'y a aucune attente sur ce qu'il faudrait faire ou non : jouer ou ne rien faire, entrer en connexion avec d'autres ou observer, explorer notre espace documentations / outils, partir quand tu le souhaites...
Sentir son envie de participer aux propositions est un exercice de consentement en soi.

- Cet événement est souhaité sans sexualité
- La nudité peut être autorisée (mais en aucun cas obligatoire) si l'ensemble des participant⋅e⋅s est d'accord (et cela te sera annoncé lors de ta confirmation d'inscription). Le formulaire d'inscription te permettra d'exprimer ton degré de confort avec la nudité, et ton souhait d'expérimenter ou non cette possibilité. En fonction des réponses, nous t'informerons du cadre choisi concernant la nudité sur l'événement.
- Ton expression vestimentaire est bienvenue quelle qu'elle soit, tu peux t'habiller avec ce qui te fait te sentir pleinement toi-même, confortable ou confiant⋅e.

✅ Soin
- Des soutiens émotionnels (les organisataires et les participant⋅e⋅s volontaires) seront présent⋅e⋅s tout au long de l’événement pour apporter de l’écoute, du soutien émotionnel aux participant⋅e⋅s qui en auraient besoin. Iels sont là pour te permettre de vivre la meilleure expérience possible et pour recevoir la parole et les émotions d’autres participant.e.s. Un lieu calme est prévu pour recevoir du soutien si nécessaire.
- Cet évènement n’a pas de visée thérapeutique, l’expression de soi, le soutien mutuel et le partage ne sont pas proposés dans un but de soigner mais plutôt d’expérimentation de faire autrement. Cela n’exclut pour autant pas des effets thérapeutiques occasionnels.

✅ Responsabilité
- Ce type d’évènement n'est jamais 100% sûr. Tout est fait par les organisataires pour le rendre aussi sécurisant que possible mais le risque zéro n'existe pas.
- Participer, c'est aussi accepter de prendre la responsabilité de ta sécurité et de l'expression de tes limites. Nous te proposerons plusieurs outils de communication pour faciliter ta capacité à prendre soin de tes limites.

✅ Attention partagée
- Chaque espace comportera des règles ou des priorités (ne pas fumer à l'intérieur, certains espaces sont sans chaussures ou ont des priorités sur ce qui peut y avoir lieur)
- Nous sommes tous⋅tes co-responsables de l'expérience, que nous espérons la plus positive et inclusive possible. Pour cela, tu peux exprimer tout commentaire par rapport à ce que tu vois ou ce que tu ressens vis-à-vis du cadre ou du contenu de l'événement (respect du cadre, comportements systémiques, angles morts...). En partageant cette envie de co-construction pour vivre l’expérience que nous souhaitons nous offrir, le collectif sera bien plus résiliant et capable d’inclure que si cela repose seulement sur l’équipe d’orga.
- Invitation à ne pas consommer de substances et à ne pas être dans un état de conscience modifié pendant l’événement, car cela modifie la capacité de consentement. Note : l'état de conscience peut aussi être modifié par la fatigue, les émotions fortes, le sentiment d'urgence...

✅Confidentialité
- Pas d’enregistrement (audio, photo, vidéo) pendant l’événement
- Si tu souhaites parler de l'événement en dehors, prends soin de ne pas dévoiler le nom d'autres participant⋅e⋅s ou d'information permettant de l'identifier.
- Les personnes avec lesquelles tu interagis pendant l'événement n'auront peut-être pas envie d'en reparler ou de partager certaines interactions avec toi en dehors, invitation à être vigilant⋅e aux limites potentielles après l'événement aussi

✅ Gestion de conflit
- Invitation à faire de ton mieux pour incarner les valeurs du collectif : l’écoute de soi et de ses émotions, l’ouverture aux autres, le consentement explicite et enthousiaste, le non-jugement,  la liberté et la bienveillance.
- Il est difficile d'intégrer tous les codes dès le début, et si cela met en difficulté le groupe et la qualité d'expérience que nous souhaitons offrir à vivre aux participant⋅e⋅s, nous pourrons décider d’exclure les personnes dont l’acculturation nous demande une énergie préjudiciable au reste du groupe. Dans ce cas, nous leur indiquerons les préalables qui nous sont nécessaires pour les réintégrer lors de prochains évènements, ou lors d'événements avec d'autres curseurs d'inclusivité.
- Il y a un rôle Gestion de conflits et pratiques restauratives dans l'équipe d'orga qui peut être sollicité pour prendre soin de tensions, conflits, désaccord ou bris de consentement qui pourraient avoir lieu avant/pendant/après l'événement, que ce soit entre participant⋅e⋅s, entre participant⋅e et orga, ou entre orga. N'hésite pas à nous solliciter !


