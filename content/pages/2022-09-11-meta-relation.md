title: Méta'relation du 11 septembre 2022
-------------------
![bannière d'illustration](img/bannieres/meta-relation.png)

# Méta'relation du 11 septembre 2022

Bienvenue à cette première après-midi *méta'relation* avec un focus proposé sur le RBDSM(PB)

Avant de t'y inscrire prend le temps de lire la [présentation du format d'événement **méta'relation**](presentation-meta-relation.md),
tu sauras mieux si c'est fait pour toi et les informations pratiques à savoir.

## Prêt⋅e ? C'est par ici pour [**les pré-inscriptions**](https://framaforms.org/pre-inscription-metarelation-du-11-septembre-2022-1660904595) !

Cette après-midi *méta'relation* cloture un week-end entier avec plusieurs événements intim’idées qui peuvent se faire aussi bien indépendamment, qu’à la suite.

Il sera possible de [dormir sur place](laborizon-dortoir-infos-pratiques.md) le vendredi et samedi soir en dortoir.
Nous t'accueillons donc volontiers même si tu viens de loin !

Au programme du week-end :
- Vendredi soir : [jeux'thèmes](2022-09-09-jeux-themes.md)
- Samedi après-midi : [idées'couvrir](2022-09-10-idees-couvrir.md)
- Samedi soir : [idées'culottées](2022-09-10-idees-culottees.md)
- Dimanche après-midi : [méta'relation](2022-09-11-meta-relation.md)

À très bientôt,

La team’idées ❤
