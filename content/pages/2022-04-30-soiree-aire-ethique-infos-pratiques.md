title: Soirée aire'éthique, infos pratiques
-------------------
# \[intim'idées] Inscription acceptée pour la soirée aire'éthique du 30 avril 2022
Bonjour,
Nous avons retenu ta préinscription pour la soirée aire’éthique de samedi 30 avril, tu es bienvenue parmi nous !

Il ne te reste qu’à **nous confirmer ta participation par retour mail ou sms et nous indiquer de quelle façon tu vas nous faire parvenir ta participation financière**.

La compta détaillée sera disponible prochainement.
Pour consulter celle des événements antérieurs :

* [Bilan financier Laborizontale du 5 mars 2022](https://intim-idees.fr/2022-03-05-laborizontale-compta.html)
* [Bilan financier idées’couvrir du 9 avril 2022](https://intim-idees.fr/2022-04-09-idees-couvrir-compta.html)

Pour nous transmettre ta participation, tu peux utiliser :

* un virement bancaire à l’IBAN : FR76 1090 7000 0105 8192 7634 740
* Paypal vers l’email [gammanu@free.fr](mailto:gammanu@free.fr) (en choisissant l’envoi à un proche pour éviter que paypal ponctionne des frais)
* Lydia vers le numéro 0770772770
* Nous l’amener en main propre dès que possible si tu nous connais déjà
* Nous l’amener en espèces sur place (à faire en dernier recours, histoire que nous ayons de la trésorerie)
  PS : tu peux tout à fait choisir de nous envoyer le minimum que tu avais prévu maintenant, et compléter à ta discrétion en fin d’événement si c’est plus confort pour toi.

**Nourriture et boissons :**
Nous avons oublié de parler nourriture dans le formulaire soirée.
L'orga fournira de la nourriture érotisable au cours de la soirée, cependant, le repas du soir, entre 19h et 20h ainsi que le buffet nocturne repose sur ce que chacun⋅e y amènera. Nous vous demandons donc, comme pour le déjeuner, de venir avec un plat préparé salé ou sucré, à partager. Pour être en cohérence avec nos valeurs écologiques, nous vous encourageons à privilégier des ingrédients locaux, de saisons, sans emballages jetables et bios (si c’est compliqué coté budget, vous pouvez nous contacter, on pourra vous partager des astuces).

Pour que vous puissiez équilibrer entre sucré et salé, salade et quiche... Nous t'invitons à regarder ce qu'il y a sur le [pad auberge espagnole](https://pad.aquilenet.fr/p/intim-idees-YLYrkil) pour choisir quoi apporter, et à indiquer à ton tour sur le pad ce que tu apportes.
Idem, nous t'invitons à prendre des boissons (sans alcool), kéfir, kombucha, jus de fruit... pour étoffer le buffet, et à indiquer ce que tu apportes sur le pad.

Parmi les participant⋅e⋅s :

* 1 personne mange sans gluten
* 7 sont végétariennes
* 1 est végan
* 1 est allergique aux crustacés
* 1 est allergique ou intolérante aux produits laitiers

**Horaire :**
Rendez-vous samedi à 19h30 pour faire ton auto-test (si tu n'étais pas déjà là pour la journée)
A 20h, nous fermons les portes pour commencer le cercle d’ouverture de la soirée.

**Covid :**
Pour limiter les risques covid lors de l’événement, vous pouvez au choix :

* faire un auto-test sur place en arrivant
* fournir le résultat d’un test covid à votre nom effectué le samedi même.

**L’adresse du lieu :**
26 Ter, rue André Lapelleterie, 33130, Bègles
L’entrée est entre le 24 et le 26, au fond d’une ruelle non goudronnée, au niveau du portail blanc avec bouteille en contrepoids (on fléchera le chemin depuis le tram).

L’arrêt du tram C “La belle rose” est à 200 mètres de la maison.
Si vous venez en voiture, difficile de se garer dans le quartier.
Le plus simple est de se garer dans les parkings proches des arrêts de tram Lycée Václav-Havel ou Villenave Pyrénées et de finir en tram. Si vous voulez quand même tenter votre chance à vous garer dans les ruelles proches de la maison, merci de garer quand-même vos véhicules de façon à faciliter la circulation dans la rue et la sortie des voitures de leurs garages.
Si vous souhaitez covoiturer, voici le [pad où s’auto-organiser](https://pad.aquilenet.fr/p/intim-idees-YLYrkil)

**Tenue et parements :**
Viens paré⋅e de ta plus belle tenue colorée, où tu te sentes pleinement t’incarner toi-même : sens-toi sublime et rayonnant⋅e !
Les couleurs de Beltane sont toutes les couleurs de l’arc-en-ciel, avec une prédilection pour le rouge (l’amour), le blanc (la pureté), le vert foncé (l’abondance), le jaune (le soleil et l’été).
Tu seras libre d'adapter ta tenue à tes envies tout au long de la soirée et de la nuit : tenue à paillette, arc en ciel, lingerie, lumineuse, nue... Libre à toi.
Notes que les étages sont sans chaussures, et que certaines activités sont prévues en extérieur, dans le jardin.

La créativité et la diversité dans l’expression de genre est bienvenue. Si tu n’es pas prêt⋅e à côtoyer des gens qui ne rentrent pas dans les cases “homme” ou “femme”, il est encore temps de te désister.
Des soutiens émotionnels pourrons t’aider à accueillir ce qui se passe pour toi si cela te perturbe.

**Composition du groupe :**
Pour la soirée, sauf imprévu, nous serons 15 à 21 (dont 3 animataires soirée, et 5 à 6 soutiens)

* 6 à 9 femmes (dont 1 animatrice et 1 ou 2 soutiens)
* 6 à 9 hommes (dont 1 animateur soirée, 2 animateurs journée, et 2 soutiens)
* 3 non-binaires (dont 1 animataire et 2 soutiens)

Les âges s’étalent de 24 à 39 ans.

Pour 4 à 6 personnes, c’est un premier pas chez intim'idées (dont 2 animataires, et 1 soutien)

Les [statistiques détaillées](https://intim-idees.fr/2022-04-30-soiree-aire-ethique-stats.html) seront bientôt disponibles.
Il en va de même pour le [bilan financier](https://intim-idees.fr/2022-04-30-soiree-aire-ethique-compta.html) de l’événement.

À très vite, et si tu as des questions, n’hésites pas !

La team’idées

–-

Notre site : https://intim-idees.fr/

Si tu ne veux plus recevoir d’email de notre part, réponds-nous STOP par email.
Tu peux même nuancer si tu veux certaines infos et pas d’autres !
