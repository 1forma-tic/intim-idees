title: Rêve restauratif
----
# Rêve restauratif (Atelier de 3h)

## Fiche descriptive

**Intention :** Explorer notre rapport au bris (de consentement) de manière restaurative.

**Durée :** 3h (2h stricte minimum, peux déborder jusqu'à 4h si le cadre permet cette souplesse)

**Nombre :** Nombre pair de participant⋅e⋅s souhaité.

**Préalable participant⋅e⋅s:**
- savoir prendre soin de ses limites pour ne pas se remuer plus que vous ne pouvez gérer
- accepter de parler en son "Je" pour qu'il soit possible d'exprimer des possibles différents sans que les autres se sentent attaqué⋅e⋅s ou nié⋅e⋅s.
- recommandé : prendre votre nécessaire de prise de note (carnet+stylo ou ordinateur)

**Préalable orga/cadre:**
- au moins un soutien émo disponible pendant l'atelier et durant l'heure qui suit
- lieu calme, apaisant (matelas, coussins)...
- Si animé par Hensa, ventilateur, brumisateur, clim ou saison fraiche, mon cerveau fait grève au-dessus de 27°C

### Présentation rapide
Comment rêveriez-vous être traité⋅e lors qu'un bris de consentement a lieu ?
Si vous en êtes l'auteurice ? Si vous l'avez subi ?
Si votre cadre l'a permis ? Si vous en êtes témoin ou impacté autrement ?
Et qu'aimeriez-vous faire dans chacune de ces positions, comment être en mesure de le faire effectivement si ça à lieu ?

Nous explorerons ça par duo suivi de retour en collectif avant d'aller vers des propositions concrètes
à mettre en place en évènement et/ou dans vos vies.

Plus d'info : https://intim-idees.fr/atelier-reve-restauratif.html


## Déroulé
1. 10" Accueil cohésion et adaptation au groupe en cercle de parole :
   - Météo : Comment vous sentez-vous ?
   - Intension : Pourquoi venez-vous à cet atelier, que venez-vous y trouver ?
   - Centrage posture : "Restauratif" c'est quoi pour vous ? (Sens, experience...)
2. 3" Restaura'quoi ? (Qu'est-ce qu'une dynamique restaurative ?)
3. 1" Sécurité, cadre : On va explorer des situations qui peuvent être très remuantes/confrontante/trigger.
   Vous pouvez vivre les exercices en vous plongeant dedans à fond si ça vous aide
   aussi bien que vous protéger en adoptant différentes stratégies de mise à distance de la situation.
   Prenez soin de vous et de vos limites.
4. 10" Choix collectif du scénario : quelle situation explorer ensemble ? (avec TW associés)
5. 30" Première mise en situation (généralement le rôle qui subi la situation)
  - 2" mise en duo
  - 1" méta-communication trigger et prévention (comment éviter de mettre votre binome en PLS et l'aider à en sortir ; comment iel fonctionne dans sa communication et comment je peux mieux le comprendre)
  - 4" Plongez dans le rôle, comment vous sentez-vous ? (2" par personne)
  - 4" Qu'aimeriez-vous faire ?
  - 4" Qu'aimeriez-vous qui soit fait ?
  - 1" Re-brassage des duo
  - 4" Check émotionnel
  - 10" En collectif, popcorn : Avez-vous besoin de (dire) quelque-chose avant de plonger dans le rôle suivant ?
    (déposer au groupe ce qui est envahissant, encombrant, éphémère, ou besoin de soutien émo, ou de pause, **pas un temps de débat**)
6. 30" Deuxième mise en situation (généralement le rôle qui produit la situation)
  - même phases
7. 30" × N Autres mise en situation (animation, orga, témoin... ou les mêmes mais à des moments différents)
  - même phases
8. 15" à 45" Mise en commun & co-construction de stratégies restauratrices et transformatrices
   (Classées en prévention primaire (avant), secondaire (sur le moment) et tertiaire (après coup))
9. 15" à 30" Méta-moisson : concernant l'atelier et son format
  - Accueil, présence, clarté : comment avez-vous vécu l'atelier
  - Puissance : Qu'est ce qui était précieux, à conserver, qu'est ce qui serait à revoir et comment ?


## Scénarios disponible

### La succube, le prêtre et le photographe
#### Description
- Lors d'un événement Sx+, un photographe professionnel est convié pour immortaliser avec qui veut des scènes.
- fin de journée, le photographe range son materiel et s'apprête à partir
- Une participante, en tenue de succube vois un autre participant en tenu de prêtre et souhaite une séance photo sur cette dynamique
- La succube demande au photographe s'il est encore possible de faire une séance
- Le photographe, bien que pressé, accepte, ressort son materiel et commence à guider la séance
- Guidé par Le photographe, La succube se retrouve à simuler une fellation faite au prêtre
- La séance se termine, le photographe range et part.
- La succube se rend compte que le rôle et la posture dans la quelle elle a été mise en scène ne lui conviens pas.
  Elle s'est retrouvée en posture soumise face au prête au lieu d'être dans la posture de corruptrice qu'elle souhaitais incarner.
  Et ce n'était pas ok de mimer cette fellation, mais dans le stress du moment, elle n'a pas su affirmer ses limites.
- Dans les jours qui suivent, elle indique à l'orga. de l'événement qu'elle n'est pas d'accord pour que ses photos soient diffusées.
#### Mise en situation
- Imaginez-vous Succube : vous venez de finir la séance, vous ne vous sentez pas bien, le photographe range.
- Imaginez-vous Photographe : vous venez de finir la séance, un doute vous traverse l'esprit sur le déroulé de la séance, vous êtes fatigué et pressé de partir.
- Imaginez-vous Témoin : vous assistez à la séance, vous percevez La succube mal à l'aise.
- Imaginez-vous Orga : La succube vous informe qu'elle à mal vécu la séance photo et refuse que ses photos soient utilisées.
- Imaginez-vous Photographe : L'orga vous informe qu'il y a un véto à l'utilisation des photos de la dernière séance, que vous aviez accepté de faire en bonus.

### La soirée romantique

Prénom épicènes : Camille | Daniel⋅e | Emmanuel⋅le | Ebe | Raphaël⋅le | Isaure
| Léonore | Imèle | Maël | Joy | Gaël⋅e | Alix⋅e | Ambre | Ebène | Amel⋅le | Amine
| Anaël⋅le | Aurèle | Aya | Claris⋅se | Clem | Dominique | Edwige | Elie | Emi⋅e | Flo
| Gabriel⋅le | Guenoël⋅le | Gwen | Jenn | Joël⋅le | Jo | Ju | Leï | Lesly | Lotus | Lou | Lulu
| Maya | Myriam | Naomy | Nathanael⋅le | Ninon | Noël | Numa | Pascal⋅e | Paul⋅e | Faune | Pia
| Poka | Roro | Salomé | Sam | Cendre | Siriel⋅le | Soraya | Steph | Zala | Zoé

#### Description
- Gwen & Zoé match sur un site de rencontre.
- Après quelques échanges, Gwen : "Je suis dispo demain soir, ça te dit de manger Japonnais ?"
- Zoé accepte et s'y rend en voiture.
- Un peu de stress au départ, le diner s'avère agréable.
- Avant de partir, pendant que Zoé va aux toilettes, Gwen paie l'addition.
- Zoé ramène Gwen en voiture jusqu'à son domicile.
- Sur la proposition de Gwen, Zoé monte boire une infusion
- A l'initiative de Zoé, le visionnage d'un épisode de série prend la suite de l'infusion.
- Les voici une couette sur les jambes, à se câliner tendrement sur le canapé pendant l'épisode.
- À partir du générique, le mood évolue vers de la sexualité.
- Gwen initie non verbalement un 69.
- 10 minutes plus tard, Gwen glisse un doigt à Zoé qui gémie de surprise.
- Y voyant un encouragement voir un orgasme, Gwen continue.
- Orgasme de Gwen qui s'endort en serrant Zoé dans ses bras.
- En confusion totale, Zoé ne sait pas quoi penser de ce qui s'est passé, ni douleur ni extase,
  une cacophonie émotionnelle et sensorielle inextricable. Et ce que c'était ok ? Pourquoi je me suis laissé⋅e
  embarquer. En avais-je envie ? De quoi avais-je envie ? C'était cool ou pas ?
  Et-ce que c'était trop pour moi ou pas assez ? Pourquoi j'étais à la fois frustré⋅e que ça s'arrête et soulagé⋅e ?
  Je fais quoi maintenant. Je dors sur place ou je rentre ? J'ai envie de rentrer mais je ne veux pas vexer ni réveiller Gwen...
  Et puis je suis coincé⋅e dans ses bras, sa tête sur mes jambes, et sa cuisse calé entre mon épaule et ma tête...
- Environ 1h d'absence ou de sommeil plus tard, Zoé reprend ses esprits :
  Non mais je suis pas bien là, je vais jamais arriver à dormir ! Bon je rentre sinon je vais être KO demain.
- Zoé s'extrait en silence et Gwen continue sa nuit pendant que Zoé regagne son domicile.
- Gwen le lendemain, par sms : "Super soirée, mais je ne t'ai pas entendu⋅e partir. Bien rentré⋅e ? On se revoit ce soir ?"
- Zoé toujours en confusion vis à vis de la veille : "Je sais pas, je suis pas en forme aujourd'hui"
- Gwen, à 18h30 : "Alors, toujours pas décidé⋅e ?", Zoé : "Non", Gwen : "Ok, repose toi bien"
- Le WE suivant, Zoé raconte sa soirée à Camille et se rend compte que :
  - les doigts dans le cul n'était pas ok
  - que le 69 sans avoir parlé IST non plus
  - et une fois dedans, l'arrêt brutal de Gwen après son orgasme, merci mais non merci.
  - En fait, en rester à de la sensualité aurais été parfais
- Gwen, le dimanche soir : "Super WE de plongée, c'était magnifique. La prochaine fois je t'emmène ? Et toi, bon WE ? Comment vas-tu ?"
- Lundi soir, pas de réponse de Zoé, Gwen appel deux fois, Zoé ne décroche pas.

<!-- version détaillée :
- Gwen & Zoé match sur un site de rencontre.
- Après quelques échanges, Gwen : "Je suis dispo demain soir, ça te dit de manger Japonnais ?"
- Zoé accepte, le rendez-vous est convenu à 20h au restaurant japonnais près du travail de Gwen. Zoé s'y rend en voiture.
- Un peu de stress au départ, le diner s'avère agréable.
- En attendant le dessert, au gré d'une conversation sur leurs séries préférées,
  quelques échanges de regards hésitant et complice, leur mains s'approchent puis se touche,
  pour ne se lâcher qu'à l'arrivée du dessert.
- Avant de partir, pendant que Zoé va aux toilettes, Gwen paie l'addition.
- Zoé revient et se dirige vers le comptoir, Gwen lui indique que c'est bon.
- Zoé, mi touché⋅e mi en colère, lui rétorque d'un air de défiance complice : "Si c'est comme ça, c'est moi qui te ramène !"
- Les voilà en voiture direction chez Gwen, dont la main viens prudemment se placer et se retirer de la jambe de Zoé
  au gré des passages de vitesse. D'abord concentré⋅e sur la conduite Zoé esquisse des sourires au jeu de mains de Gwen.
- Voiture garée au parking de la résidence, Gwen propose :
  "Tu montes boire un verre ? J'ai une infusion cacao gingembre réglisse cannelle qui devrais te plaire !"
- Zoé hoche la tête les yeux enjoués, fait un bisou sur la joue de Gwen, dit "merci" et descend de la voiture.
- Gwen prépare l'infusion pendant que Zoé explore du regard l'appartement.
- Entre deux gorgées, Zoé : "Tu me montres le premier épisode de Sense8 dont tu m'a parlé tout à l'heure ?"
- Les voici une couette sur les jambes, à se câliner tendrement sur le canapé pendant l'épisode.
- Pendant le générique, Gwen et Zoé se regardent sensuellement puis échange un premier baiser langoureux.
- L'excitation monte, les voici à se déshabiller mutuellement, à s'explorer avec poigne et appétit.
- Une 20aine de minutes plus tard, Gwen se retourne en 69 sur Zoé et reprend de plus belle.
- Zoé s'y adapte et prend soin de l'entre-jambe de Gwen.
- 10 minutes plus tard, Gwen glisse progressivement un doigt dans l'anus de Zoé qui gémie de surprise.
- Y voyant un encouragement, Gwen y plonge un second doigt et joue avec.
- Les mouvements et gémissement de Zoé s'accentuent ce que Gwen interprète comme un orgasme.
- En parallèle, avec toutes ces stimulations, un orgasme convulsif traverse Gwen qui pivote ensuite sur le flanc,
  tire la couette sur leurs corps fatigués, serre Zoé dans ses bras et s'endort.
- En confusion totale, Zoé ne sait pas quoi penser de ce qui s'est passé, ni douleur ni extase,
  une cacophonie émotionnelle et sensorielle inextricable. Et ce que c'était ok ? Pourquoi je me suis laissé⋅e
  embarquer. En avais-je envie ? De quoi avais-je envie ? C'était cool ou pas ?
  Et-ce que c'était trop pour moi ou pas assez ? Pourquoi j'étais à la fois frustré⋅e que ça s'arrête et soulagé⋅e ?
  Je fais quoi maintenant. Je dors sur place ou je rentre ? J'ai envie de rentrer mais je ne veux pas vexer ni réveiller Gwen...
  Et puis je suis coincé⋅e dans ses bras, sa tête sur mes jambes, et sa cuisse calé entre mon épaule et ma tête...
- Environ 1h d'absence ou de sommeil plus tard, Zoé reprend ses esprits :
  Non mais je suis pas bien là, je vais jamais arriver à dormir ! Bon je rentre sinon je vais être KO demain.
- Zoé s'extrait en silence et Gwen continue sa nuit pendant que Zoé regagne son domicile.
- Gwen le lendemain, par sms : "Super soirée, mais je ne t'ai pas entendu⋅e partir. Bien rentré⋅e ? On se revoit ce soir ?"
- Zoé toujours en confusion vis à vis de la veille : "Je sais pas, je suis pas en forme aujourd'hui"
- Gwen, à 18h30 : "Alors, toujours pas décidé⋅e ?", Zoé : "Non", Gwen : "Ok, repose toi bien"
- Le WE suivant, Zoé raconte sa soirée à Camille et se rend compte que :
  - les doigts dans le cul n'était pas ok
  - que le 69 sans avoir parlé IST non plus
  - et une fois dedans, l'arrêt brutal de Gwen après son orgasme, merci mais non merci.
  - En fait, en rester à de la sensualité aurais été parfais
- Gwen, le dimanche soir : "Super WE de plongée, c'était magnifique. La prochaine fois je t'emmène ? Et toi, bon WE ? Comment vas-tu ?"
- Lundi soir, pas de réponse de Zoé, Gwen appel deux fois, Zoé ne décroche pas.
-->

#### Mise en situation
- Imaginez-vous Zoé : vous venez de revisiter la soirée avec Camille.
- Imaginez-vous Gwen : Vous venez de recevoir de Zoé la réponse "Je sais pas, je suis pas en forme aujourd'hui".
- Imaginez-vous Camille : Zoé viens de vous raconter sa soirée.
- Imaginez-vous Gwen : Le lundi soir, sans nouvelle de Zoé.
- Imaginez-vous Zoé : Devant le message de retour de WE de Gwen.


### La chasse humaine
#### Description
- 1h du matin, l'animataire présente le jeu, le cadre de sécurité (safeword "rouge" et "double tape", bracelet indiquant qui est bienvenue à interagir avec cette proie et si de la génitalité est bienvenue...)
- 2h du matin, la chasse commence, les proies se cachent, les chasseur⋅esses se préparent.
- Le chasseur trouve La proie (Le et La pour désigner cette situation particulière).
- Il reconnaît la personne (qu'il connais d'avant), ses bracelets lui indique qu'il peut jouer avec, il plonge dans le rôle primal
  et performe l'incarnation qu'il imagine adéquoite : immobiliser La proie, sortir son sexe pour lui imposer une fellation.
- La proie fait mine de se débattre en semblant dans son rôle, mais arrivée à la fellation, quelque chose ne va pas.
- Le chasseur se rend compte que La proie n'a plus l'air d'être dans son rôle, il arrête, demande si ça va.
- La proie répond un oui un peu hésitant
- Le chasseur libère sa proie pour continuer sa chasse.
- Après la chasse, La proie va voir le chasseur et lui exprime que sans protection, la fellation n'était pas ok.
- Le chasseur s'en excuse.
- Le lendemain, La proie s'aperçoit que ce n'était pas la seule chose qui ne lui convenait pas
- La proie demande du soutien au médiataire (une des personnes de l'équipe soutien)
- Un temps d'écoute commence avec La proie, qui aurais aimée plus de progressivité et de vérification intermédiaire.
  Elle s'est sentie prise de court. Ne s'est pas saisie des outils de consentement présentés lors du cadrage de la chasse
  Elle a peur qu'adresser la situation dégénère en call-out et abime au lieu de prendre soin.
- Un temps d'écoute commence avec Le chasseur, qui n'avais pas envie de génitalité, mais pour performer le rôle,
  y est aller tout de même promptement. Qui c'est arrêté dès qu'il as eu un doute sur le fait que ce soit ok.
  Il a peur d'avoir abimé le lien avec La proie, et d'être exclu par la communauté et catalogué agresseur (call-out).
- Un temps de médiation entre elleux à lieu, chacun⋅e s'exprime et se sent entendu par l'autre.
- Iels se sentent apaisé en fin de processus et ne ressente pas le besoin qu'autre chose soit fait.

#### Mise en situation
- Imaginez-vous L'animataire de la chasse : vous venez d'apprendre que cette situation a eu lieu dans votre atelier.
- Imaginez-vous Lae chasseuresse : En plein action, vous avez un doute sur le fait que les résistances de La proie soit jouée ou réelle.
- Imaginez-vous Lae chasseuresse : La proie viens de vous dire que sans protection, la fellation n'était pas ok.
- Imaginez-vous Lae chasseuresse : Lae médiataire vous informe que La proie est mal à l'aise avec d'autres aspects de la situation et souhaite une médiation.
- Imaginez-vous Lae proie : En posture de soumission et résistance liée au jeu, mais avec une gêne bien réelle d'avoir ce sexe en bouche sans avoir eu le temps de vous demander si c'était ok.
- Imaginez-vous Lae proie : Toujours en jeu, alors que le chasseur est parti.
- Imaginez-vous Lae proie : Le lendemain, vous voyez que ça ne va pas, que ce qui s'est passé hier n'était pas ok.
- Imaginez-vous Lae médiataire : La proie viens de vous décrire ce qui s'est passé.
- Imaginez-vous Lae médiataire : Votre rôle maintenant, c'est de contacter Le chasseur pour lui apprendre qu'il y a un souci, et recueillir son consentement à participer à une médiation.

### Le cercle de parole
Main levée ignorée, départ anticipé
#### Description
- Al facilitataire du cercle explicite les règles de prise de parole :
  on lève la main pour demander la parole, si d'autre l'ont demandée avant, on lève plusieurs doigts pour se mettre en file d'attente.
- Le cercle commence avec une présentation par al facilitataire.
- Al cadriste lève la main pour demander le sens de métamour.
- Al facilitataire ne semble pas s'en être rendu compte et continue.
- Al fluidiste profite d'une respiration d'al facilitataire pour demander "Maon partenaire ne veux rien entendre, je fais comment ?"
- Al facilitaire tente de répondre.
- S'en suis 3 autres prises de paroles spontanées alors qu'al cadriste lève toujours la main sans que personne ne semble y prêter attention.
- Al cadriste quitte le cercle soudaienement.

- Information complémentaire :
  - une rigidité autistique est présente chez al cadriste, la règle c'est la règle,
    impossible d'imaginer se servir du contexte pour en déduire des ajustements tacites
    en fonction de la manière dont le groupe se saisie ou remodèle le cadre.
  - al facilitataire n'est ni au courant de cette rigidité autistique chez al cadriste,
    ni sensibilisé⋅e aux handicaps invisibles.
    Il semble en être de même pour les autres participant⋅e⋅s au regard de leurs comportements.

#### Mise en situation
- Imaginez-vous Al cadriste : 100 mètres plus loin après avoir quitté le cercle, en pleine rue passante, vous fondez en larme de rage et d'impuissance.
- Imaginez-vous Al facilitataire : vous voyez une personne quitter votre cercle précipitamment, alors que vous commenciez à répondre à quelqu'un d'autre.
- Imaginez-vous voisin⋅e d'Al cadriste : vous avez remarqué sa main levée et al facilitataire répond à al fluidiste.
- Imaginez-vous voisin⋅e d'Al cadriste : Le visage fermé et crispé, Al cadriste attrape son sac, se lève et commence à s'éloigner du cercle.
- Imaginez-vous passant⋅e : vous voyez al cadriste s'arrêter, s'accroupir et s'effondrer en larme en pleine rue.

<!--
### Scénario générique (issue de la V1)
- **imaginez-vous victime :**
  Vous venez de vivre un bris, vous réalisez à peine.
- **imaginez-vous avoir vraisemblablement produit un bris :**
  La personne avec la quelle vous venez d’interagir n’a pas l’air de se sentir bien.
  Êtiez-vous aveuglé⋅e par votre enthousiasme ? Avez-vous insisté ? Raté ou occulté des signaux ?
- **imaginez-vous entendre que vous avez produit un bris :**
  Vous n’aviez conscience de rien, ou aviez considéré réglé/anecdotique une situation passée.
  Cette situation vous est rappelée en vous y désignant comme auteurice d’un bris.

Prenez 2 grandes respirations pour vous mettre en situation et imaginez comment vous vous sentez.
Comment-vous sentez-vous (en quelques mots) ?
Que rêveriez vous de faire ? Et comment rêveriez-vous d’être traité⋅e et par qui ?
-->

------
Scénarios à ne pas diffuser pour l'instant. Pour le reste :
Format d'atelier librement réutilisable : CC-BY-SA 4.0 intim'idées

Nous serions ravis de recevoir vos retours à l'issue de vos sessions : contact@intim-idees.fr

Auteur : Millicent le 19/02/2022

Librement inspiré du travail de Charpi et Leïla ansi que des processus de création de système restauratif (Dominic Barter) et de justice transformatrice.


<!--

# fruits de la première session

- expliciter le cadre au préalable avec les protagonistes (photographe, modèles)
- oser intervenir, interrompre, questionner, se questionner (en tant que modèle ou en second plan en tant que témoin)
- se questionner sur la disponibilité à prendre le temps de faire le shooting correctement (temporellement et émotionnellement)
- temps d'after care/débrief

- en tant que modèles contacter l'orga, informer de la situation, demander de l'écoute/soutiens, voir le démarage d'un processus restauratif
- en tant que photographe, être informé de la situation, me laisser le temps de digérer (avec un sans soutiens) avant d'entendre des demande (ou à plus forte raison des exigeances), puis qu'on me propose/m'accompagne sur comment faire mieux à l'avenir.



Condition propices :
- animation plus guidée/affirmée
- Cadre cozy
- petit commité
- faire une pause

- fournir une situation pré-tirée
- clarifier la finalité des processus restauratifs (aporter de la paix, réhumaniser...)

-->

Phase visibiliser les biais
