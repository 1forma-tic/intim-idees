title: Stats Laborizontale du 5 Mars 2022
-------------------
# Stats Laborizontale du 5 Mars 2022

Actualisé le : 06/03/2022

## Pré-inscriptions reçues :

| Description          | Total | Femmes | Hommes | Autres |
|----------------------|------:|-------:|-------:|-------:|
| Total reçues         |    46 |     18 |     24 |      4 |
| Orga.                |     5 |      2 |      1 |      2 |
| Présent⋅es           |    28 |     15 |     13 |      0 |
| Retenues             |    34 |     16 |     16 |      2 |
| Écartées             |     7 |      0 |      7 |      0 |
| Pas encore examinées |     0 |      0 |      0 |      0 |

## Effectivement présent⋅e⋅s sur place

| Description           |                Global |              Femmes |               Hommes |
|-----------------------|----------------------:|--------------------:|---------------------:|
| participant⋅e⋅s       |                    28 |                  15 |                   13 |
| Age (min/moy/med/max) | 24 / 34,4 / 33,5 / 45 | 24 / 33,1 / 32 / 44 |  28 / 35,9 / 36 / 45 |
| PaF annoncée¹ (idem)  |    1 / 24,5 / 20 / 50 | 1 / 21,28 / 20 / 50 | 10 / 28,08 / 30 / 40 |
| PaF reçue¹ (idem)     |    0 / 25 / 22,5 / 50 |    0 / 23 / 20 / 50 | 10 / 27,30 / 30 / 40 |

¹ PaF (Participation aux frais). Au-delà des statistiques, le [bilan financier](2022-03-05-laborizontale-compta.md) est consultable.

| Attirances                                   | Global | Femmes | Hommes |
|:---------------------------------------------|-------:|-------:|-------:|
| Hétéro                                       |      9 |      3 |      6 |
| Hétéro-flexible / curieu⋅ses⋅x               |     11 |      5 |      6 |
| Pan / bi                                     |      8 |      7 |      1 |
| Homosexuelles / gays / lesbiennes            |      0 |      0 |      0 |
| *Parité idéale pondérée par les attirances²* |     28 |   15,8 |   12,2 |
| Pour rappel : pré-inscriptions retenues      |     28 |     15 |     13 |

² Estimatif de l'idéal de parité avec l'algorithme suivant :
- pour chaque hétéro, 1 personne du genre opposé
- pour chaque hétéro-flexible, 0,2 personnes du même genre et 0,8 personnes du genre opposé
- pour chaque personne pan, 0,5 personnes du même genre et 0,5 personnes d'un autre genre
- pour chaque personne homo, 1 personne du même genre

## Pré-inscriptions retenues :
| Description                      |              Global |                Femmes |                Hommes | Autres |
|----------------------------------|--------------------:|----------------------:|----------------------:|-------:|
| Pré-inscriptions retenues        |                  34 |                    16 |                    16 |      2 |
| Avec PAL                         |                  32 |                    15 |                    15 |      2 |
| Sans PAL³                        |                   2 |                     1 |                     1 |      0 |
| Age (min/moy/med/max)            |   21 / 33 / 33 / 45 | 24 / 33,2 / 32,5 / 44 | 23 / 33,9 / 34,5 / 45 |    NC⁴ |
| Participation financière¹ (idem) |    1 / 23 / 20 / 50 |   1 / 21,19 / 20 / 50 |  10 / 26,45 / 25 / 40 |    NC⁴ |

³ Accepté⋅e⋅s après échange téléphonique ou de visu, avec conditions sécurisantes supplémentaires

⁴ Pas assez nombreu⋅ses⋅x pour fournir des stats sans révéler d'information personnelle.

Venue conditionnée à l'absence d'une autre personne : 1

| Attirances                                   | Global | Femmes | Hommes | Autres |
|:---------------------------------------------|-------:|-------:|-------:|-------:|
| Hétéro                                       |     11 |      3 |      8 |      0 |
| Hétéro-flexible / curieu⋅ses⋅x               |     13 |      6 |      7 |      0 |
| Pan / bi                                     |     10 |      7 |      1 |      2 |
| Homosexuelles / gays / lesbiennes            |      0 |      0 |      0 |      0 |
| *Parité idéale pondérée par les attirances²* |     34 |   19,8 |   14,2 |      ∞ |
| Pour rappel : pré-inscriptions retenues      |     34 |     16 |     16 |      2 |

## Pré-inscriptions écartées
| Description               | Global |              Femmes | Hommes | Autres |
|---------------------------|-------:|--------------------:|-------:|-------:|
| Pré-inscriptions écartées |      7 |                   0 |      7 |      0 |
| Avec PAL                  |      0 |                   0 |      0 |      0 |
| Sans PAL                  |      7 |                   0 |      7 |      0 |

| Attirances                     | Global | Femmes | Hommes | Autres |
|:-------------------------------|-------:|-------:|-------:|-------:|
| Hétéro                         |      4 |      0 |      4 |      0 |
| Hétéro-flexible / curieu⋅ses⋅x |      3 |      0 |      3 |      0 |

| Autres Métriques         | Minimum | Moyenne | Médiane | Maximum |
|:-------------------------|--------:|--------:|--------:|--------:|
| Age                      |      28 |    44,5 |      42 |      62 |
| Participation financière |      20 |      45 |      40 |     100 |
