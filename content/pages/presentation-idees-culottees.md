title: Idées'culottées
-------------------
![bannière d'illustration](img/bannieres/idees-culottees.png)

# Idées'culottées

## Objectif :

Une « Idées'culottées » est un évènement conçu pour explorer dans un cadre sécurisant adapté, le partage et la connexion consciente aux autres de façon verbale, sensorielle, sensuelle ou sexuelle.
La nudité, la sensualité et la sexualité seront donc possibles.

Cet événement est destiné prioritairement aux personnes ayant déjà fait une « Idées'couvrir », puis potentiellement aux personnes ayant fait d'autres évènements ayant des ateliers de consentement (à voir selon vos expériences).

## Présentation de la journée :

### ⏱ Horaires et déroulé ⏱
- 19h00 : Auberge espagnole (sans alcool) où chacun.e apporte quelque chose.
- 20h30 : Cercle d'ouverture de la soirée, de rappel des règles et du cadre, puis cercle en autogestion pour exprimer les propositions les demandes d’ateliers animés par des orgas et/ou des participant⋅e⋅s.
- 21h45 : Exploration libre via notamment des ateliers proposés (mais aucun ne sera obligatoire).
- 04h00 : Clôture des ateliers dans la salle dortoir, mais possibilité de continuer dans la mezzanine.
- 12h00 : Brunch prévu par l'équipe intim’idées.
- 13h00 : Cercle de clôture.

### 💶 Et combien ça coûte ? 💶
Pour qu'organiser, animer et héberger cet événement et les suivants reste joyeux et enthousiaste, nous avons besoin de soutiens et d'encouragements.
Nous serions heureu⋅ses⋅x d'entendre ce que l'événement vous a apporté, mais aussi d'avoir votre aide pour couvrir nos frais.
À cet effet nous proposons une participation à **prix libre**.

Le prix est libre, et nous proposons plusieurs fourchettes de prix pour vous guider :
- PaF solidaire : 1 à 10 €
- PaF équilibre : 10 à 20 €
- Contribution soutien : 20 à 40 €
- Contribution justice sociale & soutien des futurs évènements : 40 à 80 € (ou plus)

Le prix que vous pouvez mettre ne fait pas partie des critères de sélections, tant que ça ne met pas collectivement en péril la viabilité de l’évènement.

#### À quoi va servir cet argent ?
La participation aux frais (PaF) permet de payer la nourriture et de couvrir le prix du matériel (matelas, son, décoration, etc.). Vos contributions au-delà couvrent l’hébergement et son entretien, ainsi que tout le travail d’organisation de l’évènement, voire peuvent faciliter de futurs évènements et, dans la mesure du possible, de rémunérer les organisataires de l'événement.

La transparence sur le budget de l'événement sera disponible sur le site Intim'idées dans les jours qui suivent l'événement.

### 😷 Informations COVID 😷

Pour prendre soin des enjeux de santé virale comme mentale et psychologique
ainsi que des enjeux politique de préservation des libertés individuelles, d'auto-détermination et de cohésion sociale,
nous faisons le choix pour cet événement de nous contenter de demander aux personnes malades
de rester se reposer chez elles.

Pour les autres nous vous recommandons d'adapter vos prises de risques en conscience de ce que vous êtes prêt à assumer
qu'il s'agisse d'attraper ou de transmettre à autrui le covid ou quoi que ce soit d'autres d'ailleurs.

Pour avoir conscience des risques covid et identifier les mesures les plus efficaces
pour les réduire, nous vous recommandons le simulateur [microCovid](https://www.microcovid.org/),
logiciel libre réalisé par des passionné⋅e⋅s en s'appuyant sur des chiffres issus de publication scientifique.

En résumé :

| 💉 Dose de vaccin |  0  |  1  |   2   |   3   |
|-------------------| --- | --- | ----- | ----- |
| Risque            | x 1 | x 1 | x 0.8 | x0.25 |

| 😷 Type de masque      | tissu  | chirurgical | FFP2   |
|------------------------|--------|-------------|--------|
| que tu portes          | x 0,66 | x 0,5       | x 0.33 |
| que les autres portent | x 0,33 | x 0,25      | x 0.16 |

- Donc si tu portes un masque FFP2 en étant vacciné⋅e avec 3 doses, tu réduis tes risques par 12 soit 8,3% du risque initial.
- Si les autres font de même, le risque pour chacun⋅e est réduit par 72 soit 1,4% du risque initial.

Information inconnues :
- taux de réduction du risque (RdR) de faire un dépistage systématique en arrivant sur place.

Pour la soirée, nous seront en intérieur, fenêtres ouvertes si la météo le permet.
D'experience, rares sont les personnes masquées.

## Inscription :

En répondant à ce formulaire de pré-inscription :
https://framaforms.org/pre-inscription-ideesculottees-du-10-septembre-2022-1660742464

Si vous rencontrez des difficultés pour remplir ce questionnaire, n’hésitez pas à nous contacter :
contact@intim-idees.fr ou par sms : 0 770 772 770

PS : nous aurons beaucoup à faire durant les 48 heures précédant l’événement.
Il n’est pas sûr que nous ayons le temps d’étudier ta pré-inscription si tu nous l’envoi seulement en dernière minute.
On fera au mieux.

Il est possible que tu ne sois pas retenu⋅e pour participer cette fois-ci car :
- Le groupe sera constitué de 30 personnes maximum, pour fournir la qualité de cadre et d’animation à laquelle nous tenons.
- Nous choisirons les personnes afin de favoriser un équilibre et une cohérence entre les personnes présentes lors de l’événement, avec une vigilance à l’inclusivité des personnes appartenant à des groupes discriminés.
  L'âge n’est pas un critère de sélection mais il est à noter que l’âge moyen des participant.e.s se situe entre 20 et 45 ans (si cette information est importante pour toi.)


Nous espérons vous voir nombreu⋅x⋅ses et que ce format d'événement vous plaira 🙂

La team’idées ❤
