title: Budget prévisionnel jeux'thèmes
-------------------
# Budget prévisionnel jeux'thèmes

## Participations :
- Participations prévisionnelles : 150 € ( 20 x 7,5 € en considérant qu'entre les soutiens et les solidaires, on arrive pile à l'équilibre )

## Répartition prévisionnelle :
- [ ] 50€ de participation aux frais du lieu d’accueil (loyer & charges ≈ 1600€/mois, stockage du matériel).
- [ ] 50€ pour acheter un [nouveau jeu](liste-jeux.md) à vous faire découvrir (ou fabriquer le materiel nécessaire).
- [ ] 50€ pour aider les orgas à manger jusqu'à la prochaine édition.
