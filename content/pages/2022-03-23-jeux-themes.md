title: Jeux'thèmes
-------------------
![bannière d'illustration](img/bannieres/jeux-themes.jpg)

# Soirée jeux'thèmes
_**mercredi 23 Mars à partir de 19h**_

## Objectif :

Faire réfléchir de manière ludique et pratique sur des thèmes qui nous tiennent à cœur chez intim'idées :
émotion, communication, esprit critique, systémique...
Puis au travers de ces expériences ludique, mener à des prises de consciences et des changements de comportements.

## Conditions :

- Être prêt⋅e à se poser des questions remuantes, et idéalement assumer d'y répondre face aux autres et à soi-même.
- Faire de votre mieux pour accueillir de manière bienveillante et constructive ce qu'exprimeront les autres joueureuses.
  (On pourra vous y aider si besoin grâce à l'équipe de soutiens émotionnel présente sur place)

## Présentation de la soirée :

Salut à toustes,

Pour innover un peu, nous allons vous proposer une première soirée jeux
(nous espérons d’une longue série), et avec des jeux triés sur le volet !

En effet, ce seront spécifiquement des jeux de société thématiques
autour des sujets de déconstruction sociale, d’éducation, d’esprit critique
et d’autres sujets qui nous tiennent à cœur.

Nous envisageons de vous proposer pour cette première soirée :
- le **"jeu des privilèges"** de sexploration,
- le jeu **"connecte"** de l'apprentie Girafe,
- le jeu **"Askhole"** traduit en français.
<!-- - le jeu **"COCréations"** sur l'intelligence collective -->

Nous vous accueillerons à partir de 19h en **auberge espagnole** (sans alcool)
où chacun.e apporte quelque chose (pour un menu varié,
[voici un pad pour vous auto-organiser](https://pad.aquilenet.fr/p/intim-idees-auberge-espagnole-YLYrkil)
puis nous commencerons les jeux en fonction des envies de chacun.e vers 20h.

La soirée est proposée à **prix libre**
(pour acheter de super jeux à vous faire découvrir dans les thématiques intim'idées,
et pour soutenir le lieu d'accueil).

Nous limitons à **20 personnes** cette première édition.

## Inscription :

Inscrivez-vous en envoyant par sms au 0 770 772 770 (mobile non surtaxé) :

**Soirée jeux'thèmes 23/03 &lt;TonPrénom&gt;**

Nous vous répondrons "Inscription OK" ou votre position en liste d'attente.

[Pour plus d'infos sur la liste d'attente, consultez la page détaillant les inscriptions sms.](https://intim-idees.fr/inscription-sms.html)

Nous espérons vous voir nombreu⋅x⋅ses et que ce nouveau format d'événement vous plaira 🙂
