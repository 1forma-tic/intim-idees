title: Bilan financier Jeux'thèmes du 23 Mars 2022
-------------------
# Bilan financier Jeux'thèmes du 23 Mars 2022

Actualisé le : 04/04/2022

## Participations :
- Participations à prix libre reçues : 28,90 €

## Répartition :
- [x] 28,90 € de soutiens aux frais du lieu d’accueil (loyer & charges ≈ 1600€/mois, stockage du matériel) et des 3 orgas qui y vivent.
