title: Journée aire'éthique
-------------------
# Bilan financier Journée aire'éthique du 30 Avril 2022

Actualisé le : 04/04/2022
## Participations :
- Participations prévisionnelles : 150 € ( 20 x 7,5 € en considérant qu'entre les soutiens et les solidaires, on arrive pile à l'équilibre )
- Participations reçues : 1095 € (dont 45€ de la part de Charpi et Leila)


- Frais de transport déjà remboursé : 130€
