title: Inscription par sms
-------------------

# Inscription par sms

## Procédure d'inscription détaillée

1. Inscris-toi en envoyant par sms au **07 83 10 40 07** (mobile non surtaxé) :<br/>
   **&lt;TitreÉvénement&gt; &lt;JJ/MM DateÉvénement&gt; &lt;TonPrénom&gt; &lt;PaF¹&gt; €**<br/>
   Exemple : **jeux'themes 01/12 Clem 10 €**<br/>
   ¹ Participation aux Frais (à quelle hauteur comptez-vous contribuer financièrement, voir présentation de l'événement)
2. Tu recevras dans les heures qui suivent :
   - `Inscrition OK, paiement en ligne ou sur place ? ( Paypal : envoi à un proche : gammanu@free.fr |ou| Lydia 0770772770 |ou| virement IBAN FR76 1090 7000 0105 8192 7634 740 )`
   - `En liste d'attente : N`, `N` étant votre position en liste d'attente.
3. S'il y a du monde en liste d'attente, 12 à 48 heures avant l'événement, tu recevras "Confirmes-tu ta venue à &lt;TitreÉvénement&gt; le &lt;JJ/MM&gt; ?".
4. Si tu comptes toujours venir, confirme en répondant "Oui" (ou autre réponse non ambiguë).
5. En cas de désistement ou d'absence de réponse le jour de l'événement à 14h (s'il commence après 18h), ou la veille à 20h (s'il commence avant 18h),
   ta place sera libérée au profit de celleux inscrit⋅e⋅s en liste d'attente.
6. Si tu réponds présent⋅e mais après l'horaire (14h ou 20h selon l'événement),
   tu seras ajouté⋅e en fin de liste d'attente.

## Procédure liste d'attente
Quand une place se libère :

1. Nous appelons dans l'ordre de la liste d'attente chaque personne pour lui proposer la place.
2. La première personne à accepter reçoit la place.
3. Si personne ne répond aux appels, nous continuons par sms en envoyant à toute la liste d'attente
   "nouvelle place libre pour &lt;TitreÉvénement&gt; le &lt;JJ/MM&gt;"
2. La première personne à accepter (en répondant au sms "dispo" ou "je prends") reçoit la place.
