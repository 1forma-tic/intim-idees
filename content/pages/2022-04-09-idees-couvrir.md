title: Y'découvrir & idées'connectées
-------------------
![bannière d'illustration](img/bannieres/idees-couvrir.png)

# Après-midi idées'couvrir & soirée idées'connectées
_**samedi 9 Avril à 13h**_

## Objectif :

- Faire découvrir et expérimenter la culture du consentement.
- Proposer une experience enrichissante dans les prises de conscience, les postures incarnées,
  et l'authenticité des connexions émotionnelles avec les autres comme avec soi-même.
- Proposer l'exploration du rapport au corp, avec douceur et précaution,
  au rythme et dans les limites des personnes présentes.

## Conditions :

- Faire de votre mieux pour accueillir de manière bienveillante et constructive ce qui sera exprimé.

## Présentation de la soirée :

Bienvenue à tous⋅tes dans l’univers du collectif « Intim'idées » !

Nous sommes heureux⋅ses de vous inviter à la première édition d'idées'couvre !

L'intégralité de cette présentation prend 15 à 30 minutes à lire selon votre rythme de lecture.
Elle vous permettra d'être le plus au clair possible sur ce que nous proposons.

### 💡 Qu’est-ce que le collectif « Intim’idées »  ? 💡

Le but du collectif est de s’entre-aider à s’aimer (soi et les autres) en prenant le soin d’offrir la sécurité nécessaire à l’exploration libre des possibles.

Nous prônons et organisons des événements permettant l’émancipation et le développement personnel et collectif avec humanité et ouverture.
Plus spécifiquement :
- Proposer des outils à la croisée entre communication non-violente (CNV), féminisme et sexualité (sex-positif) dans un esprit d’amusement, d’inclusivité, d’auto-responsabilité, d’entraide et de liberté.
- Aider la prise de conscience des conditionnements, privilèges, normes, injonctions et oppressions auxquelles nous sommes toustes, plus ou moins, exposé⋅e⋅s et que nous avons intériorisé ;
- Accompagner des réflexions, des déconstructions en ayant un esprit critique et bienveillant ;


### 🥰 Qu’est-ce qu’une « Idées'couvre » ? 🥰

Une « Idées'couvre » est un évènement pour faire un premier pas dans le milieu sex-positif (S+), en découvrant le cadre, les valeurs, les codes de communication et en expérimentant le consentement à travers des jeux de connexion à soi et aux autres.
Cet événement est destiné à des personnes débutantes ne connaissant pas ou peu la culture S+.
Il n'y aura pas de sexualité. La sensualité sera autorisée (câlins, caresses non génitales, massages) mais il est possible de vivre l'événement sans contact physique avec d'autres.
Rien ne sera obligatoire dans les ateliers proposés.

### 🫂 Pourquoi un atelier d'éducation au consentement ? 🫂

Le consentement, au-delà d'être une valeur socle des cultures S+ féministes, est un outil permettant de prendre soin de soi et des autres.
Les compétences du consentement sont très peu approfondies dans la société. Les ateliers que nous proposons ont pour objectifs de :
- S'entraîner à se connecter à soi, à identifier ses désirs et ses limites
- Apprendre à exprimer ses envies
- Identifier les potentielles intentions cachées de ses désirs ou de ses demandes
- Apprendre à dire non
- Communiquer pour faciliter le consentement de l'autre : poser des questions, encourager le non
- Apprendre à recevoir un non
- Expérimenter sa gestion émotionnelle, y compris dans les vécus désagréables ou la frustration
- Connaître et reconnaître les biais de consentement qui peuvent flouter les lignes du oui et du non
- Choisir de vivre des interactions pleinement choisies et enthousiastes

### 🦄 Qui peut participer ?☝️

Il s'adresse en priorité aux personnes intéressées par le milieu sex-positif, débutant.e.s ou confirmé.e.s et peut servir de porte d'entrée avant de participer à des événements où la nudité et la sexualité sont permises avec un cadre de consentement explicite.
- Pour les débutant.e.s afin de réellement prendre le temps de s'initier à ces nouveaux codes, et à ce qu'ils impliquent comme changement de paradigme et potentiels chamboulements internes.
- Pour les habitué.e.s, afin d'aller plus loin, de prendre le temps de la rétrospective, de l'introspection et de progresser dans sa capacité à respecter ses limites et celles des autres.

Il s'adresse aussi aux personnes qui souhaitent simplement progresser sur leur capacité à s'écouter, poser leurs limites, communiquer autour du consentement et être à l'écoute des autres. Par exemple au sein de leurs relations amicales ou amoureuses, dans leur exercice professionnel, dans le cadre familial…

### Où ?

Cet évènement aura lieu dans une maison à Bègles, à 300 mètres de l’arrêt La Belle Rose du tram C

### 🕟 Le déroulement 🕟

Entre 13h et 14h - Accueil
Temps pour te changer si besoin (il y a un vestiaire sur place), grignoter et bavarder hors cadre. Tu es également invité.e à amener ce que tu souhaites manger ou boire (sans alcool). De quoi grignoter sera disponible tout au long de l’après-midi mais pas de quoi faire un repas.

14h – Fermeture des portes
Nous choisissons de ne pas accepter les personnes après 14h car nous avons à cœur que tout le monde assiste au rappel du cadre, donc pour les retardataires chroniques retenez 13h, pas 14h.

14h15 – Cercle d’ouverture
Un cercle d’ouverture permettra à tout le monde de se présenter et d'entrer doucement dans l’évènement. Ce sera aussi l'occasion pour nous de répondre aux questions. Nous présenterons à ce moment-là le cadre et les propositions de l’évènement.

15h30 - Ateliers
Nous vous proposerons des jeux pour mettre en pratique les valeurs de consentement, puis des jeux de connexion et déconstruction progressive pour aller à la rencontre de soi-même et des autres.
Par exemple (tout ne sera pas forcément exploré) :
- fournir une écoute de qualité
- communiquer émotionnellement
- identifier ses désirs
- prendre conscience des biais qui nous influence
- oser dire non
- recevoir des non avec gratitude
- affirmer son NON !
- communiquer sans mots
- oser exprimer ses désirs

18h30 - Cercle de clôture
de l’évènement afin de partager les différentes expériences et prendre le temps d'explorer ce qui est vivant en nous et ce que l'on désire apprendre de ça.


Puis, il y aura un temps qui sera libre, à la fois pour prendre le temps, initier ou continuer des interactions, et pour expérimenter de l'auto-gestion où chacun⋅e pourra aussi proposer des activités si iel le souhaite.

Tu pourras bien sûr quitter l’évènement dès que tu le souhaites.


### 👥 Qui animera l'évènement ? 👥

Millicent, secondé.e par Sam et peut-être Céliane seront les animataires principales de l’événement.
Les principaux actaires du collectif intim’idées sont actuellement : Céliane, Loïc, Millicent, Myrha et Sam.


### 💸 Participation financière 💸

La participation aux frais (PaF) permet de payer la nourriture et de couvrir le prix du matériel (matelas, son, décoration, etc.). Vos contributions au-delà couvrent l’hébergement et son entretien, ainsi que tout le travail d’organisation de l’évènement, voire peuvent faciliter de futurs évènements et, dans la mesure du possible, de rémunérer les organisataires de l'événement.
Le prix est libre, et nous proposons plusieurs fourchettes de prix pour vous guider :
- PaF solidaire : 1 à 20 €
- PaF équilibre : 20 à 40 €
- Contribution soutien : 40 à 80 €
- Contribution justice sociale & soutien des futurs évènements : 80 à 160 € (ou plus)
  Le prix que vous pouvez mettre ne fait pas partie des critères de sélections
  La transparence sur le budget de l'événement sera disponible sur le site Intim'idées dans les jours qui suivent l'événement.


### 😷 Information COVID 😷

Nous tenons à rappeler que le virus est toujours parmi nous et que venir à cet événement comporte des risques, d’autant plus que le cadre ne permettra pas le respect des gestes barrières.
Le port du masque ne sera pas obligatoire.

Avant la tenue de la soirée :
- Nous vous recommandons d’avoir un schéma vaccinal complet, mais nous ne l’exigeons pas.
- Il est également demandé de ne pas venir en cas de symptôme ou de cas contact.
- Que vous soyez vacciné⋅e ou non, nous vous demanderons de nous fournir le résultat d’un test antigénique négatif à votre nom, datant de moins de 24h avant le début de l’évènement ou, si impossibilité de faire un test à l'avance, de venir avec un autotest à faire sur place.

Après :
- Merci de prévenir l’orga si des symptômes apparaissaient dans les 7 jours après l’événement afin que chaque participant⋅e puisse se faire tester et s’isoler si nécessaire.


### ✉️ Comment je m’inscris ? ✉️

En répondant à ce formulaire de pré-inscription :<br/>
https://framaforms.org/pre-inscription-ideescouvre-du-9-avril-2022-1648527182

Si vous rencontrez des difficultés pour remplir ce questionnaire, n’hésitez pas à nous contacter :
contact@intim-idees.fr ou par sms : 0 770 772 770

Il est possible que tu ne sois pas retenu⋅e pour participer cette fois-ci car :
- Le groupe sera constitué de 30 personnes maximum, pour fournir la qualité de cadre et d’animation à laquelle nous tenons.
- Nous choisirons les personnes afin de favoriser un équilibre et une cohérence entre les personnes présentes lors de l’événement, avec une vigilance à l’inclusivité des personnes appartenant à des groupes discriminés.

L'âge n’est pas un critère de sélection mais il est à noter que l’âge moyen des participant.e.s se situe entre 20 et 45 ans (si cette information est importante pour toi.)

Si tu es sur liste d'attente ou que l'événement est complet, nous tâcherons de te prévenir au plus tôt.

Chronologie d’inscription :
- 21 mars : ouverture des préinscriptions
- 4 avril à 14h : clôture des pré-inscription, sélection des participant⋅e⋅s et émission des demande de paiement pour validation d’inscription
- 6 avril à 14h : date limite de paiement (et invitation des personnes en liste d’attente le cas échéant)
- 7 avril : email récapitulatif (info pratiques, lieu exact notamment)
- 9 avril avant 13h : vérification des tests antigénique


### ❓ Des questions ❓

Tu peux bien évidemment nous contacter ! Notre préférence : par email (contact@intim-idees.fr), sur notre page Facebook « Intim’idées » ou sur nos autres réseaux sociaux.

Merci d’avoir lu toutes ces informations nécessaires et à très bientôt !

La team’idées ❤


PS : Une partie de ce texte est inspirée de ce que proposent "La Bulle", "Love Experience", "La Licornerie " et "Les Félinades", n'hésitez pas à regarder ce qu'iels proposent ! ❤
