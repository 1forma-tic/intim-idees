title: Après-midi idées'couvrir du 8 mai 2022
-------------------
# Après-midi idées'couvrir du 8 mai 2022

Une *idées'couvrir* est un évènement pour faire un premier pas
dans le milieu sex-positif (S+), en découvrant le cadre, les valeurs,
les codes de communication et en expérimentant le consentement
à travers des jeux de connexion à soi et aux autres.

Cet événement est aussi bien destiné aux personnes
débutantes ne connaissant pas ou peu la culture S+,
qu’aux personnes souhaitant approfondir leurs connaissances
et la pratique des outils de consentement.

Il n'y aura pas de sexualité.
La sensualité sera autorisée (câlins, caresses non génitales, massages)
mais il est possible de vivre l'événement sans contact physique avec d'autres.

Rien ne sera obligatoire dans les ateliers proposés.

Pour en savoir plus et connaitre les modalités d’inscriptions, voici :

## [La présentation détaillée & inscription](2022-05-08-idees-couvrir-presentation.md)
## [Le bilan financier](2022-05-08-idees-couvrir-compta.md)
## [Les statistiques de l'événement]() (bientôt)

Pour prolonger l’expérience, l'après-midi idées'couvrir
sera suivie par une [soirée idées'connectées](2022-05-08-idees-connectees.md).

Nous espérons vous voir nombreu⋅x⋅ses à ces deux évènements.

La team’idées ❤
