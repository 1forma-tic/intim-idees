title: Journée aire'éthique, infos pratiques
-------------------
# \[intim'idées] Inscription acceptée pour la journée aire'éthique du 30 avril 2022
Bonjour,
Nous avons retenu ta préinscription pour la journée aire'éthique de samedi 30 avril, tu es bienvenue parmi nous !

Il ne te reste qu’à **nous confirmer ta participation par retour mail ou sms et nous indiquer de quelle façon tu vas nous faire parvenir ta participation financière**.

La compta détaillée sera disponible prochainement.
Pour consulter celle des événements antérieurs :
- [Bilan financier Laborizontale du 5 mars 2022](https://intim-idees.fr/2022-03-05-laborizontale-compta.html)
- [Bilan financier idées'couvrir du 9 avril 2022](https://intim-idees.fr/2022-04-09-idees-couvrir-compta.html)

Pour nous transmettre ta participation tu peux utiliser :

* un virement bancaire à l’IBAN : FR76 1090 7000 0105 8192 7634 740
* Paypal vers l’email [gammanu@free.fr](mailto:gammanu@free.fr) (en choisissant l’envoi à un proche pour éviter que paypal ponctionne des frais)
* Lydia vers le numéro 0770772770
* Nous l’amener en main propre dès que possible si tu nous connais déjà
* Nous l'amener en espèces sur place (à faire en dernier recours, histoire que nous ayons de la trésorerie)
  PS : tu peux tout à fait choisir de nous envoyer le minimum que tu avais prévu maintenant, et compléter à ta discrétion en fin d’événement si c’est plus confort pour toi.


**Horaire :**
Rendez-vous samedi à 9h pour faire ton auto-test, ou dès vendredi entre 12h et 22h pour aider à préparer le lieu.
A 9h45, nous aurions besoin de volontaires pour prendre le relais à l’accueil histoire que l’orga puisse se préparer pour démarrer à l’heure.
A 10h, nous fermons les portes et on commence le cercle d’ouverture.

**Covid :**
Pour limiter les risques covid lors de l'événement, vous pouvez au choix :
- faire un auto-test sur place en arrivant
- fournir le résultat d'un test covid à votre nom effectué après le 29/04 à 12h (donc vendredi après-midi, soir ou samedi matin avant de venir)

**L’adresse du lieu :**
26 Ter, rue André Lapelleterie, 33130, Bègles
L’entrée est entre le 24 et le 26, au fond d’une ruelle non goudronnée, au niveau du portail blanc avec bouteille en contrepoids (on fléchera le chemin depuis le tram).
L’arrêt du tram C “La belle rose” est à 200 mètres de la maison.
Si vous venez en voiture, difficile de se garer dans le quartier.
Le plus simple est de se garer dans les parkings proches des arrêts de tram Lycée Václav-Havel ou Villenave Pyrénées et de finir en tram. Si vous voulez quand même tenter votre chance à vous garer dans les ruelles proches de la maison, merci de garer quand-même vos véhicules de façon à faciliter la circulation dans la rue et la sortie des voitures de leurs garages.
Si vous souhaitez covoiturer, voici le [pad où s’auto-organiser](https://pad.aquilenet.fr/p/intim-idees-YLYrkil)

**Nourriture et boissons :**
Parmi les participant⋅e⋅s :
- 1 personne mange sans gluten
- 7 sont végétariennes
- 1 est végan
- 1 est allergique aux crustacés
- 1 est allergique ou intolérante aux produits laitiers
  Les plats prévus sont listés sur le [pad auberge espagnole](https://pad.aquilenet.fr/p/intim-idees-YLYrkil)
  N'hésites pas à venir aussi avec des boissons (sans alcool !), kéfir, kombucha, jus de fruit..., si possible en l'indiquant sur le pad !

**Tenue et parements :**
Viens paré⋅e de ta plus belle tenue colorée, où tu te sentes pleinement t'incarner toi-même : sens-toi sublime et rayonnant⋅e !
Les couleurs de Beltane sont toutes les couleurs de l'arc-en-ciel, avec une prédilection pour le rouge (l'amour), le blanc (la pureté), le vert foncé (l'abondance), le jaune (le soleil et l'été).
Notes que l'étage est sans chaussures, et que certaines activités sont prévues en extérieur, dans le jardin.

Dans la mesure du possible, **merci d'amener des fleurs !** Ce sera pour faire des bouquets ou pour confectionner des couronnes de fleurs ! Si tu en as dans ton jardin ou que tu peux t'en procurer sur le chemin, nous en seront ravi.e.s !

La créativité et la diversité dans l’expression de genre est bienvenue. Si tu n’es pas prêt⋅e à côtoyer des gens qui ne rentrent pas dans les cases “homme” ou “femme”, il est encore temps de te désister.
Des soutiens émotionnels pourrons t’aider à accueillir ce qui se passe pour toi si cela te perturbe.

**Composition du groupe :**
Pour la journée, sauf imprévu, nous serons entre 31 et 34 (dont 5 animataires, et 6 soutiens)
(Pour la soirée, nous équilibrerons davantage les genres)

* 11 à 12 femmes
* 17 à 19 hommes
* 3 non-binaires

Les âges s’étalent de 23 à 61 ans.

Pour 14 personnes, c'est un premier pas chez intim'idées (dont 2 animataires, et 2 soutiens)

Les [statistiques détaillées](https://intim-idees.fr/2022-04-30-journee-aire-ethique-stats.html) seront bientôt disponibles.
Il en va de même pour le [bilan financier](https://intim-idees.fr/2022-04-30-journee-aire-ethique-compta.html) de l’événement.

À très vite, et si tu as des questions, n’hésites pas !

La team’idées

–-

Notre site : https://intim-idees.fr/

Si tu ne veux plus recevoir d’email de notre part, réponds-nous STOP par email.
Tu peux même nuancer si tu veux certaines infos et pas d’autres !
