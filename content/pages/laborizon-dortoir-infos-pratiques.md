title: Dormir sur place à Laborizon
-------------------
# Dormir sur place à Laborizon

Si tu es accepté⋅e sur un événement ayant lieu à Laborizon,
nous avons suffisamment de place/matelas pour t'héberger.

En revanche, pense à prendre :
- sac de couchage (on n'a clairement pas assez que draps/couettes ou sac de couchage pour fournir)
- oreiller (on a quelques coussins en rab mais le stock est limité)
- nécessaire de toilette dont serviette (idem, on a un peu de rab mais pas illimité)
- bouchons d'oreille (en dortoir, il peut y avoir des ronflements)

Si vous êtes nombreux⋅se⋅s à dormir sur place, les matelas doubles seront partagés à 2.

Si tu as des besoins spécifiques ou des inquiétudes tu peux nous contacter : [**contact@intim-idees.fr**](mailto:contact@intim-idees.fr)
Ce n'est pas sûr que nous puissions y répondre, mais nous pourrons essayer.

Enfin, nous t'invitons à soutenir la vie du lieu qui t'héberge,
que ce soit en contribuant à la propreté du lieu, aussi bien qu'à ses frais.

Voici les participations suggérées par nuit en mode dortoir :
- PaF solidaire : 0 à 3 €
- PaF équilibre : 4 à 8 €
- Contribution soutien : 9 à 18 €
- Contribution justice sociale : 19 € et plus

Et par petit déjeuner :
- PaF solidaire : 0 à 1 €
- PaF équilibre : 2 à 4 €
- Contribution soutien : 5 à 10 €
- Contribution justice sociale : 11 € et plus

Tu peux aussi bien la payer à part directement auprès du lieu
que l'intégrer au montant pour les événements auxquels tu participes sur place,
intim'idées veillera à ce que le lieu ne soit pas déficitaire afin de pérenniser l'accueil d'événements sur place.

