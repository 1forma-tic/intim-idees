title: Soirée aire'éthique
-------------------
# Soirée aire'éthique

## Prérequis :

Idéalement avoir participé et à la journée aire'éthique et à un événement tel qu'[idées'couvrir](2022-04-09-idees-couvrir.md).
Si nous ne somme pas complet avec des participant⋅e⋅s correspondant à ces critères, ce qui est fort probable, nous étudierons les pré-inscriptions ayant au moins un des critères suivants :
- avoir participé à la journée aire'éthique
- avoir participé à un événement Sx+ sur le consentement
- être recommandé par des personnes du milieu Sx+ en qui nous avons confiance
- être recommandé par des personnes d'un milieu proche du Sx+ en qui nous avons confiance
- illustrer votre cheminement dans votre rapport au consentement,
  à l'autonomie affective et émotionnelle,
  ainsi que dans votre rapport à la sexualité,
  pour que nous puissions avoir confiance qu'en vous acceptant,
  vous passerez une bonne soirée, et celleux qui seront à votre contact aussi.

## Rappel :

- Durant cette soirée, la **nudité et la sexualité seront autorisées** dans la majorité des espaces.
  Si vous ne souhaitez pas y être exposé⋅e, ne venez pas à la soirée.
  Vous pouvez en revanche venir à la journée qui ne comportera pas de sexualité.
- Participer ne vous donne aucun droit sur le corp d'autrui,
  si vous venez pour avoir de la sexualité et que ça n'a pas lieu,
  il se peut que vous soyez **frustré⋅e et déçu⋅e. Vous êtes légitime à vivre cela.**
  En revanche, **hors de question de faire payer** votre frustration aux autres.
- Vous pourrez cependant demander du **soutien émotionnel**
  si vous passez des moments difficiles.


## Déroulé :
- 19h : repas en auberge espagnole
- 19h30 : accueil des éventuel⋅le⋅s participant⋅e⋅s qui n'aurais pas participé à la [journée aire'éthique](2022-04-30-journee-aire-ethique.md)
- 20h : fermeture des portes et début du cercle d'ouverture
- 20h30 : atelier consentement (Affirmation du non, gratitude pour le non, accueil de la frustration, consentement verbal et non verbal)
- 22h : Démystification/rationalisation du rituel
- 22h15 : Rituel / cérémonie de Beltane : célébration sur le thème de la fertilité pouvant mener à de la sexualité
  - ouverture de cérémonie
  - multiples intermèdes de chants
  - conte
  - méditation guidée : alignement entre terre et ciel
    apports sur le sens de Beltane et ses symboliques
  - tirage d'oracle
  - adoration
  - clôture de cérémonie

  Fil rouge : nourriture érotisable (salade de fruit, chocolat fondu, chantilly, glace)
- 00h : moisson du soir pour celleux qui souhaitent partir ensuite
- 00h30 : cercle de triage (co-création du programme nocturne en fonction des envies et propositions)
- 01h : priorité sommeil dans un des espaces pouvant faire dortoir
- 01h : buffet nocturne
- 11h le lendemain : moisson de la nuit et cercle de cloture
- 12h : rangement nettoyage et départ

## ✉️ Comment je m’inscris ? ✉️

Ça y est, le formulaire de pré-inscription est enfin disponible :

https://framaforms.org/pre-inscription-soiree-aireethique-du-30-avril-2022-1650990701

Nous avons pour l'instant un gros écart de parité dans les pré-inscription,
nous allons donc refuser des hommes pour équilibrer le groupe.
Quelque soit votre genre, vous pouvez vous pré-inscrire,
mais si vous êtes un homme et qu'aucune femme ne conditionne sa venue à la votre,
vos chances d'être accepté pour cette édition sont mince.

Nous aurons beaucoup à faire durant les 48 heures précédant l’événement.
Il n’est pas sûr que nous ayons le temps d’étudier ta pré-inscription si tu nous l’envoi seulement en dernière minute.
On fera au mieux.

Si vous rencontrez des difficultés pour remplir ce questionnaire, n’hésitez pas à nous contacter :
contact@intim-idees.fr ou par sms : 0 770 772 770
