title: Bilan financier Laborizontale du 5 Mars 2022
-------------------
# Bilan financier Laborizontale du 5 Mars 2022

Actualisé le : 06/03/2022

- Participations reçues : 700 € ([voir les statistiques de participation](2022-03-05-laborizontale-stats.md))
- Materiel / consommables : ~ 350€ (chiffre précis en cours de collecte)
  (dont ~ 30€ pour 15 auto-test)
- [x] 150€ de participation aux frais du lieu d’accueil (nourriture, eau, électricité, loyer, stockage du matériel, nettoyage des draps…). Tient également lieu de soutien à 3 des orgas habitant sur place.
- [x] 100€ de trésorerie, fond de roulement pour les prochains événements
- [x] 90€ de provisions pour investir dans du matériel pour les futurs événements
- [ ] 10€ de soutien à Framasoft (nous utilisons FramaForms, Framagit et Mobilizon)
- [ ] 10€ de soutien au CHATONS Cloud Girofle (qui héberge à prix libre notre forum Discourse)
