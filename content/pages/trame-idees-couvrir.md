title: Idées’couvrir, trame d'animation
---
# Idées’couvrir, trame d'animation

## Introduction :

Je suis Sam et je suis ravi de vous accueillir ici aujourd'hui pour ce premier atelier Idées'couvrir.

Tout au long de l'après-midi, nous allons vous proposer des exercices verbaux et corporels sur les thèmes du consentement et de la connexion à soi et aux autres.

Ce sont des exercices repris à l'identique ou inspirés de ce que font les autres collectifs, notamment sex-positif, que nous avons cotoyés.

Nous vous proposons une boite à outils qui peut vous être utile ou pas selon les situations.

#### Concernant les défaillances potentielles des Orgas et les biais d’aura et de pouvoir :

Nous ne sommes pas des gourous et nous ne sommes pas parfait, nous sommes toujours en apprentissage, nous avons fait des erreurs dans nos vies et il nous arrive d'en faire encore. Mais nous cherchons à en prendre conscience et travailler dessus. Nous sommes des êtres humains comme les autres, avec nos propres biais, fatigue, et problèmes. Nous sommes aussi faillibles que les autres participants. N’hésitez pas à intervenir si vous pensez qu’un Orga est dans l’erreur, comme vous interviendriez auprès d’un autre participant.

Ce n’est pas parce qu’un Orga vous dit de faire ou de ne pas faire quelque chose que vous devez obtempérer sans rien dire. Votre consentement s’applique face à un Orga. Vous avez le droit de demander à comprendre avant de faire ce qui vous est demandé. D’ailleurs, les Orgas essais le plus possible d’inviter les gens à faire des choses pour le bien du groupe.

Nous cherchons également à faire attention aux biais de pouvoir qu’il peut y avoir avec notre position.

Vous pouvez vous dire que pour être reconnaissant, être intégrer le mieux possible au groupe, être réaccepter aux prochains événements, que vous avez intérêt à avoir des interactions sensuelles avec les Orgas. Ne le faites pas. Ces raisons représentent ce que l’on appelle le biais de pouvoir et si ce biais est présent il est préférable de s’abstenir. Ne le faites pas tant que vous sentez qu’une de ces raisons ou une raison apparentée est présente dans un coin de votre esprit.

#### Concernant les rôles des différents orgas :

Au cours de cet évènement, Millicent et moi-même sommes les animataires de la majorité des ateliers.

Doriane sera la gardienne de l'espace (sonore) et du temps, et on va en avoir besoin pour nous cadrer toutes les deux avec Millicent.
Elle animera aussi quelques exercices/ateliers.

Nous avons également deux personnes en soutien émotionnel pour apporter de l’écoute,
du soutien émotionnel aux participant⋅e⋅s qui en auraient besoin.
Iels sont là pour recevoir la parole et les émotions d’autres participant.e.s
et un lieu calme est prévu pour recevoir du soutien si nécessaire.

Ces deux personnes sont Myrha qui est aussi référente consentement
et Loic qui est aussi référent déconstruction en plus du rôle d'hôte à qui s'adresser pour ce qui concerne le lieu.

#### Concernant le lieu, ils se compose de plusieurs espaces :

Il y a des priorisations et limites selon les espaces.
Par exemple, il n’y a pas de chaussures dans cette pièce
et il n’y a pas de boissons à part l’eau.

Merci de prêter attention aux affiches qui vous les indiquent.

Vous êtes ici dans l’espace de jeux où nous ferons les exercices
et où l’on passera la majorité de la journée.

Vous avez accès également en bas au séjour, à la cuisine,
à l’espace soutien (si vous souhaitez en recevoir ou vous isoler),
à la salle de bain
et aux toilettes qui sont sèches et donc nécessite une attention particulière.
Je laisse la parole à Loïc.

## Maintenant, nous allons faire un tour de présentation.

On vous invite donc à votre tour de parole d’indiquer :
- votre prénom ou pseudo,
- le pronom que vous souhaitez que l’on utilise (elle, il, iel, …)
- et en un mot, l'énergie dans laquelle vous arrivez (votre état émotionnel).

Myrha, écrira au tableau votre prénom ou pseudo et votre pronom
afin que nous puissions toustes nous en rappeler.

### Il existe des outils/signes/codes non-verbaux qui permettent de faciliter l’expression en groupe.

Qui peut nous rappeler un signe de parole en groupe ?

- Pour appeler le groupe au silence : On se tait et lève les deux mains.
- On lève la main pour intervenir : On indique qui lève la main en le montrant du doigt si la personne qui est en train de parler ne la voit pas.
- Index pointé on effectue une rotation des poignets : On en parlera ensemble après.
- Incise
- Mouvement de mains vers le haut / bas : demande à parler plus fort / moins fort.
- Bras en croix : on n’accepte pas ce qui se dit, opposition forte parce que l’on se sent blessé.
- Agiter les mains : pour applaudir / soutenir ce qui est dit.
- Cisaux
- Pianoter des doigts : envoyer de l’amour, des paillettes.
- Faire un geste vers soi : pour exprimer de l’empathie / que l’on est dans la même situation.
- Faire le moulin : Abréger, prise de parole trop longue.

## Maintenant nous allons faire un atelier que l’on a nommé le Brise-glace mouvant.

On va partager ensemble quelques affirmations, plus elle vous correspond, plus vous vous placez au fond de la pièce, moins elle vous correspond, plus vous vous placez à l’entrée de la pièce.

L’intention est de voir la diversité dans le groupe et que je ne suis probablement pas isolé.e dans certaines situations.

Si vous souhaitez vous exprimer sur la raison de vos placements, cela est possible. Plus particulièrement celleux aux extrémités ou isolé⋅e⋅s.

Affirmations :

• Je me sens anxieu⋅x⋅se

• Je me sens nouvelleau dans le Sx+ ?

• C’est difficile pour moi d’identifier mes envies ou désirs

• C’est difficile pour moi de poser mes limites

• C’est difficile pour moi d’être à l’écoute et de respecter les limites des autres

• Je viens partager mes MST

• Je me sens intimidé⋅e par l’intégration au groupe

• Je viens tisser des liens

• Je viens apprendre

• D’autres affirmation ?

## Pour le prochain atelier, nous allons faire des lignes glissantes.

C’est-à-dire, que nous vous invitons à former 2 lignes en face à d'une personne de l'autre ligne.

Vous pourrez vous asseoir ou rester débout selon les exercices et faire un exercice avec cette personne.

À la fin de l'exercice, nous inviterons une des lignes à se décaler d'une place vers la droite. Vous pourrez alors faire l'exercice suivant avec une nouvelle personne.

## Écoute foireuse

Maintenant que vous êtes en binômes,
nous vous proposons un temps expression et d'écoute qu'on espère hors du commun.

Nous allons poser une question
celleux face au mur du fond la répètent à leur binôme qui y répond.
Au bout de 2 minutes, nous vous inviterons à échanger vos rôles.

Pour cette question uniquement,
le jeu va consister pour la personne qui écoute
à bailler, soupirer, regarder ailleurs,
réagir en coupant la parole, faire un air dégouté, dénigrer...
bref, déranger la personne qui parle.

Pour rappel, vous pouvez mettre vos bras en croix si c'est trop pour vous,
et donc tant que ce n'est pas le cas, lachez-vous, la suite n'en sera que meilleure.

### La question est : Peux-tu me raconter un hobby ou une passion ?
(ou une anecdote affective pas trop impliquante vu le style d'écoute qu'elle va recevoir).

Temps écoulé, échangez vos rôles !

Je vous invite toustes à vous déplacer d’une place vers votre droite.

## Écoute attentive

### Méditation respiratoire
Avant de passer à l’exercice lié à celui que vous venez de faire,
je vous propose une petite méditation respiratoire
pour faire redescendre la pression que vous avez peut-être ressentie.

Êtes-vous bien dans votre position actuelle ?
Si ce n'est pas le cas, pouvez-vous ajuster quelque-chose ?
Je vous invite maintenant à fermer les yeux
et à vous recentrer sur vous-même.

Nous allons prendre trois grandes respirations ensemble.

1. La première en silence, en remplissant les poumons par le thorax puis par le ventre, et expirez par le nez.
2. La deuxième inspiration par le ventre puis le thorax, et expirez par la bouche avec un son harmonieux
3. La troisième, inspirez comme bon vous semble, et expirez avec bruits et mouvements.

### Check émotionnel

Maintenant, nous allons repartir sur une question en binôme, mais l'écoute sera différente :
lorsque l'un.e des deux parle, l’autre écoute attentivement, en silence
sans rebondir sur ce qui est dit.
C'est votre temps, votre espace, pour vous exprimer et être écouté⋅e.
Ce temps est pour vous quoi que vous en fassiez.

- Parfois des choses peuvent émerger après 1 minute de silence et c'est OK.
- Vous pouvez pleurer, être traversé⋅e par toutes sortes d'émotions et de malaise, c’est OK.
- L’apprentissage du consentement, en prenant la responsabilité de ses propres émotions
  et seulement celles-là, c’est aussi apprendre à rester avec de l’inconfort, de la frustration,
  ou de décevoir, de ne pas répondre aux attentes.

### La question est : Comment ça va, vraiment ?
En partant de l'instant présent et des ressentis corporels et émotionnels, sans s'y enfermer.

Je vous invite à terminer votre phrase /o\ puis à changer de rôle

Je vous invite à terminer votre phrase /o\

## Moisson (débrief)
Et maintenant, une petite moisson (débrief).

- Avez-vous appris quelque-chose de cette experience ?
- Le premier type d'écoute était-il agréable ?
- Le second ?
- Si vous aviez eux en tête le biais d'autorité lors du premier temps d'écoute,
  vous le seriez-vous infligé ?
- Si oui avec quelle intention ? Et la conscience de cette intension aurait-elle changé votre experience ?
- D'autres retours concernant ces derniers exercices ?

## Pause 10min
Maintenant, nous allons faire une pause d’environ 10 minutes.

(ici ou après présentatin du cadre selon l’heure et l’état des participant.e.s)

## Le cadre
Maintenant nous allons vous présenter le cadre du reste de l’évènement.

Déjà, vous l’avez peut-être remarqué, mais nous cherchons à utiliser un langage inclusif et à utiliser le féminin neutre. Ex : vous êtes toustes magnifiques /vous êtes toutes belles.

Nous vous invitons à profiter de l’occasion de cet évènement pour apprendre à mieux vous écouter et à faire attention à votre consentement. C’est le sujet !

Faites les choses pour vous, pas pour les autres ; et prenez soin de vous en priorité

Vous pouvez aller vous restaurer ou allez aux toilettes quand vous le souhaitez, même s’il y a des moments de pause pour cela.

Vous pouvez vous rester en retrait d’un exercice sur une partie et être plus actif⋅ve à d’autres moments.

Vous pouvez vous retirer d’un exercice à tout moment et même quitter l’événement à tout moment.

Les rires, les larmes et toute autre forme d’expression émotionnelle sont bienvenues. Vous êtes libre d’exprimer toutes les émotions qui vous traversent. C’est Ok de sentir et d’exprimer de la vulnérabilité. On aborde des sujets pas évidents, et on peut reconnaître en soi des choses pas évidente (déconstruction).

Cet évènement n’a pas de visée thérapeutique, l’expression de soi, le soutien mutuel et le partage ne sont pas proposés dans un but de soigner mais plutôt d’expérimentation de faire autrement. Cela n’exclut pour autant pas des effets thérapeutiques occasionnels.

Ce type d’évènement n’est jamais 100% sûr. Tout est fait par les organisataires pour le rendre aussi sécurisant que possible mais le risque zéro n’existe pas.

Participer, c’est aussi accepter de prendre la responsabilité de ta sécurité et de l’expression de tes limites. Et nous proposons plusieurs outils de communication pour faciliter votre capacité à prendre soin de vos limites.

La nudité partielle est autorisée (mais en aucun cas obligatoire) car l’ensemble des participant⋅e⋅s à exprimer son accord.

Cet événement est sans sexualité mais il peut y avoir de la sensualité, notamment les contacts avec la bouche sont bienvenus.

Comme c’est un lieu et un moment où vous pouvez montrer de la vulnérabilité, il est pour nous indispensable de respecter une certaine confidentialité.

Donc, pas d’enregistrement (audio, photo, vidéo) pendant l’événement.

Si vous souhaitez parler de l’événement en dehors, prenez soin de ne pas dévoiler les noms d’autres participant⋅e⋅s ou d’information permettant de les identifier.

Les personnes avec lesquelles vous interagissez pendant l’événement n’auront peut-être pas envie d’en reparler ou de partager certaines interactions avec vous en dehors. Nous vous invitons à être vigilant⋅e aux limites potentielles après l’événement aussi.

De façon générale, nous vous invitons à faire de votre mieux pour incarner les valeurs du collectif : l’écoute de soi et de ses émotions, l’ouverture aux autres, le consentement explicite et enthousiaste, le non-jugement, la liberté et la bienveillance.

Il est difficile d’intégrer tous les codes dès le début, et si cela met en difficulté le groupe et la qualité d’expérience que nous souhaitons offrir à vivre aux participant⋅e⋅s, nous pourrons décider d’exclure les personnes dont l’acculturation nous demande une énergie préjudiciable au reste du groupe. Dans ce cas, nous leur indiquerons les préalables qui nous sont nécessaires pour les réintégrer lors de prochains évènements, ou lors d’événements avec d’autres curseurs d’inclusivité.

Il y a un rôle Gestion de conflits et pratiques restauratives dans l’équipe d’orga qui peut être sollicité pour prendre soin de tensions, conflits, désaccord ou bris de consentement qui pourraient avoir lieu avant/pendant/après l’événement, que ce soit entre participant⋅e⋅s, entre participant⋅e et orga, ou entre orga. N’hésite pas à nous solliciter !


## Connections

*En lignes glissantes*

Je vous invite maintenant à vous remettre en lignes
puis à répéter un exercice d'eye contact avec des personnes différentes.

x5

    • 1 min eyes contact
    • + 30 sec invitation à tenir les mains
    • + invitation à remercier pendant 30s l’autre (signe, calin, hélicobite (sans sexualité) )





## Réapprendre le NON

*En lignes glissantes*


• Intention : savoir dire et faire respecter son NON quand l’autre n’est pas coopératif (self défense / enpuissancement)

• Affirmer son NON ! jusqu’à ce qu’il soit entendu et respecté 3'
Vous pouvez restez que sur de verbale, dans un premier temps, puis y mêler de la gestuelle non-verbale, essayer que du non verbale.
• Idem en inversant les rôles 3'
• Glisser vers la droite


• A exprime une demande à B qui répond toujours NON, A répond « merci pour ton nom/de prendre soin de toi/de me le dire… ». Invitation à graduer vos propositions dans un sens ou dans l’autre. Pas besoin d’exprimer de vrai désir. 2'
• Idem en inversant les rôles 2'
• Glisser vers la droite


## Débrief
Avez-vous des retours concernant ces derniers exercices ?


## Le consentement

Dans un idéal de consentement Conscient, Révocable, Libre, Authentique, Spécifique :

- • Si tu reçois un “non”, l’invitation est de remercier la personne qui s’est écoutée

- • Observer une personne ou un groupe est aussi une interaction qui impliqué le consentement des personnes regardées

- • Pour rejoindre un groupe, invitation à t’assurer du consentement explicite de chaque personne du groupe (avec des outils pour éviter le sentiment de pression de groupe)

- • Être attentif⋅ve aux biais de consentement : ⋅ les rapports de pouvoir réels ou ressentis (liés au statut, à l’expérience, au genre, à l’âge, à l’introversion/extraversion, à la posture orga/participant⋅e…) ⋅ l’effet de groupe ou le rapport à soi (se sentir inclus⋅e, faire comme les autres, se prouver quelque chose…) ⋅ les biais liés aux relations antérieures (avoir déjà vécu une interaction similaire avec quelqu’un⋅e, avoir une relation intime ou amoureuse, avoir déjà dit oui précédemment…)




●	Consentement. (10 min) 8,5
- ○	CONSCIENT : Le consentement doit être en conscience et éclairé, ainsi il faut donner et avoir toutes les informations en pleine transparence
- ○	AVANT : Le consentement se demande à priori, pas après ! Vous devez demander AVANT de faire quoi que ce soit, où que ce soit, avec qui que ce soit. Il est de la responsabilité de tou.te.s de favoriser la communication
- ○	LIBRE : Un oui n’est valable que s’il est donné sans pression, ni soumission à un biais (dynamique de pouvoir, pression de groupe, rareté, relationnel, attente, environnement incitatif, escalade d'engagement…)
- ○	INDIVIDUEL : Il n'y a que vous qui pouvez donner votre consentement et vous devez ne prendre en compte que ce qui est juste pour vous. Le consentement se donne et se demande individuellement à chaque interaction.
- ○	BIENVEILLANT : Soyez bienveillant.e et juste avec vous-même et avec les autres. Ne vous forcez à rien ! Accueillez les refus en remerciant la personne de s’écouter et de faire uniquement ce qui est juste pour elle.
- ○	RÉVOCABLE : Vous avez le droit de changer d'avis aussi souvent que vous le sentez. Le consentement peut toujours être révoqué à tout moment ! Vérifiez régulièrement verbalement et/ou non verbalement si c'est toujours bon pour vous et pour l'autre.
- ○	ENTHOUSIASTE : Si vous pensez "peut-être", c'est un "non". Si vous ne savez pas, c'est un "non". Si vous n'êtes pas sûr, c'est un "non". Si vous vous sentez obligé.e.s, c'est un "non".
- ○	SPÉCIFIQUE : Le consentement se donne pour une action ou un type d'interaction particulier. Vous devez demander de nouveau quand vous changez d'actions.
- ○	Pas élargir le cadre (subspace…)


### Cartographie du consentement


<!--
Check émotionnel 5'/question
• B : Quelles sont tes limites (voire peurs) concernant ces événements et tes interactions avec les autres ? Contacts, touchés, pratiques, marques d'affections, ...
-->






## Biais

C'est quoi les biais ?

et les conflits d'intérêt ?


<!--
Intelligence collective < 1h

BRAINSTORMING

Préciser qu’on va prendre environ une heure pour prendre du recul, discuter autour du
consentement, des conditions qui permettent des interactions saines, de ce qui rend
complexe même avec de la bonne volonté d’avoir uniquement des interactions consenties et
enthousiastes et de ce qui peut favoriser ça malgré les biais.
Préciser qu’un document leur sera partagé après l’atelier qui synthétise une partie de ce
qu’on va partager dans ce temps de discussion/réflexion.
Consentement et Outils - Doc post-atelier
Création de sous-groupes de 4-5 3’

Invitation à discuter en sous-groupes autour de 3 sujets. A chaque fois, on va prendre 8
minutes de discussions. Puis 3-4 minutes de partages en pop-cor (sinon c'est trop long).
Et compléter nous-mêmes avec quelques pistes si ça nous semble pertinent.
Possibilité en changeant de question de changer de groupe pour celleux qui préfèrent
papillonner, en checkant le consentement du groupe. Invitation à laisser la parole de
manière relativement égale à chacune.

-->
Sujets abordés :
- C’est quoi le consentement et quelles sont les conditions qui favorisent le
  consentement ?
- Qu’est-ce qui peut biaiser le consentement dans mon expérience ?
- Quelles pratiques m’aident à vivre des interactions enthousiastes, saines et
  justes pour moi et les autres ?
  C’est quoi le consentement et quelles sont les conditions qui favorisent le
  consentement ? 15’
  Le consentement, c'est l'accord éclairé qu'une personne donne à son / sa / ses partenaires pour
  participer à une activité.
###  Nuances et zones grises :
- Défaut de consentement
- Bris de consentement / Agression sexuelle (“Constitue une agression sexuelle toute atteinte
  sexuelle commise avec violence, contrainte, menace ou surprise ou, dans les cas prévus par la
  loi, commise sur un mineur par un majeur.”)
- Viol (“Tout acte de pénétration sexuelle, de quelque nature qu'il soit, ou tout acte bucco-génital
  commis sur la personne d'autrui ou sur la personne de l'auteur par violence, contrainte,
  menace ou surprise est un viol.”)
- Volontaire / involontaire

### Type de biais :
###  Les biais de pouvoir :

La sexualité se pratique dans des contextes sociaux précis. Les rapports de pouvoirs intrinsèques à la situation peuvent influencer la capacité à consentir et respecter le consentement de manière éclairée.

Quelques exemples

Le patriarcat crée directement un rapport de pouvoir des hommes sur les femmes, qui sont éduquées à agir avec des codes impliquant de nombreux biais de consentement. Par exemple, les femmes éduquent à prendre soin, à être gentille, à sourire, à flatter. Les hommes à être plus “rentre dedans”, à insister, à guider.

##### Biais de légitimité avec une personne expérimentée :
En étant avec une personne que je considère comme plus expérimentée, par exemple dans le milieu sex positif, je peux faire plus confiance à l’autre qu’à moi et considérée qu’iel sait mieux que moi ce qu’il “faut” faire et comment.

##### Biais d’aura :
En posture de formataires, psy, animataires, coach, accompagnant.es, on peut être source de projection et de fantasme qui créént des bais de consentement.

##### Le/la supérieure hiérarchique :
Peu importe le contexte, un.e supérieur.e hiérarchique a d’office un ascendant, et en lui exprimant des limites, on peut craindre les conséquences qu’il y aura par ailleurs.

### Les biais de relation

##### L’attrait romantique / amicale :
le désir de plaire peut nous faire faire des choses dont on a pas réellement envie

Désir de plaire, attrait romantique

##### Le biais d’attirance ou de correspondance aux canons de beauté :
On va plus facilement dire “oui” à quelqu’un qui correspond aux canons de beauté car c’est valorisant d’être perçu.e avec. On va plus facilement chercher à correspondre à des codes liés aux canons de beauté même lorsque ce n’est pas intrinsèquement juste pour soi pour la même raison.

##### Le besoin de reconnaissance :
Si je sais qu’en faisant certaines choses je peux être plus reconnue socialement, cela peut biaiser mon consentement

##### Le besoin d’appartenance :
Si un groupe de personnes fait la même chose ( par exemple se met nu), je peux ressentir une pression à le faire pour faire partie du groupe, pour ne pas me sentir seule ou par peur d’être exclue.

##### La peur de la confrontation, du malaise, de rendre triste, de blesser :
La peur de générer de l’inconfort chez l’autre peut rendre difficile de dire non ou stop

##### Les habitudes et automatismes :
je peux avoir l’habitude de certaines pratiques avec une personne, et ne me doute pas que ça ne conviendra pas du tout à une autre personne. A l’inverse, si j’ai l’habitude d’interagir d’une certaine manière avec une personne, je peux partir du principe qu’elle est toujours consentante pour cela, alors que ces envies peuvent changer avec le temps.


### Biais de groupe

### Autres biais

##### Le biais de cohérence :
Vous connaissez l’idée du pas dans la porte ? Quand une personne s’est engagé par exemple par un câlin, le besoin d’être cohérent peut la pousser à accepter d’autres choses alors qu’elle n’en a pas envie. Ou si elle a accepté une fois.

##### Les scénarios appris :
De par notre culture, les films, les histoires etc. on connait toustes le ce fameux scénario : regard > rapprochement > bisous > caresses > “préliminaires” > Coït > orgasme masculin > fin.

Si rien ne vient stopper l’enchaînement de ces actes, il semble convenu d’office qu’ils s’enchaînent de cette manière. Pourtant, cela ne correspond pas forcément aux désirs du moment.

##### L’éducation :
Notre éducation nous a globalement appris à obéir, à dire oui, à éviter le non comme pouvant blesser l’autre ou créer du conflit, ce qui rend pour de nombreuses personnes beaucoup plus facile de dire oui que de dire non.

##### Etats modifiés de conscience :
Certaines substances, émotions, pratiques peuvent réduire notre capacité à consentir.

##### La sidération :
En état de choc, le cerveau peut bloquer les processus psychiques qui empêchent la personne de penser à l’évènement ou d’y réagir. Il est précieux de vérifier régulièrement,


### conclusion biais

Tous ces biais peuvent s’imbriquer et se cumuler.
Ils font partie de nous et de la vie sociale.
L’idée n’est pas d’en avoir peur et de ne plus oser interagir pour autant,
mais d’en avoir conscience, d’y être vigilant.e et de les prendre en compte dans nos manières d’interagir.




### Endo-consentement

Introspection guidée et partage 20’

● 2 ‘ Choisir un souvenir où j’ai dépassé mes limites, je n’ai pas exprimé ma limite et ça a créé de l’inconfort (un inconfort à entre 4 et 6 sur 10 plutôt qu’un inconfort traumatique)

● 5’ Méditation guidée :

- Qu’est-ce que je ressens quand mes limites ne sont pas respectées ? Quand je ne trouve pas l’espace de les poser ? (dire à voix haute les yeux fermés)

- Qu’est-ce qui a fait que je n’ai pas su poser mes limites ?

- Revoir la scène : qu’est ce que je pourrais faire différemment ?

- Je reviens dans le présent

● 3’ Réflexion solo :

- Dans ma propre expérience, qu’est-ce qui m’amène à ne pas respecter/poser mes limites ?

- Qu’est-ce que je peux faire dans ces cas-là ? Prochain pas à mettre en place pour me soutenir dans ces moments là

● 10’ Partage à 2 de 5’ chacun.e : Dans quelles situations j’ai tendance à ne pas respecter ce qui est juste pour moi ? Qu’est-ce que je pourrais essayer de mettre en place par rapport à ça ?

### Check émotionnel ?

Maintenant, pour cette question lorsque l'un.e des deux parle, l’autre écoute en silence et ne rebondis pas. C'est est votre espace.

• B : Quelles sont tes limites (voire peurs) concernant ces événements et tes interactions avec les autres ? Contacts, touchés, pratiques, marques d'affections, ...

## Exo consentement verbal P2

** inclus roue du consentement light**

Mise en situation d’exemple (faire jouer à deux volontaires)

Par groupe désignez une personne Actaire et une personne Réceptaire.

L’actaire va faire une sollicitation au réceptaire à chaque tour.

Une sollicitation est une demande précise composée à minima d’une action et d’une zone précise, l’on peut être encore plus précis en donnant une indication de rythme, d’intensité, etc...).

Le réceptaire peut soit refuser, soit négocier l’action ou la zone, soit accepter.

L’actaire va réagir à la réponse du réceptaire. En cas de refus, iel remercie. En cas de négociation, iel peut refuser, accepter ou re-négocier.

Exemple :

Actaire : “Est-ce que je peux te masser les épaules doucement et en appuyant fort ?”

Réceptaire : “Je préfères que tu me masses les épaules sans appuyer fort.”

Actaire : “Ok. Faisons-ça !”

• Dédramatiser la posture permettre (ok pour l’intéraction par enthousiasme pour le lien, vs enthousiasme pour l’action) 1'

• A exprime une demande à B qui répond préférentiellement OUI MAIS + ajustement, A peut faire une contre-proposition… jusqu’à accord ; mais ne fait pas l’action (pour expérimenter la frustration) 3'

• Idem en inversant les rôles 3'

• Glisser vers la droite

• A demande à B d’agir sur A de tel manière, au bénéfice de A, B répond ce qu’iel veux (oui/non/ne sais pas/peutêtre/la réponse D) avec ajustements au besoin et mise en pratique. 3'

• Idem en inversant les rôles 3'

• Glisser vers la droite

• A demande à B de permettre une action spécifique de A, au bénéfice de A, B répond ce qu’iel veux (oui/non/ne sais pas/peutêtre/la réponse D) avec ajustements au besoin et mise en pratique. 3'

• Idem en inversant les rôles 3'

• Glisser vers la droite

• Intention : consentement verbal hyper spécifique (petit bonhomme qui se promène, félinade)

• Est-ce que je peux « touché spécifique » ? Oui franc → faire, Oui hésitant → es-tu sur ? (recheck), Non → autre proposition jusqu’à fin du temps, Stop → arrêter d’interagir. 3'

• Idem en inversant les rôles 3'

## Débrief

Avez-vous des retours concernant ces derniers exercices ?

## Exo consentement non verbal

• Présentation des outils de communication non-verbal :

Décomposition des différents stades d’une interaction avec 1 actaire et 1 receptaire.

Sollicitation :

Selon le contexte plusieurs précautions peuvent être prises.

Contact visuel yeux dans les yeux.

Mouvement lent pour pouvoir être facilement esquivable.

Contact avec le dos de la main ou contact sur une partie non sexualisé du corps pour être moins impliquant.

Vérification :

L’actaire à la main posée et immobile sur la partie du corps interrogée, il opère un tapotement de l’index jusqu’à réponse du réceptaire.

Acceptation :

Le réceptaire caresse la main qui effectue la vérification.

Refus :

Le réceptaire enlève la main de l’actaire pour soit posée la main de l’actaire dans une zone plus neutre, soit la posée au sol.

Négociation (redirection) :

Le réceptaire pose sa main, sans l’attraper sur celle de l’actaire pour la guider doucement. Il est important d’être délicat pour que le consentement de l’actaire soit préservé.

Arrêt immédiat de l’interaction :

Le réceptaire opère une double tape sonore soit sur le corps de l’actaire, soit sur soi ou sur le sol.

• Sollicitation en aveugle. En groupe. 6'

Deux cercles : Interne, fixe / externe, tourne (d’un ou plusieurs à chaque fois)

L’exercice se fait dans le silence. Les participant.e.s se séparent en deux groupes, un groupe actaire et un groupe réceptaire. Le groupe réceptaire, s’installe en cercle tourné vers l'extérieur et les yeux bandés.

Le groupe actaire se mélange et chaque actaire va se mettre en face d'un réceptaire.

Il s'agit surtout de travailler la sollicitation donc nous invitons les actaire à prendre particulièrement attention à cette étape là. Les réceptaires à se connecter à eux même et à tâcher de ne pas regarder qui les sollicites, à capter les informations autrement qu’avec la vue et l'ouïe.

1ére phase : 6 tours de 30 secs.

2éme phase : On échange les rôles.

3ème phase : Débrief (3 min)

arbre/papillon pour la soirée)

## exo désir sans réponse :

• est-ce que je peux t’exprimer un désir ?

• si oui : affirmation du désir

• réponse : merci

• charge à l’autre de revenir s’il souhaite concrétiser.

exo connexion lien émotionnel (thème consentement),
par exemple endo-consentement licorne curieuse → invitation restitution avec quelqu’un du même genre
ou avec qui vous vous sentez en sécurité/confiance pour partager votre vulnérabilité

adoration

intégrer un groupe/consentement collectif

ambulant partage des désirs par frôlement de main

Déambuler, en se frôlant la main pour manifester notre intérêt aux personnes croisées. (1min)

Aller dire à des personnes nos envies d’activités avec elles. On ne réponds que “merci d’avoir partagé ce désir avec moi” en restant neutre. Si on a envie de répondre par une autre proposition (ou la même), on laisse au moins une interaction avec une autre personne.

groupe de 3, personnes que l’on a envie de découvrir. Chercher un compromis dans les envies que l’on a à 3. Si on arrive à un compromis à 3

Effectuer l’action.

Je vous invite à négocier au moins un peu, au moins pour la forme, mais si c est une vraie négociation c’est mieux.

Si on n’a pas ou plus envie on utilise le non verbal ou non-verbal des derniers exercices. On dit merci à un non et on n’insiste pas derrière un non.

Quand on a fini une phase de négociation puis réalisation on peut passer à une autre phase surtout si l’envie avait été porter par une personne auquel cas ça nous permet de changer de demandeur ou décider de changer de groupe.

brise glace pour jeux’thèmes ou aire’éthique : se classer par croyance en tel sujet/privilège/avarice/gourmandise/fénéantise/luxure… (un critère qui implique du dialogue)

• Quel a été mon dernier moment de joie ?

• Comment je me sens connecté à mon sexe ?

• Quels sont mes complexes sur mon corps et comment lui envoyer de l’amour ?

• RBDSM-PB

Option : check émo et exo endoconsentemement intégrés en duo à la roue/file glissante

Ou 2 files glissantes pour que tout le monde se rencontre si peu de monde

• D : Quels sont tes désirs concernant cet évènement ou les interactions avec les autres ? Connections, de faire, que l'on te fasse, pour prendre soin de toi, ...

• B : Quels sont tes biais que tu as pu identifier qui peuvent t’influence lors de l’évènement et des interactions avec les autres ?

• R : Quelles sont tes relations actuellement ? Types, fonctionnements, accords, ...

• S : Comment prends-tu soin de ta santé et de celle des autres? IST connues chez toi ou chez un.e partenaire, dates des derniers tests IST, moyens de protection et contraception, risques pris, ...

• M : Quelles sont les significations

intention Garder un lien contact, quelle type

sens de cette relation attentes, envies et objectifs

● Exercice en déambulation (14 minutes)

En déambulation on sollicite une autre personne (laisser déambuler un peu)

Etape 1 : "Est-ce que tu veux faire l'exercice avec moi?" La réponse est toujours non. On remercie le non. ( 3 min) 1

Etape 2: On peut dire oui ou non à la sollicitation et on effectue pas l’action. On remercie le non (3 min) 1,5

Etape 3 : On peut négocier et faire l’action. On se remet en déambulation une fois l’action faite ou qu’on a accueillit le non. (5 min)1,5

Etape 4 : Débrief (3 min)

D/ Intégrer un groupe (30 min)

● Exercice en groupe de trois :

Action à 3

Explication code couleur + consentement de groupe = OK de toutes les personnes impliquées, même pour regarder

Phase 2 : Tout le monde reste en groupe de 3. Hormis deux groupes qui vont faire les électrons libres. Ces électrons libres doivent partir vers un autre groupe et demander la couleur de chacun des membres du groupe. Puis demander à intégrer le groupe ou aller voir un autre groupe et faire un autre check couleur. En cas d’intégration du groupe il y a redistribution des rôles, et départ d’un des membres. On doit toujours rester par 3. (15 min)

Phase 3 : Débrief (3 min)

“Je vous invite à prendre un moment pour vous recentrer sur l’instant présent, de laisser derrière vous les soucis du quotidien. Puis à prendre par les mains de vos voisins et de vous offrir le temps et l’espace pour vous connecter avec ce groupe que nous formons, et avec lequel nous évoluerons durant ce week end. Prenons le temps de prendre trois profondes respirations à l’unisson pour nous connecter les uns les autres.”
