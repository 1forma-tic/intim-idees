title: Liste des bilans et prévisionnels financiers
-------------------
# Liste des bilans et prévisionnels financiers


<% pages.forEach(page=>{
if(page.fileName.split('-compta.').length < 2) return;
const tmp = page.fileName.split('.html')[0].split("-");
tmp.pop();
const Y = tmp.shift();
const m = tmp.shift();
const d = tmp.shift();
const name = tmp.length===2 ? tmp.join("'") : tmp.join(" ").replace(/ ([^ ]+)$/,"'$1");
_%>
- [<%=name%> du <%=d%>/<%=m%>/<%=Y%>](<%=page.fileName%>)
<% }); _%>
