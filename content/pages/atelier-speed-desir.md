title: Speed Désir
-------
# Speed Désir (Atelier 15 à 30 minutes)

**Intention :** Prenez la température des possibles avec chaque participant⋅e (rien ne vaux comme promesse)

**Durée :** 5 minutes + 2 minutes par participant⋅e⋅s

**Comment :** Par binôme tournant :

10s : 1 respiration en se regardant, 1 respiration les yeux fermés
50s : A exprime ses désir à B, B prend une respiration les yeux fermé puis répond par un chiffre* comment c’est de recevoir ça ? Et quel enthousiasme à concrétiser ?
10s : 1 respiration en se regardant, 1 respiration les yeux fermés
50s : B exprime ses désir à A, A répond par un chiffre de -10 à 10 : comment c’est de recevoir ça ? Et quel enthousiasme à concrétiser ?

## Échelle :
10 : C’est la plus belle chose que j’ai entendu / Je ne pouvais rêver mieux, on fait ça juste après !
5+ : Emu⋅e merci ! / Enthousiate
1 : Ok, cool / Pourquoi pas sans enthousiasme
0 : neutre
-1 : légèrement triste déçu⋅e d’inspirer ça / pas inspiré⋅e pour l’instant, probablement pas pour l’év.
-5 : déception, léger dégout  / y’a pas moyen (probablement jamais)
-10 : choqué⋅e, furieux⋅se, trigger, essentialisé⋅e, objectifié⋅e… / N’y pense même pas

## Gestion des tour/phases d'échange

Chaque personne de la première ligne parle avec celle en face dans la seconde. Si personne, attendre là phase suivante.

- Si pair (exemple avec 6)

|phase 1|phase 2   |phase 3|phase 4   |phase 5|phase 6   |
|-------|----------|-------|----------|-------|----------|
| 1 2 3 | - 1 2 3 | 6 1 2 | - 6 1 2 | 5 6 1 | - 5 6 1 |
| 6 5 4 | 6 5 4 - | 5 4 3 | 5 4 3 - | 4 3 2 | 4 3 2 - |

- si impair (exemple avec 5)

|phase 1|phase 2|phase 3|phase 4|phase 5|
|-------|-------|-------|-------|-------|
| 1 2 3 | 1 2 3 | 5 1 2 | 5 1 2 | 4 5 1 |
| - 5 4 | 5 4 - | - 4 3 | 4 3 - | - 3 2 |
