title: Jeux'thèmes
-------------------
![bannière d'illustration](img/bannieres/jeux-themes.jpg)

# Soirée jeux'thèmes

## Objectif :

Faire réfléchir de manière ludique et pratique sur des thèmes qui nous tiennent à cœur chez intim'idées,
puis au travers de ces expériences ludique, mener à des prises de consciences et des changements de comportements.

## Présentation de la soirée :

Le concept :
Après un temps d'auberge espagnole, commence une soirée légère¹ et ludique
avec des jeux choisis pour explorer certaines thématiques spécifique à chaque édition.

¹ légère dans notre référentiel avec son organisation simplifiée et son format court.
Ceci dit, les thèmes abordés ne sont pas forcément léger
et les remises en question qui peuvent en découler non-plus.
C'est pourquoi, comme à chaque événement intim'idées, du soutien émotionnel est à disposition
pour accueillir et traverser les vécus inconfortables dans de bonnes conditions.
C'est un filet de sécurité que nous proposons
pour aider à vivre le moment avec authenticité, en osant être vulnérable
et sortir de sa zone de confort.

### ⏱ Horaires et déroulé ⏱
- 19h : **Auberge espagnole** (sans alcool)
  où chacun.e apporte quelque chose (pour un menu varié,
  [voici un pad pour vous auto-organiser](https://pad.aquilenet.fr/p/intim-idees-auberge-espagnole-YLYrkil).
- 20h : Mini présentation et début des animations.
- selon les jeux, l'ambiance et la fatigue des animataires, fin des animations entre 22h et minuit pour continuer en auto-gestion.
- fermeture du lieu entre 23h et 2h du matin, selon l'ambiance et les contraintes des habitant⋅e⋅s.

### 💶 Et combien ça coûte ? 💶

Pour qu'organiser, animer et héberger cet événement et les suivants reste joyeux et enthousiaste,
nous avons besoin de soutiens et d'encouragements.
Nous serions heureu⋅ses⋅x d'entendre ce que l'événement vous a apporté,
mais aussi d'avoir votre aide pour couvrir nos frais.
À cet effet nous proposons une participation à **prix libre**.
Pour savoir combien mettre vous pouvez vous appuyer sur notre [budget prévisionnel](budget-previsionnel-jeux-themes-compta.md),
les [bilans financier des précédentes éditions](liste-compta.md)
ou sur les ordres de grandeurs que nous vous suggérons ici :
- PaF solidaire : 0 à 5 €
- PaF équilibre : 6 à 10 €
- Contribution soutien : 11 à 20 €
- Contribution justice sociale & soutien des futurs évènements : 21 € et bien au dela !

À quoi va servir cet argent ? À acheter ou fabriquer de [super jeux](liste-jeux.md)
à vous faire découvrir dans les prochaines éditions jeux'thèmes ainsi qu'à soutenir le lieu d'accueil et les orgas.

Même si ça ne nous facilite pas l’organisation, tu peux mettre un “?€” si tu préfères garder ta participation anonyme.
Tu peux aussi indiquer une fourchette de prix si tu préfères décider en fin d’évènement combien mettre.

### 😷 Informations COVID 😷

Pour prendre soin des enjeux de santé virale comme mentale et psychologique
ainsi que des enjeux politique de préservation des libertés individuelles, d'auto-détermination et de cohésion sociale,
nous faisons le choix pour cet événement de nous contenter de demander aux personnes malades
de rester se reposer chez elles.

Pour les autres nous vous recommandons d'adapter vos prises de risques en conscience de ce que vous êtes prêt à assumer
qu'il s'agisse d'attraper ou de transmettre à autrui le covid ou quoi que ce soit d'autres d'ailleurs.

Pour avoir conscience des risques covid et identifier les mesures les plus efficaces
pour les réduire, nous vous recommandons le simulateur [microCovid](https://www.microcovid.org/),
logiciel libre réalisé par des passionné⋅e⋅s en s'appuyant sur des chiffres issus de publication scientifique.

En résumé :

| 💉 Dose de vaccin |  0  |  1  |   2   |   3   |
|-------------------| --- | --- | ----- | ----- |
| Risque            | x 1 | x 1 | x 0.8 | x0.25 |

| 😷 Type de masque      | tissu  | chirurgical | FFP2   |
|------------------------|--------|-------------|--------|
| que tu portes          | x 0,66 | x 0,5       | x 0.33 |
| que les autres portent | x 0,33 | x 0,25      | x 0.16 |

- Donc si tu portes un masque FFP2 en étant vacciné⋅e avec 3 doses, tu réduis tes risques par 12 soit 8,3% du risque initial.
- Si les autres font de même, le risque pour chacun⋅e est réduit par 72 soit 1,4% du risque initial.

Information inconnues :
- taux de réduction du risque (RdR) de faire un dépistage systématique en arrivant sur place.

Pour la soirée, nous seront en intérieur, fenêtres ouvertes si la météo le permet.
D'experience, rares sont les personnes masquées.

## Inscription :

La procédure est détaillée sur la page : [inscription par sms](inscription-sms.md)
Dans la majorité des cas, tu peux te contenter du premier point :
les infos qui nous permettent de nous organiser envoyé par sms au numéro indiqué.

Nous espérons vous voir nombreu⋅x⋅ses et que ce format d'événement vous plaira 🙂
