title: Idées'couvrir
-------------------
![bannière d'illustration](img/bannieres/idees-couvrir.png)

# Idées'couvrir


### 🥰 Qu’est-ce qu’une « Idées'couvrir » ? 🥰

Une « Idées'couvrir » est un évènement pour faire un premier pas dans le milieu sex-positif (S+), en découvrant le cadre, les valeurs, les codes de communication et en expérimentant le consentement à travers des jeux de connexion à soi et aux autres.

Cet événement est aussi bien destiné aux personnes débutantes ne connaissant pas ou peu la culture S+, qu’aux personnes souhaitant approfondir leurs connaissances et la pratique des outils de consentement.

Il n'y aura pas de sexualité. La sensualité sera autorisée (câlins, caresses non génitales, massages) mais il est possible de vivre l'événement sans contact physique avec d'autres.

Rien ne sera obligatoire dans les ateliers proposés.


### 🫂 Pourquoi un atelier d'éducation au consentement ? 🫂

Le consentement, au-delà d'être une valeur socle des cultures S+ féministes, est un outil permettant de prendre soin de soi et des autres.

Les compétences du consentement sont très peu approfondies dans la société. Les ateliers que nous proposons ont pour objectifs de :
- Connaître et reconnaître les biais de consentement qui peuvent flouter les lignes du oui et du non ;
- Choisir de vivre des interactions pleinement choisies et enthousiastes.


### 📜 Le cadre de l'évènement📜
Si vous souhaitez avoir des informations sur le cadre pour cet évènement, suivez ce lien :
https://intim-idees.fr/2022-04-09-idees-couvrir-cadre.html


### 🦄 Qui peut participer ?☝️

Cet évènement s'adresse en priorité aux personnes intéressées par le milieu sex-positif, débutant.e.s ou confirmé.e.s et peut servir de porte d'entrée avant de participer à des événements où la nudité et la sexualité sont permises avec un cadre de consentement explicite.

Notamment, pour les débutant.e.s, cet évènement permet de réellement prendre le temps de s'initier à ces nouveaux codes, et à ce qu'ils impliquent comme changement de paradigme et potentiels chamboulements internes.

Il s'adresse aussi aux personnes qui souhaitent simplement progresser sur leur capacité à s'écouter, poser leurs limites, communiquer autour du consentement et être à l'écoute des autres. Par exemple au sein de leurs relations amicales ou amoureuses, dans leur exercice professionnel, dans le cadre familial…


### Où ?

Cet évènement aura lieu dans une maison à Bègles, à 300 mètres de l’arrêt La Belle Rose du tram C.


### 🕟 Le déroulement 🕟

Entre 12h00 et 13h00 - Accueil
Temps pour te changer si besoin (il y a un vestiaire sur place), grignoter et bavarder hors cadre. Tu es également invité.e à amener ce que tu souhaites manger ou boire (sans alcool). De quoi grignoter sera disponible tout au long de l’après-midi mais pas de quoi faire un repas.

13h00 – Fermeture des portes
Nous choisissons de ne pas accepter les personnes après 13h00 car nous avons à cœur que tout le monde assiste au rappel du cadre, donc pour les retardataires chroniques retenez 12h00, pas 13h00.

13h05 – Cercle d’ouverture
Un cercle d’ouverture permettra à tout le monde de se présenter et d'entrer doucement dans l’évènement. Ce sera aussi l'occasion pour nous de répondre aux questions. Nous présenterons à ce moment-là le cadre et les propositions de l’évènement.

14h15 - Ateliers
Nous vous proposerons des jeux pour mettre en pratique les valeurs de consentement, puis des jeux de connexion et déconstruction progressive pour aller à la rencontre de soi-même et des autres.
Par exemple (tout ne sera pas forcément exploré) :
- fournir une écoute de qualité
- communiquer émotionnellement
- identifier ses désirs
- prendre conscience des biais qui nous influence
- oser dire non
- recevoir des non avec gratitude
- affirmer son NON !
- communiquer sans mots
- oser exprimer ses désirs

18h30 - Cercle de clôture
Ce temps permettra de partager les différentes expériences et prendre le temps d'explorer ce qui est vivant en nous et ce que l'on désire apprendre de ça.
Tu pourras bien sûr quitter l’évènement dès que tu le souhaites.


### 🥰 Prolongation avec une « Idées'culottées » ? 🥰

Une fois l'après-midi guidée terminée, il y aura un temps pour se dire au revoir et partir... Mais pour celleux qui souhaitent prolonger l'expérience, un temps d'exploration libre, sera ouvert pour la soirée.

L'intention de ce temps est orientée partages, connections... par les mots ou par les corps (avec un changement de cadre autorisant la sexualité). Cela peut aussi être un temps à s'offrir à soi-même pour intégrer paisiblement la journée en grignotant ou en se reposant.

Cet événement est destiné à des personnes ayant fait un événement « Idées'couvrir » (le jour même ou une ancienne édition).

Voir l’évènement :
https://intim-idees.fr/2022-09-10-idees-culottees.html


### 👥 Qui animera l'évènement ? 👥

Millicent, Sam, Céliane seront les animataires principales de l’événement.
Les principaux actaires du collectif intim’idées sont actuellement : Céliane, Clara Loïc, Millicent, Myrha et Sam.


### 💸 Participation financière 💸

La participation aux frais (PaF) permet de payer la nourriture et de couvrir le prix du matériel (matelas, son, décoration, etc.). Vos contributions au-delà couvrent l’hébergement et son entretien, ainsi que tout le travail d’organisation de l’évènement, voire peuvent faciliter de futurs évènements et, dans la mesure du possible, de rémunérer les organisataires de l'événement.

Le prix est libre, et nous proposons plusieurs fourchettes de prix pour vous guider :
- PaF solidaire : 1 à 20 €
- PaF équilibre : 20 à 40 €
- Contribution soutien : 40 à 80 €
- Contribution justice sociale & soutien des futurs évènements : 80 à 160 € (ou plus)

Le prix que vous pouvez mettre ne fait pas partie des critères de sélections, tant que ça ne met pas collectivement en péril la viabilité de l’évènement.

La transparence sur le budget de l'événement sera disponible sur le site Intim'idées dans les jours qui suivent l'événement.


### 😷 Information COVID 😷

Nous tenons à rappeler que le virus est toujours parmi nous et que venir à cet événement comporte des risques, d’autant plus que le cadre ne permettra pas le respect des gestes barrières.
Le port du masque ne sera pas obligatoire.

Avant la tenue de l’évènement :
- Il est demandé de ne pas venir en cas de symptôme ou de cas contact.
- Nous vous demanderons de nous fournir le résultat d’un test antigénique négatif à votre nom, datant de moins de 24h avant le début de l’évènement ou de venir avec un autotest à faire sur place.

Après :
- Merci de prévenir l’orga si des symptômes apparaissaient dans les 7 jours après l’événement afin que chaque participant⋅e puisse se faire tester et s’isoler si nécessaire.


### ✉️ Comment je m’inscris ? ✉️

En répondant à ce formulaire de pré-inscription :
https://framaforms.org/pre-inscription-ideescouvrir-du-10-septembre-2022-1660738168

Si vous rencontrez des difficultés pour remplir ce questionnaire, n’hésitez pas à nous contacter :
contact@intim-idees.fr ou par sms : 0 770 772 770

PS : nous aurons beaucoup à faire durant les 48 heures précédant l’événement.
Il n’est pas sûr que nous ayons le temps d’étudier ta pré-inscription si tu nous l’envoi seulement en dernière minute.
On fera au mieux.

Il est possible que tu ne sois pas retenu⋅e pour participer cette fois-ci car :
- Le groupe sera constitué de 30 personnes maximum, pour fournir la qualité de cadre et d’animation à laquelle nous tenons.
- Nous choisirons les personnes afin de favoriser un équilibre et une cohérence entre les personnes présentes lors de l’événement, avec une vigilance à l’inclusivité des personnes appartenant à des groupes discriminés.
  L'âge n’est pas un critère de sélection mais il est à noter que l’âge moyen des participant.e.s se situe entre 20 et 45 ans (si cette information est importante pour toi.)


### ❓ Des questions ❓

Tu peux bien évidemment nous contacter ! Notre préférence : par email (contact@intim-idees.fr), sur notre page Facebook « Intim’idées » ou sur nos autres réseaux sociaux.

Merci d’avoir lu toutes ces informations nécessaires et à très bientôt !

La team’idées ❤


PS : Une partie de ce texte est inspirée de ce que proposent "La Bulle", "Love Experience", "La Licornerie " et "Les Félinades", n'hésitez pas à regarder ce qu'iels proposent !


