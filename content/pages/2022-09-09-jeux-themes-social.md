title: Jeux'thèmes du 9 septembre 2022
-------------------
![bannière d'illustration](img/bannieres/jeux-themes.jpg)
# Jeux'thèmes du 9 septembre 2022

Pour cette édition les thèmes sont l’authenticité et les émotions.
Nous animerons “Jeu me connecte” de l’apprentie Girafe
et probablement d'autres surprises !

Tu ne connais pas encore les soirées *jeux'thèmes* ?

Rendez-vous sur notre site pour la présentation détaillé et les modalité d'inscritpiion :
https://intim-idees.fr/2022-09-09-jeux-themes.html

🔥 Combo week-end 🔥

Cette édition *jeux'thèmes* commence un week-end entier avec plusieurs événements intim’idées qui peuvent se faire aussi bien indépendamment, qu’à la suite.

Il est possible de dormir sur place le vendredi et samedi soir en dortoir.
Nous t'accueillons donc volontiers même si tu viens de loin !

Info dodo : https://intim-idees.fr/laborizon-dortoir-infos-pratiques.html

Au programme du week-end :
- Vendredi soir : jeux'thèmes https://intim-idees.fr/2022-09-09-jeux-themes.html
- Samedi après-midi : idées'couvrir https://intim-idees.fr/2022-09-10-idees-couvrir.html
- Samedi soir : idées'culottées https://intim-idees.fr/2022-09-10-idees-culottees.html
- Dimanche après-midi : méta'relation https://intim-idees.fr/2022-09-11-meta-relation.html

À très bientôt,

La team’idées ❤

