title: Bilan financier Jeux'thèmes du 29 Juin 2022
-------------------
# Bilan financier Jeux'thèmes du 29 Juin 2022

Actualisé le : 02/07/2022

## Participations :
- Participations prévisionnelles : 150 € ( 20 x 7,5 € en considérant qu'entre les soutiens et les solidaires, on arrive pile à l'équilibre )
- Participations reçues : 55 € (5E+5P+10E+10P+25P)

## Répartition prévisionnelle :
- [ ] 50€ de participation aux frais du lieu d’accueil (loyer & charges ≈ 1600€/mois, stockage du matériel).
- [ ] 50€ pour acheter un nouveau jeu à vous faire découvrir (ou fabriquer le materiel nécessaire). Par exemple :
     - [Moi c'est Madame](https://www.jeux-cooperatifs.com/shop/moi-cest-madame/) pour lutter contre le sexisme ordinaire.
     - [Bloc by Bloc](https://outofordergames.com/blocbybloc/) un jeu pour s'essayer à contrer efficacement les dérives autoritaires de nos sociétés. Pour en savoir plus : [un article francophone qui en parle](https://evasions.blackblogs.org/2018/08/07/linsurrection-nest-pas-un-jeu/).
     - [Regards sur le porno](https://www.jeux-cooperatifs.com/shop/regards-sur-le-porno/) pour en parler sans se juger et pouvoir oser en faire un vrai sujet.
     - [Consentement, t’en dis quoi ?](https://www.jeux-cooperatifs.com/shop/consentement-ten-dis-quoi/) pour questionner notre rapport au consentement sur le plan interpersonnel, mais aussi socioculturel.
     - [Mon nom est Clitoris](https://www.jeux-cooperatifs.com/shop/mon-nom-est-clitoris/) pour parler sexualité et plaisir.
     - [Européennes en Quête de Droits](https://www.jeux-cooperatifs.com/shop/europeennes-en-quete-de-droits/) pour explorer l'état des inégalités sociale et patriarcales.
- [ ] 50€ pour aider les orgas à manger jusqu'à la prochaine édition.
