title: Jeux'thèmes du 9 septembre 2022
-------------------
![bannière d'illustration](img/bannieres/jeux-themes.jpg)
# Jeux'thèmes du 9 septembre 2022

Pour cette édition les thèmes sont l’authenticité et les émotions.
Nous animerons :
- “Jeu me connecte” de l’apprentie Girafe ([🛈](https://apprentie-girafe.com/wp-content/uploads/2021/06/Regle-jeu-me-connecte-apprentie-girafe.pdf "plus d'informations") & [🛒](https://apprentie-girafe.com/produit/coffret-connecte/ "boutique"))
- "On veut du vrai"
- "We are not really strangers"

et peut-être [d'autres jeux](liste-jeux.md) !

Tu ne connais pas encore les soirées *jeux'thèmes* ?

Voici la [présentation du format d'événement **jeux'thèmes**](presentation-jeux-themes.md)
avec toutes les informations pratiques à savoir.

## Prêt⋅e ? C'est par ici pour [**t'inscrire**](inscription-sms.md) !

Cette édition *jeux'thèmes* commence un week-end entier avec plusieurs événements intim’idées qui peuvent se faire aussi bien indépendamment, qu’à la suite.

Il sera possible de [dormir sur place](laborizon-dortoir-infos-pratiques.md) le vendredi et samedi soir en dortoir.
Nous t'accueillons donc volontiers même si tu viens de loin !

Au programme du week-end :
- Vendredi soir : [jeux'thèmes](2022-09-09-jeux-themes.md)
- Samedi après-midi : [idées'couvrir](2022-09-10-idees-couvrir.md)
- Samedi soir : [idées'culottées](2022-09-10-idees-culottees.md)
- Dimanche après-midi : [méta'relation](2022-09-11-meta-relation.md)

À très bientôt,

La team’idées ❤

