title: Stats idées'couvrir du 9 Avril 2022
-------------------
# Stats idées'couvrir du 9 Avril 2022

Actualisé le : 08/04/2022

## Pré-inscriptions reçues :

| Description          | Total | Femmes | Hommes | Autres |
|----------------------|------:|-------:|-------:|-------:|
| Total reçues         |    22 |      7 |     11 |      4 |
| Orga.                |     5 |      2 |      1 |      2 |
| Présent⋅es           |     ? |      ? |      ? |      ? |
| Retenues             |    18 |      7 |      8 |      3 |
| Écartées             |     3 |      0 |      3 |      0 |
| Désistées            |     1 |      0 |      0 |      1 |
| Pas encore examinées |     0 |      0 |      0 |      0 |

<!--
## Effectivement présent⋅e⋅s sur place

| Description           |                Global |              Femmes |               Hommes |
|-----------------------|----------------------:|--------------------:|---------------------:|
| participant⋅e⋅s       |                    28 |                  15 |                   13 |
| Age (min/moy/med/max) | 24 / 34,4 / 33,5 / 45 | 24 / 33,1 / 32 / 44 |  28 / 35,9 / 36 / 45 |
| PaF annoncée¹ (idem)  |    1 / 24,5 / 20 / 50 | 1 / 21,28 / 20 / 50 | 10 / 28,08 / 30 / 40 |
| PaF reçue¹ (idem)     |    0 / 25 / 22,5 / 50 |    0 / 23 / 20 / 50 | 10 / 27,30 / 30 / 40 |

¹ PaF (Participation aux frais). Au-delà des statistiques, le [bilan financier](2022-03-05-laborizontale-compta.md) est consultable.

| Attirances                                   | Global | Femmes | Hommes |
|:---------------------------------------------|-------:|-------:|-------:|
| Hétéro                                       |      9 |      3 |      6 |
| Hétéro-flexible / curieu⋅ses⋅x               |     11 |      5 |      6 |
| Pan / bi                                     |      8 |      7 |      1 |
| Homosexuelles / gays / lesbiennes            |      0 |      0 |      0 |
| *Parité idéale pondérée par les attirances²* |     28 |   15,8 |   12,2 |
| Pour rappel : pré-inscriptions retenues      |     28 |     15 |     13 |

² Estimatif de l'idéal de parité avec l'algorithme suivant :
- pour chaque hétéro, 1 personne du genre opposé
- pour chaque hétéro-flexible, 0,2 personnes du même genre et 0,8 personnes du genre opposé
- pour chaque personne pan, 0,5 personnes du même genre et 0,5 personnes d'un autre genre
- pour chaque personne homo, 1 personne du même genre

-->

## Pré-inscriptions retenues :
| Description                      |            Global |            Femmes |            Hommes | Autres |
|----------------------------------|------------------:|------------------:|------------------:|-------:|
| Pré-inscriptions retenues        |                18 |                 7 |                 8 |      3 |
| Age (min/moy/med/max)            | 24 / 30 / 30 / 40 | 24 / 28 / 27 / 33 | 24 / 33 / 34 / 40 |    NC⁴ |
| Participation financière¹ (idem) | 0 / 28 / 20 / 100 | 0 / 29 / 15 / 100 |  0 / 30 / 28 / 70 |    NC⁴ |

⁴ Pas assez nombreu⋅ses⋅x pour fournir des stats sans révéler d'information personnelle.

## Pré-inscriptions écartées
| Description               | Global |              Femmes | Hommes | Autres |
|---------------------------|-------:|--------------------:|-------:|-------:|
| Pré-inscriptions écartées |      3 |                   0 |      3 |      0 |

| Autres Métriques         | Minimum | Moyenne | Médiane | Maximum |
|:-------------------------|--------:|--------:|--------:|--------:|
| Age                      |      35 |      37 |      36 |      41 |
| Participation financière |       6 |      19 |      20 |      30 |
