title: Journée aire'éthique
-------------------
# Journée aire'éthique

À l’occasion de Beltaine, fête de la fertilité,
s’entremêlerons ateliers d’autodéfense intellectuelle
et rituels d’inspiration païenne.
Nous espérons fertile, l’émulsion de ces deux mondes


## Nos envies pour cette journée :
- Donner à vivre la sensation d'être authentiquement écouté⋅e et pleinement entendu⋅e
- Partager ce qui nous relie toustes quel que soit votre rapport au monde
- Dépasser les aprioris et incompréhensions pour apprivoiser les complémentarités de regards
- Conscientiser les chemins de nos vérités et leurs limites
- Célébrer la vie, la beauté, la présence à soi et aux autres

## Déroulé :
- 9h30 : ouverture et accueil des participant⋅e⋅s
- 10h : fermeture des portes et début du cercle d'ouverture
- ≈ 10h30 : exercer sa qualité d'écoute
- ≈ 11h00 : Pourquoi s'intéresser aux autres manières de penser et voir le monde ?
- ≈ 11h30 : Comment débatre d'un sujet complexe en 3 phases
- ≈ 13h : déjeuner en conscience
- ≈ 14h30 : cercles de paroles ( Quelles valeurs me guident et guident ma pratique ? De quelle manière ces valeurs se manifestent dans ma pratique ? Qu'est-ce que cela m'apporte ? )
- ≈ 15h15 : Rituel :
  - cérémonie d’ouverture “accueil des éléments”
  - méditation guidée
  - ateliers créatifs (fabrication de couronnes de lierre, dessin, peinture, collages…)
  - clôture cérémonie
- ≈ 16h30: danses trad guidées
- ≈ 17h:  pause gouter & stand énigmes zététiques, (dé)jouons les illusions, (senses, experiences, analyse)
- ≈ 17h30 : philosophie de la vérité
- ≈ 17h45 : conte viking
- ≈ 18h30 : cercle de cloture
- 19h : fin de la journée, départ pour les un⋅e⋅s, repas pour celleux qui restent à la soirée
- 20h : début de la [soirée aire'éthique (18+)](2022-04-30-soiree-aire-ethique.md)

Fil rouge soutien : accueillir et dissiper les crispations résiduelles et autres images ennemies. (Tout au long de la journée, au stand soutien).

## [Présentation détaillée & inscription](2022-04-30-journee-aire-ethique-presentation.md)
## [Bilan financier]() (bientôt)
## [Statistiques de l'événement]() (bientôt)

## ✉️ Comment je m’inscris ? ✉️

Le lien est en bas de page de la [présentation détaillée](2022-04-30-journee-aire-ethique-presentation.md).

PS : nous aurons beaucoup à faire durant les 48 heures précédant l’événement.
Il n’est pas sûr que nous ayons le temps d’étudier ta pré-inscription si tu nous l’envoi seulement en dernière minute.
On fera au mieux.
