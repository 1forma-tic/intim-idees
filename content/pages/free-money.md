title: 💸 Free Money 💸
-------------------

# 💸 Free Money 💸

Ce que nous appelons "Free Money" est une caisse de solidarité entre participant⋅e⋅s.

Elle est marterialisée par un récipient sur le quel est indiqué "Free Money".

Quiconque peut mettre ou prendre dedant sans avoir à se justifier.

Si vous percevez des disfonctionnements dans l'usage effectif de ce système de solidarité,
c'est peut-être le signe qu'il est utile d'en discuter pour voir s'il est possible de faire mieux.

Vous pouvez proposez un cadre pour y travailler, ou nous contacter pour que nous en créions un.


Note : Ce système est inspiré de la [coresponsabilité financière](https://cerclesrestauratifs.org/wiki/Utilisateur:1000i100/coresponsabilite-financiere)
pratiquée par Dominic Barter et d'autres dans son sillage.
