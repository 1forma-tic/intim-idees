title: Soirée idées'connectées du 8 mai 2022
-------------------
# Soirée idées'connectées du 8 mai 2022



Une *idées'connectées* est un évènement conçu pour explorer des espaces de consentement
et de connexion consciente aux autres.

L'intention de ce temps est orientée partages, connections... par les mots ou par les corps.
La nudité sera possible seulement si l’ensemble des participant.e.s est conscentant.e.s,
la sensualité sera autorisée (câlins, caresses non génitales, massages, …)
mais il n'y aura pas de sexualité.

Cet événement est destiné avant tout à prolonger l'experience [idées'couvrir de l'après-midi](2022-05-08-idees-couvrir.md).
Pour autant, nous expérimentons de l'ouvrir, aux personnes ayant participées à une précédente après-midi idées'couvrir,
voir aux personnes ayant fait un autre de nos évènements
ayant des ateliers de consentement (Soirée Aire’éthique ou Laborizontale).

L'évènement est structuré comme ceci :
- 19h00 : Auberge espagnole (sans alcool) où chacun.e apporte quelque chose.
- 20h00 : Cercle d'ouverture de la soirée, de rappel des règles et du cadre, et de propositions et de demandes d’ateliers encadrés par les orgas et ou des participant.e.s.
- 21h15 : Exploration libre via notamment des ateliers proposés (mais aucun ne sera obligatoire).
- 00h00 : Cercle de clôture de la soirée.


Pour en savoir plus et connaitre les modalités d’inscriptions, voici :

## [La présentation détaillée & inscription](2022-05-08-idees-connectees-presentation.md)
## [Le bilan financier]() (bientôt)
## [Les statistiques de l'événement]() (bientôt)

Nous espérons vous voir nombreu⋅x⋅ses à cet évènement.

La team’idées ❤
