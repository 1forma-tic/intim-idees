title: L'inclusivité chez intim'idées
-------------------
# L'inclusivité chez intim'idées

## Être inclusif⋅ve, ça veux dire quoi ?

Chez [On SEXplique ça](https://onsexpliqueca.com/linclusivite-cest-quoi/) comme à nombre d'endroits,
l'inclusivité, sur le papier, est définie comme suis :
"Cela signifie inclure tout le monde. Aussi simple que ça."

Dans un monde à la [diversité](https://youtu.be/Cp5d45Fr3_c) inégale,
où certain⋅e⋅s on le privilège d'être déjà inclu⋅e⋅s par le système dans la société,
et où d'autres subissent différentes oppressions les en excluant,
l'inclusivité deviens, avec un grille de lecture intersectionnelle :
"l’acte de promouvoir, de favoriser et de défendre l’intégration de minorités."

Nous avons conscience de cela et souhaitons également prioriser l'inclusion des oppressés,
en favorisant les stratégies proposées par et pour inclure les concerné⋅e⋅s.
Cela peut inclure par exemple la création d'espaces et de temps à mixité choisie.

# Oppression societal vs oppression communautaire, toutes deux systémique, à des echelles différente.

Pour autant, nous ne souhaitons pas créer un contre système au sein duquel les oppressions identifiées
ont été renversée pour compenser ce qui se vis dans la société en créant ainsi au sein de la communauté,
des privilèges inverses et des exclusions inverse.

![Illustration : inclusivité circulaire à croissance constante et terre brullée centrale]()

## Dépasser les impasses habituelles en admettant ses limites

Comment pouvons-nous à la fois refuser d'exclure les plus privilégié⋅e⋅s et proposer des espaces à mixité choisie ?

En admettant que :
- inclure tout le monde dès aujourd'hui est hors de notre portée
- créer les conditions d'inclusion des un⋅e⋅s peut générer l'exclusion des autres (hyper et hypo sensible auditivement par exemple)
- favoriser le confort des un⋅e⋅s génère souvent de l'inconfort chez d'autres (au moins le temps de s'adapter)

En partant de là, la stratégie qui nous semble maximiser l'inclusivité de notre communauté est celle de faire varier
d'un évènement sur l'autre nos priorités d'inclusion.

![Illustration : inclusivité en pétale de fleur à spirale exponentielle, décentrée]()

## Précipiter l'inclusion, à quel prix ?

En observant le chemin de différentes communautés aux idéaux fort et se voulant inclusive des plus oppressé⋅e⋅s,
nous observons que le rythme de prise de conscience et d'évolution des plus impliqué⋅e⋅s
mène à pouvoir inclure nombre d'habituellement exclu⋅e⋅s,
mais à un rythme d'adaptation insoutenable pour les moins impliqué⋅e⋅s.
De ce fait, progressivement, la communauté perd la connexion avec la culture dominante
et la capacité d'inclure celleux qui en sont le fruit, sans forcément s'en rendre compte ou s'en soucier.
Ce n'est pas nécessairement un problème et peut-être un choix radical assumé.
Cela peux aussi s'opérer à l'insu de la communauté et lui porter préjudice.

## Nos objectifs d'inclusivité à long terme

Dans la communauté intim'idées, notre raison d'être et nos valeurs socles
nous invite à une triple vigilance :
- dépasser la double peine qui serait de construire un environnement privilégié
  et de le réserver à celleux qui sont déjà privilégié⋅e⋅s dans notre société.
- celle d'augmenter notre capacité à inclure des profils toujours plus diversifiés,
  pour qu'iels puissent découvrir notre culture avec les possibles qu'elle ouvre, s'y nourrir et la métisser
  puis l'essaimer autour d'elleux.
- celle de pouvoir inclure le plus grand nombre pour que la liberté d'être et de faire qui nous est chère
  ne reste pas le privilège d'une niche d'initié⋅e⋅s nous côtoyant, mais puisse infuser plus largement
  et faire évoluer la société dans laquelle nous vivons.

## Une stratégie contre-intuitive, mais gagnante sur la durée

###  Démarche pédagogique

Pour consolider la culture commune de notre communauté naissante,
nous avons conscience de faire le choix élitiste de nous entourer de personnes
qui partage déjà un maximum de nos valeurs, codes et postures pour créer une cohésion forte.
Cette dernière devrait nous permettre de faciliter l'acculturation des nouvelleaux
en leur donnant à vivre une experience cohérente avec le cadre explicitement présenté.
Ainsi, l'assimilation de nos manières d'interagir, codes, postures pourra se faire
dans la fluidité du mimétisme spontané.
En multipliant les cannaux d'apprentissage et la cohérence entre eux,
nous devrions satisfaire les conditions d'apprentissage d'un large panel de profils psychologique :
- Celleux qui ont besoin de comprendre intellectuellement avant de mettre en pratique
- Celleux qui ont besoin de faire l'experience pour adhérer
- Celleux qui ont besoin de voir faire pour imiter
- Celleux qui sont motivé⋅e⋅s par la curiosité, la découverte, comprendre et éxpérimenter
- Celleux qui sont motivé⋅e⋅s par la sensation d'appartenance, d'acceptation, de reconnaissance

### Bénéfices durables
Bref, les bénéfices pédagogiques de cette stratégie initiale élitiste de cohésion
nous semble propice pour ancrer notre culture et renforcer sa résilience.
En ayant constitué ce robuste socle de culture commune, nous devrions sereinement et durablement pouvoir faire progresser
notre inclusivité (et baisser notre élitisme) sans fragiliser le collectif, et cela,
bien au dela de ce que nous pourrions inclure à court terme sans prendre le temps d'assurer notre cohésion en tant que communauté.

---------
Auteur : Millicent, le 16/02/2022
Texte librement réutilisable : CC-0
