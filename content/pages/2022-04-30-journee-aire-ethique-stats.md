`# intention journée
# intention soirée
# programme de la journée

    9h30 : ouverture et accueil des participant⋅e⋅s

    10h : fermeture des portes et début du cercle d’ouverture, ainsi qu'outil de communication pour sortir efficacement des eccueils (et outil de prise de température)

    Exercice de qualité d'écoute (Objectif : distinguer la posutre de curiosité scincère, de l'écoute défensive/réactive pour préparer sa réponse, ou de la déconnexion totale) (avec questions TQR)

    Pause 15min

    11h00 Pourquoi je m'intéresse à la zététique ? Pourquoi je m'intéresserai à l'ésothérisme ? (débroussaillage des besoins communs et des différences de stratégie en conclusion, via charpi)

    12h -> 13h : atelier zet : questionner la confiance dans les experiences (le cerveau nous trompe) et identifier et déjouer les biais et faille de discours plausible et séduisant.

    13h : Déjeuner en conscience (avec traduction si besoin)

    14h30 :  ouverture accueil des éléments - relaxation digestive - méditation guidée (objectif : permettre aux gens de choisir une intention pour la fête) / qu'est-ce que je veux apporter pour la paix (en moi, dans le monde...) ?

    15h : ateliers créatifs (fabrication de couronnes de lierre, dessin, peinture, collages...) + ateliers résolutions d'énigmes zet (illusions et méthode de contournement)

    16h: danses trad guidées (choix fraise ou poireaux)

    16h30 : conte viking (redescente)

    clôture cérémonie

    17h ou plus tard, déconstruction des images énemies à base de cercle restauratif ou jdr CNV, ou marelle... / accueil des crispation résiduelle pour pouvoir repartir plus cerain⋅e

    18h : cercle de clôture

    19h : fin de la journée, libre à vous de partir - REPAS du soir pour celleux qui restent


Intention ésothérique : symboliser les parties de soi qui représentent la fertilité masculine (intentions, valeurs, inspirations, mental, directif, action, épée) et féminine (instincts, corporel, concrétisations dans la matière, fluide, mouvant, écoute, graal/coupe) et utiliser l'expérience, le jeu, la danse pour les faire s'entre-mêler.
(Invitation à accueillir les parties de soi même dont on a honte, qu'on n'aime pas, etc.)



# programme de la soirée

    19h30 : accueil des éventuel⋅le⋅s participant⋅e⋅s qui n’aurais pas participé à la journée aire’éthique + REPAS !

    20h : fermeture des portes et début du cercle d’ouverture

    20h30 : atelier consentement (Affirmation du non, gratitude pour le non, accueil de la frustration, consentement verbal, consentement non verbal)

    22h : cérémonie de Beltane : célébration sur le thème de la fertilité pouvant mener à de la sexualité

    ouverture de cérémonie (10 min)

    conte (15 à 20 min)

    méditation guidée : alignement entre terre et ciel (15-20min)

    intermèdes chants à différents moments

    apports sur le sens de Beltane et ses symboliques

    tirage d'oracle

    Adoration (min 20min-1h30) (avec intro orienté pleine conscience)

    clôture de cérémonie (10 min)

    00h : moisson du soir pour celleux qui souhaite partir ensuite

    01h : buffet nocturne

    01h : priorité sommeil dans un des espaces pouvant faire dortoir

    ateliers en auto-organisation ? propositions : Méditation orgasmique, lecture tarot

    11h le lendemain : moisson de la nuit et cercle de cloture

    12h : rangement nettoyage et départ



Intention ésothérique : symboliser les parties de soi qui représentent la fertilité masculine et féminine et utiliser l'expérience, le jeu, la danse, la sexualité, le dialogue, pour les faire s'entre-mêler.
Utiliser cette énergie au service d'une intention qui nous est chère.

- conte merveilleux
- lecture de conte, une interprétation
- atelier inspiration TQR, qu'est-ce que je veux apporter pour la paix (en moi, dans le monde...) ?
- méditation guidée
- rituel psychomagique
- tirage de cartes personnalisé


# la com. de la journée
-> milieux trad
-> écologirls
-> Fb
-> Mobilizon
-> Demosphere

# la com. de la soirée



# les inscription pour la journée

- Quel est ton prénom ou pseudo ? *
  Indique le même que sur le forum, qu'on te retrouve facilement.

Pour rappel, lien d'inscription sur le forum : https://forum.intim-idees.fr/invites/XSPHj9f2xm

- Comment te contacter efficacement ? (par exemple ton numéro de mobile) *

- Un email pour les infos groupées de l’évènement ? *
  [x] Je souhaite être invité⋅e au forum de la communauté intim’idées
  [ ] Newsletter intim'idées
  [ ] Newsletter miss-permaculture

### Comment nous connais-tu ?
Y'a-t-il des personnes qui te recommandent ? (si oui, lesquelles)

Comment as-tu eu connaissance de l'événement ?
Annonce lors d'un événement intim'idées au quel tu as assisté⋅e ?
Publication sur le Forum ?
Site intim-idees.fr ?
Flux RSS intim'idées ?
Publication Mobilizon ?
Publication Demosphère ?
Publication sur la page Facebook intim'idées ?
Publication Facebook sur d'autres pages ou groupes ?
Par l'Invitation reçue pour l'événement Facebook ?
Publication Instagram ?
Publication twitter ?
Bouche à oreille de quelqu'un⋅e qui n'y va pas ?
Bouche à oreille de quelqu'un⋅e qui souhaite y aller ?
De la bouche d'un⋅e orga intim'idées ?
- Newsletter intim’idées
- Newsletter miss permaculture

Si tu as découvert l'événement autrement, ou que tu veux préciser qui en bouche à oreille, ou par quel groupe, page...


### Harmonisation du groupe

As-tu déjà vécu des experiences ou événements :
aucun ; oui, en découverte ; oui, j'ai l'habitude
intim'idées
Burn (nowhere, crème brulée...)
Tantra
Welöv
Dev. personnel
Rainbow
Auto-gérée / coresponsabilité
CNV
santé / bien-être
- féminin sacrée
- travail qui relie
- rituel chamanique
- stage de permaculture






D'autres experiences qu'il te semble utile de préciser ?
Quel âge as-tu ? *
Pourquoi veux-tu venir ? *
Qu'est ce qui te donne envie de venir ? Quelles sont tes intentions pour cet événement ? Qu’as tu envie d’y vivre ? Qu’en attends-tu ? (Apprendre, tisser de liens, découvrir...)


### Pour prendre soin de toi
Y’a-t-il des choses que nous devrions savoir sur toi pour ta sécurité ? (allergies non allimentaires, phobies, handicap et besoins associés, façon de gérer des crises…)

Qu’est-ce qui t’aiderait à te sentir bien et en sécurité dans cet événement ? Que peux-tu faire toi-même pour cela et sur quoi aurais-tu besoin de nous ?

### Nourriture

Pour l'auberge espagnole du déjeuné, nous vous demandons d'amener un plat préparé salé ou sucré, à partager. Pour être en cohérence avec nos valeurs écologiques, nous vous invitons à privilégier des ingrédients bios, locaux, de saisons et sans emballages jetables (même si nous comprenons les limitations financières de certain.e.s, il est possible de faire des choses pour pas cher !)
Que comptes-tu apporter ?
Peux-tu lister les ingrédients pour les personnes allergiques ?
Est-ce :
- salé
- sucré
- végétarien
- végan

Pour l'auberge espagnole du soir, si tu comptes rester :
Que comptes-tu apporter ?
Est-ce :
- salé
- sucré
- végétarien
- végan
Peux-tu lister les ingrédients pour les personnes allergique ?

As tu un régime alimentaire particulier ?
Végétarien ?
Végan ?
Autre ? (Qu'il s'agisse d'allergie ou de toute autre chose)


### Conditions relationnelles
Ta venue est-elle conditionnée à la venue d’autres personnes ? Lesquelles ?
Ta venue est-elle conditionnée par la NON venue de certaines personnes ? Lesquelles ?


### Contributions
Sur cet événement, plusieurs personnes viennent en renfort de l'équipe habituelle d'animation. Tout en ayant des moyens limités, 3 personnes ont aussi des frais de déplacements (deux viennent de Toulouse, une de dordogne).
Ce qui fait que pour la journée il y aura au total au moins 6 personnes en animation ou soutien au bon déroulé, de 9h à 19h (soit 60h à votre service, soirée non comprise). Au regard des précédents événements, le temps de prépartion et de bilan hors événement est 2 fois superieur à celui sur place, soit 120h de plus pour permettre cette journée. Oui, on vous bichonne !
Nous souhaitons à minima défrayer les personnes organistrices et animatrices qui en ont besoin, et autant que possible de soutenir financièrement celles qui investissent de leur temps et compétences pour l'événement en n'ayant pas d'autre source de revenu stable.
En outre, nous souhaitons aussi contribuer aux frais du lieu d'accueil, ainsi qu'aux asso qui créent ou gèrent les outils numérique que nous utilisons. Bref, contribuer à faire fleurir ce qui contribue à faire fleurir cette journée !


À l’issue de la pré-inscription, pour les personnes acceptées, nous indiquerons différents moyens de paiement pour valider l’inscription. Indiquer un montant ici nous permet d’estimer combien nous pouvons nous permettre d’avancer comme frais pour préparer l’évènement.
Pour information, tu peux consulter le bilan financier du précédent événement.
Même si ça ne nous facilite pas l’organisation, tu peux mettre un “?” si tu préfères garder ta participation anonyme. Tu peux aussi indiquer une fourchette de prix si tu préfères décider en fin d’évènement combien mettre.
En gardant en mémoire qu’il y a plus de pauvres que de riches, voici quelques fourchettes de prix suggérées pour la journée :

    PaF solidaire : 1€ à 30€

    Équilibre 30€ à 60€

    Soutien 60€ à 120€

    Justice sociale & soutien des futurs évènements 120 € à 240€ ou plus.

Combien es-tu prêt⋅e à mettre, en prix libre, pour contribuer à l’organisation de l’après-midi et ses ateliers ?




Nous aurons sûrement besoin de renfort pour :

    lla veille de l'évènement : nettoyer, aménager et décorer les lieux (si vous avez de la déco ambiance nature / celtique / bien-être à prêter, elle sera bienvenue) ainsi qu'aller chercher du lierre

    en début d'évènement : accueillir et présenter les lieux

    le lendemain ou sur-lendemain : nettoyer et ranger les lieux

Pendant l'événement :

    Faciliter l'accès à du soutien émotionnel (soit en se proposant soi-même pour en fournir, soit en alertant les soutiens émotionnels quand quelqu'un⋅e en aurait besoin)

    Vérifier les niveaux des toilettes sèches et si besoin, alerter ou changer les bacs.

    Entretenir le bar/buffet (évacuer les récipients vides et remettre de quoi grignoter et se désaltérer).

    Assurer des premiers secours si besoin.

    Apporter du materiel créatif (peinture, crayons...) ou musical (instruments)

Et probablement d'autres choses que nous aurions oublié de préciser ici.


As-tu l'élan et la capacité de contribuer, et de quelle(s) façon(s) ? (à partir de nos besoins ou sous d'autres formes)



### Autres
As-tu quelque chose à rajouter ?

Enfin, qu’as-tu pensé de ce questionnaire ? Trop long ? Pas assez (il manque certaines questions importantes selon toi) ? Trop indiscret ? Trop précautionneux ? *

Si tu rencontres des difficultés pour remplir ce questionnaire, n’hésite pas à nous contacter !
PS : si rien ne se passe au moment de soumettre le formulaire rempli, déploie toutes les sous-parties et ré-appuie sur le bouton "soumettre", la page t'indiquera ce qui manque.



### soirée

Après avoir clôturé la journée, nous ouvrirons la soirée pour celleux qui souhaitent y participer.

Elle sera composée entre autre d’exercices de consentement puis de célébrations pouvant ouvrir à de la sexualité dans les espaces commun. Tu n’aura ni l’obligation ni la garantie d’avoir des activités sexuelles, mais si tu ne souhaite pas être exposé⋅e à de la sexualité, cette soirée n’est pas faite pour toi.


Souhaites-tu également participer à la soirée ? *
Non, pas dispo
Non, ça ne m'inspire pas (peur/pas envie/pas un cadre pour moi)
Peut-être... je suis pas sur.e
Oui carément !


Si tu as répondu « peut-être », quels sont tes freins, et qu’est ce qui te donnes envie de venir, que nous puissions te rassurer ou te recommander d’autres événements.


Pour te pré'inscrire à la soirée aire'éthique, voici le formulaire complémentaire :
URL


### Bonus :

Un des orgas se pose plein de question sociologique, si vous souhaitez contribuer à ses recherches, voici quelques questions suplémentaires (sans impacte sur votre acceptation ou non à l'événement) :

Comment te sens tu vis à vis :

?, -5, -3, -1, 1, 3, 5, 9

- de la méthodologie scientifique ?
- des partisan⋅ne⋅s zététique, septique, esprit critique ?
- des athées
- de la médecine occidentale (allopathique) ?
- des vaccins en général ?
- des vaccins contre le covid ?
- de l’obligation vaccinale contre le covid ?
- des mesures politico-sanitaire du gouvernement ?
- de la qualité de démocratie de la france ?
- pense-tu que la civilisation occidentale capitaliste existera dans 100 ans ?
- pense-tu que l’humanité existera encore dans 100 ans ?
- de la psychiatrie ?
- de la psychothérapie ?
- de la psychanalyse ?
- du coaching ?
- du développement personnel ?
- des médecines douces en générale ?
- de la naturopathie ?
- de la lithotérapie ?
- de l’homéopathie ?
- du magnétisme ?
- des concepts féministes
- des partisan⋅ne⋅s féministes
- du tantra ?
- de l’ésotérisme en général ?
- du vocabulaire religieux ? (âme, sacré...)
- du vocabulaire new-age ? (énergie, chakra… spiritualité orientale, occidentalisée)
- du vocabulaire païen ? (Wicca, tarrot, Beltaine…)
- des mystiques ? (personnes qui se qualifient comme tel)
- des sorcières ? (personnes qui se qualifient comme tel)
- des spiritualités scientiste (la science et les math expliqueront tout, d’ici là on postule : athéiste, économistes, psychanalyste...)
- des spiritualités ésotérique (le monde invisible se révèle aux initié⋅e⋅s : religions, new-age, païen...)




Note : nous aurons beaucoup à faire durant les 48h précédent l'événement. Il n'est pas sur que nous ayons le temps d'étudier votre pré-inscription si vous nous l'enovoyez seulement en dernière minute.



Préciser l’intention :

pas de jugement, aucune réponse n’est excluante, pouvoir se préparer pour mieux se respecter les un⋅e⋅s les autres dans nos convictions et choix. (réponses confidentielles à vocation de répondre à la curiosité d'un des orga pour mieux connaitre notre public ! xD)




# les inscription pour la soirée



# liste des personnes ressources journée
- Charpi, Leïla, Nathalie Dard, Amaury Educ'pop, Renault CNV (à confirmer)
- musique trad Valérie Hélène?
- JC R. Contes
# liste des personnes ressources soirée
- Amaury, les autres à confirmer


# Demande logisitique/ambiance :
- Matériaux pour faire couronnes de lierre
- Propreté et beauté du lieu : déco, bouquets de fleurs, clarté des indications...
- Bougies, pierres, encens, guirlande, etc
- poteau avec rubans dans jardin
- ruban pour soutien émo



`
