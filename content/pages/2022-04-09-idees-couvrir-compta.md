title: Bilan financier idées'couvrir du 9 Avril 2022
-------------------
# Bilan financier idées'couvrir du 9 Avril 2022

Actualisé le : 16/04/2022

- [Trésorerie antérieure](2022-03-05-laborizontale-compta.md) : 190 € (pour avance de frais et investissement)
- Participations reçues : 505 € ([voir les statistiques de participation](2022-04-09-idees-couvrir-stats.md))
- [x] 80€ de nourriture
- [x] 102€ pour 60 tests covid (ce qui nous fait du stock pour les prochains événements),
- [ ] 150€ de participation aux frais du lieu d’accueil (eau, électricité, loyer, stockage du matériel, nettoyage des draps…).
- [ ] 150€ de trésorerie, fond de roulement pour les prochains événements
- [ ] 150€ de provisions pour investir dans du matériel pour les futurs événements
- [ ] 10€ de soutien à Framasoft (nous utilisons FramaForms, Framagit et Mobilizon)
- [ ] 110€ de formation des orgas à la Licornerie pour pouvoir organiser idées'couvrir
- [ ] 245€ pour 3 allers retours à Paris pour aller se former à la Licornerie.
- [ ] 100€ pour défrayer les orgas de leur repas à raison de 5€ pour 5h de travail bénévole.
      En effet, hors animation sur place, 25 demi-journées (une centaine d'heures) ont été consacrées à l'événement (préparation en amont, bilan en aval)

695 € disponible pour financer
- idéalement 1097 € de postes de dépenses
- ou semble-t-il plus réaliste : 742 € (sans couvrir la formation que nous sommes aller faire sur Paris).

