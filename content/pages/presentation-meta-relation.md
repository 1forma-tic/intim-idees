title: Méta'relation
-------------------
![bannière d'illustration](img/bannieres/meta-relation.png)

# Méta'relation

## Objectif :
L'idée est de créer un espace de réflexion collective et d’entraide sur des outils, concepts ou notions.

Les « méta’relation (à plat) » sont des évènements destinés à mettre plus au clair ce qui se joue dans une relation, qu’elle soit à peine envisagée ou déjà établie.

Le thème est donc la communication méta-relationnelle (la façon dont on définit et fait évoluer une relation avec d’autres, quelle que soit cette relation), et l’outil proposé pour commencer à explorer ce sujet de façon à la fois théorique et pratique est le RBDSM(PB).


Le RBDSM(PB) est un outil de communication méta-relationnelle qui permet d’explorer de façon précise plusieurs dimensions de l’interaction à l’autre. Il permet de mettre des mots sur ce qui est habituellement implicite et de faciliter un consentement clair et des interactions fertiles.

La signification :
- Relations
- Boundaries (Limites)
- Désirs
- Status (Santé)
- Meaning (Symbolique)
- Public / Privé
- Biais


## Présentation de la journée :
Cet événement est destiné à toustes, mais les personnes venant accompagnées seront sélectionnées en priorités, afin de travailler directement avec une personne avec qui vous avez déjà une relation, qu’importe le type de relation. Les personnes ayant participé aux événements du weekend sont également bienvenues, vous pourrez bénéficier de cet espace pour explorer de la clarification avec des personnes avec qui vous avez connecté dans le weekend.


### ⏱ Horaires et déroulé ⏱
- 14h00 : Auberge espagnole (sans alcool) où chacun.e apporte quelque chose.
- 15h05 : Cercle d'ouverture, rappel des règles et du cadre.
- 15h20 : Présentation du RBDSM(PB), puis pratiques.
- 17h00 : Pratique du RBDSM(PB) en binômes et accompagné⋅e.
- 18h30 : Cercle de clôture.

### 💶 Et combien ça coûte ? 💶

Pour qu'organiser, animer et héberger cet événement et les suivants reste joyeux et enthousiaste, nous avons besoin de soutiens et d'encouragements.
Nous serions heureu⋅ses⋅x d'entendre ce que l'événement vous a apporté,mais aussi d'avoir votre aide pour couvrir nos frais.
À cet effet nous proposons une participation à **prix libre**.

Pour information, tu peux consulter <a href="https://intim-idees.fr/liste-compta.html" target="_blank">les bilans financiers des précédents événements</a>.

- PaF solidaire : 0€ à 7€
- Équilibre : 8€ à 15€
- Soutien : 16€ à 32€
- Justice sociale & soutien des futurs évènements : 33€ et au delà.



### 😷 Informations COVID 😷

Pour prendre soin des enjeux de santé virale comme mentale et psychologique
ainsi que des enjeux politique de préservation des libertés individuelles, d'auto-détermination et de cohésion sociale,
nous faisons le choix pour cet événement de nous contenter de demander aux personnes malades
de rester se reposer chez elles.

Pour les autres nous vous recommandons d'adapter vos prises de risques en conscience de ce que vous êtes prêt à assumer
qu'il s'agisse d'attraper ou de transmettre à autrui le covid ou quoi que ce soit d'autres d'ailleurs.

Pour avoir conscience des risques covid et identifier les mesures les plus efficaces
pour les réduire, nous vous recommandons le simulateur [microCovid](https://www.microcovid.org/),
logiciel libre réalisé par des passionné⋅e⋅s en s'appuyant sur des chiffres issus de publication scientifique.

En résumé :

| 💉 Dose de vaccin |  0  |  1  |   2   |   3   |
|-------------------| --- | --- | ----- | ----- |
| Risque            | x 1 | x 1 | x 0.8 | x0.25 |

| 😷 Type de masque      | tissu  | chirurgical | FFP2   |
|------------------------|--------|-------------|--------|
| que tu portes          | x 0,66 | x 0,5       | x 0.33 |
| que les autres portent | x 0,33 | x 0,25      | x 0.16 |

- Donc si tu portes un masque FFP2 en étant vacciné⋅e avec 3 doses, tu réduis tes risques par 12 soit 8,3% du risque initial.
- Si les autres font de même, le risque pour chacun⋅e est réduit par 72 soit 1,4% du risque initial.

Information inconnues :
- taux de réduction du risque (RdR) de faire un dépistage systématique en arrivant sur place.

Pour la soirée, nous seront en intérieur, fenêtres ouvertes si la météo le permet.
D'experience, rares sont les personnes masquées.

## Inscription :

Le formulaire d'inscription est ici : https://framaforms.org/pre-inscription-metarelation-du-11-septembre-2022-1660904595
Il nous permettra de mieux connaître tes attentes pour cet événement pour nous adapter au groupe.

Nous espérons vous voir nombreu⋅x⋅ses et que ce format d'événement vous plaira 🙂


