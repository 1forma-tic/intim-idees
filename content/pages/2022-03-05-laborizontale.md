title: Laborizontale
-------------------

# Laborizontale
_**Samedi 5 Mars à 19h30**_

## Objectif :

Organiser une soirée de lancement du collectif sexe-positif de Bordeaux auprès des communautés ouvertes sur la sexualité avec une initiation aux concepts et valeurs du collectif.


## Comment ?

Introduire la soirée avec une cérémonie contenant des exercices de consentement et de connections.
Organiser une soirée festive ouverte à la sexualité avec piste de danse et coin câlins.
Proposer en parallèle, dans une pièce à part, des ateliers autour de la sexualité.


## Prérequis :

Être à l’aise à assister à de la sexualité de groupe.

Être prêt à être remis en question vis à vis de ses comportements systémiques (conditionnements, dynamique et injonctions sociales, par exemple : privilèges, genre, féminisme, handicap, LGBTQIA+...).
S’inscrire en duo ou trio coresponsable de leurs agissements.


*Des cultures ayant un rapport au consentement différent vont se côtoyer durant la soirée.
Une forme explicite leur sera proposé, pour autant ayez conscience que les habitudes ne changent pas en un jour.*

## Préférences :

Avoir de l’expérience hors des normes de sexualité scriptée (moldus) : Libertinage, non-monogamie éthique, BDSM, Sexpo, …



## Présentation de la soirée sur les réseaux :

Accrochez vos ceintures, ça va être un peu long mais c'est vraiment important pour nous que vous alliez jusqu'au bout de la description pour que vous soyez bien au-clair sur ce qu'on propose !


### 💡 Qu'est-ce que le collectif « Intim’idées » 💡

**Intim'idées est un collectif favorisant le développement de pratiques relationnelles permettant d'expérimenter les libertés d'être et de faire, en prenant le soin d'offrir la sécurité nécessaire à ces explorations.**


Nous prônons et organisons des événements permettant l'émancipation et l'empuissancement individuel et collectif avec humanité et adogmatisme.

Plus spécifiquement :
- Aider la prise de conscience des conditionnements, privilèges, normes, injonctions et oppressions auxquelles nous sommes toustes, plus ou moins, exposé⋅e⋅s et que nous avons intériorisé ;
- Accompagner des réflexions, des déconstructions en ayant un esprit critique et restauratif ;
- Proposer des outils via le prisme de la sexualité (sex-positif) dans un esprit d'amusement, d'inclusivité, d'auto-responsabilité, d'entraide et de liberté.


### ❓ Qu'est-ce qu'une « Laborizontale » ❓

Une « Laborizontale » est une soirée mêlant musique, liberté sexuelle et ateliers sex’périmentaux, dans un esprit sexe-positif et festif à l’inclusivité tournée vers les personnes déjà à l’aise avec ce qui précède.


### ⭐️ Les intentions de cet évènement ⭐️

🔞 Cette soirée est avant tout destinée aux personnes habituées à des milieux ouverts à la diversité des formes de sexualité (Libertinage, non-monogamie éthique, BDSM, Sexpo, Burns, …)
et qui sont intéressées par découvrir le cadre et l’approche que propose le mouvement sexe-positif, à la sauce Bordelaise.
Cet évènement est donc une soirée découverte et d’initiation au milieu sexe-positif adapté à cette population et pour 40 personnes maximum.

💡 Lors de cette soirée, nous chercherons à :
- vous faire percevoir, découvrir, expérimenter et pratiquer la sexualité autrement ;
- favoriser les prises de conscience et la déconstruction des attitudes et comportements normatifs, empreints de sexisme et d'éducation genrée pour réinventer d'autres façons de faire ;
- encourager la connexion à ses émotions, ses ressentis, favoriser l’écoute de soi, l’authenticité, l’alignement ;
- inventer et incarner une manière inclusive de vivre ensemble dans la richesse de nos différences ;
- faciliter les rencontres et les échanges entre participant⋅e⋅s ;
- s’amuser et explorer grâce à un cadre sécurisant (mais qui ne le sera jamais à 100%).

Bien évidemment, toutes les activités proposées ne seront que des invitations et absolument rien ne sera obligatoire (si ce n'est de respecter le cadre posé et sa présentation).


### 🌞 Le déroulement 🌞

🏡 Cette soirée aura lieu dans une maison à Bègles, à 300 mètre d'un arrêt du tram C, le samedi 5 mars, de 19h30 à 6h. Vous pourrez bien sûr partir de la soirée dès que vous le souhaitez.

⏱ Nous vous accueillerons entre 19h30 et 20h30 (il y a un vestiaire sur place) avec un apéro soft, puis à 20h30, nous fermons les portes afin de lancer la soirée. Nous choisissons de ne pas accepter les personnes après 20h30 car nous avons à cœur que tout le monde assiste à la présentation du cadre.

🔘 Un cercle d'ouverture (obligatoire pour participer à la soirée) permettra à tout le monde de se présenter et ensuite, nous vous proposerons des jeux pour mettre en pratique les règles de consentement puis des jeux de connexion et déconstruction pour aller à la rencontre de soi-même et des autres. Cela devrait durer un peu plus d’une heure.

🎶 Après vous aurez la liberté d’utiliser les espaces suivant à votre guise : Salon, piste de danse (musiques latines et électro en alternance), bar, salle d’eau, espace câlins, espace fumeur (en exterieur).
En parallèle, dans l’espace guidé, des ateliers autour de la sexualité vous seront proposé en quasi-continu, mais avec des créneaux fixes et pouvant être limités dans la capacité d’accueil.

🦄 Il n'y aura pas de dress code lors de cette soirée, mais nous vous invitons à profiter de cet événement pour vous habiller dans un esprit délire et/ou sexy, à savoir :
Soyez imaginati⋅f⋅ve, décadent⋅e fun, élégant.e, fetish, extravagant⋅e, travesti⋅e, déguisé⋅e, paillettes et amour, voire tout simplement en sous-vêtements ou nu.e.

🍾 Nous vous suggérons d'avoir mangé avant de venir, d’apporter des boissons (plutôt soft, pour un consentement de meilleur qualité, mais amenez ce que vous avez envie de consommer) et une serviette (si vous avez l'intention d'aller à la douche ou dans une chambre).

🍬 De quoi grignoter sera disponible tout au long de la soirée (mais pas de quoi faire un repas).


### 📜 Le cadre 📜

Le cadre désigne explicitement ce qui est encouragé, ce qui prime et ce qui est proscrit lors de cet évènement. Il vise à créer un espace propice à l’exploration, à l’expression de soi, au jeu, à la prise de conscience mais aussi un espace sécurisant.

✔ Consentement
- Il est attendu de pratiquer le consentement enthousiaste, spécifique, réversible, informé, libre et éclairé.
- Si vous pensez oui, dites oui ; si vous pensez non, dites non. (Personne ne vous en voudra)
- Si vous pensez peut-être, alors c'est un NON (au moins pour le moment)
- Vous pouvez changer d’avis autant de fois que vous le souhaitez. Vraiment.
- Si vous recevez un non, l’invitation est de remercier la personne, qui a écouté son propre consentement.
- Il est fortement recommandé d’utiliser les outils qui vous seront proposés lors du cercle d’ouverture.
- Il sera demandé une vigilance collective vis-à-vis des biais de consentement liés aux relations de pouvoir réelles ou ressenties, notamment âgé/ jeune, homme/femme, etc.

🏳️‍🌈 Je n'émet aucun jugement ni ne présume du genre, de l'orientation, des pratiques et du physique d'autrui.

📸 Aucune photo/ vidéo n'est autorisée pendant la soirée, sauf atelier dédié.

🕺 Toutes les activités proposées sont des invitations qu’il est possible de refuser et de quitter à tout moment.

🍾 La consommation de tous produits (alcool et drogues) est découragée (mais pas interdite) car elle peut altérer l’état de conscience et la capacité à donner son consentement de manière éclairée. Pour permettre aux autres de consentir ou non à interagir avec des personnes ayant un état de conscience altéré, un signe distinctif vous sera fourni.

🩹 Cet évènement n'a pas de visée thérapeutique, l’expression de soi, le soutien mutuel et le partage ne sont pas proposés dans un but de soigner mais plutôt d'expérimentation de faire autrement. Cela n'exclu pour autant pas des effets thérapeutiques occasionnels.

💙 Des personnes se rendront disponibles en tant que soutien émotionnel pour recevoir la parole et les émotions d’autres participant.e.s et seront identifiées par un symbole qui signifie que donner du soutien prime sur ne pas être interrompu.
Un lieu calme est prévu pour recevoir du soutien si nécessaire.

🗑 Respectez les règles de chaque espace (interdiction de fumer à l'intérieur, priorisations des espaces, etc...)
Des poubelles ainsi que du matériel de nettoyage seront à disposition.

🙋‍♀️ Je questionne tout comportement qui me semble inapproprié.


### 🖖 Vous êtes bienvenu.e.s si... 🖖

🏳️‍🌈 Vous partagez les intentions de cet évènement quel que soit vos sexes, genres, orientations sexuelles et romantiques, atypies, morphologies, ethnicités, nationalités, âges, situations de handicap, croyances, niveaux d’éducation, revenus, …

📝 Vous vous engagez à respecter les règles de l'évènement et à faire de votre mieux pour incarner la culture que nous vous proposons, celle de l'écoute de soi et de ses émotions, celle de l'ouverture aux autres, celle du consentement explicite et enthousiaste, celle du non-jugement, de la liberté et de la bienveillance.

Il est possible de ne pas réussir à intégrer tous les codes dès la première heure
cependant, si cela nous met en difficulté pour maintenir la qualité d'expérience que nous souhaitons offrir à vivre aux participant⋅e⋅s, nous pourrons décider d'exclure les personnes dont l'acculturation nous demande une énergie préjudiciable au reste du groupe. Dans ce cas, nous leur indiquerons les préalables qui nous sont nécessaire pour les réintégrer lors de prochains évènements.

Pour que l'expérience offre bien-être et inclusion au maximum de participant⋅e⋅s, nous vous invitons collectivement à faire preuve de vigilance aux comportements, schémas et scripts systémiques que nous pouvons toutes et tous avoir. En effet, si nous nous montrons toustes co-responsable de vivre l'experience que nous souhaitons nous offrir, le collectif sera bien plus résiliant et capable d'inclure que si cela repose seulement sur l'équipe d'orga.


### 👯‍♀️ Vous venez en duo ou trio mutuellement responsable 👯‍♀️
Nous nous inspirons du [système PAL (Pervy Action Liaison) des salons kinky](https://www.kinkysalon.fr/le-systeme-pal/)
pour ouvrir cet évènement au-delà des personnes que nous connaissons déjà,
sans trop sacrifier la confiance dans la qualité du respect et de l’éthique entre participant⋅e⋅s.
Concrètement, cela veux dire que pour participer vous devez vous inscrire avec minimum une et maximum deux autres personnes (groupe PAL)
qui vous font confiance et en qui vous avez confiance pour bien se comporter lors de l'évènement.
Si nous avons à prendre une décision à l'égard d'une des personnes, elle s'appliquera aussi aux autres membres du groupe PAL.

Nous n’utiliserons pas ce système sur chacun de nos évènements. S’il ne vous conviens pas, attendez les prochains (et contactez-nous pour nous dire en quoi cela ne vous conviens pas).
Enfin, ce système complète mais ne remplace pas le processus de sélections des participant⋅e⋅s pour constituer un groupe aussi harmonieux et épanouissant que possible.


### 😷 Information COVID 😷

Nous tenons à rappeler que le virus est toujours parmi nous et que venir à cet événement comporte des risques, d’autant plus que le cadre ne permettra pas le respect des gestes barrières.
Avant la tenue de la soirée :
- 💉 Nous vous recommandons d’avoir un schéma vaccinal complet, mais nous ne l'exigeons pas.
- 🌡 Il est également demandé de ne pas venir en cas de symptôme ou de cas contact.
- 📞 Il est aussi demandé de prévenir l'orga si des symptômes apparaissaient dans les 7 jours après l'événement afin que chaque participant.e puisse se faire tester et s'isoler si nécessaire.
- 🧪 **Enfin, étant donné la situation sanitaire actuelle, en préalable à votre venue, que vous soyez vacciné⋅e ou non, nous vous demanderons de nous fournir le resultat d'un test antigénique négatif à votre nom, datant de moins de 24h avant le début de l'évènement.**


### ☔️ Risques IST/MST et Safer Sex ☔️

🔬 Nous recommandons d’avoir fait, moins de 3 mois avant l'évènement, un test IST/MST pour pouvoir communiquer ouvertement avec des partenaires potentiel.le.s sur un statut à jour.

☂️ Nous invitons les participant.e.s à se protéger autant que possible grâce à votre matériel de prophylaxie ou celui disponible sur place (préservatifs, gants, ...), tout en questionnant le niveau de risque auquel chacun.e est prêt à s'exposer.


### 👥Qui animera la soirée ? 👥

Sam, secondé par Millicent seront les animataires principaux de la soirée.
Nous complèterons la liste au fur et à mesure.

Les principaux actaires du collectif intim'idées sont actuellement : Céliane, Loïc, Millicent et Sam.


### 💶 Et combien ça coûte ? 💶

La participation aux frais (PaF) permet de payer la nourriture et le matériel (matelas, son, décoration, etc.). Vos contibutions audela couvrent l’hébergement, ainsi que tout le travail d’organisation de l’évènement, voir peuvent faciliter de futures évènements.

En gardant en mémoire qu’il y a plus de pauvre que de riche, voici pour cette « Laborizontale » les prix suggérés par personne :
- PaF solidaire : 1 à 20 €
- PaF équilibre : 20 à 40 €
- Contribution soutien : 40 à 80 €
- Contribution justice sociale & soutien des futurs évènements : 80 à 160 € (ou plus)
  Le prix que vous pouvez mettre ne fait pas partie des critères de sélections, tant que ça ne met pas collectivement en péril la viabilité de l’évènement.


### ✉️ Comment je m’inscris ? ✉️

En répondant à ce formulaire de pré-inscription :

https://framaforms.org/pre-inscription-laborizontale-du-5-mars-2022-1644130835

Si vous rencontrez des difficultés pour remplir ce questionnaire, n’hésitez pas à nous contacter : contact@intim-idees.fr ou par sms : 0 770 772 770.

**Chronologie d'inscription**

1. **5 février :** ouverture des préinscriptions
2. **21 février à 14h :** début de la sélection des participant⋅e⋅s et émission des demande de paiement pour validation d'inscription
3. **27 février à 14h :** clôture des pré-inscription
4. **28 février à 14h :** date limite de paiement (et invitation des personnes en liste d'attente le cas échéant)
5. **1er mars :** email récapitulatif (info pratiques, lieu exact notamment)
6. **5 mars avant 19h30 :** vérification des tests antigénique
7. **5 mars à 19h30 :** accueil des participant⋅e⋅s.
8. **5 mars à 20h30 :** fermeture des portes et début du cercle d'ouverture.


Il est possible que vous ne soyez pas retenu⋅e pour participer cette fois-ci car nous choisirons les personnes afin de favoriser un équilibre et une cohérence entre les personnes présentes lors de la soirée.


### ❓ Des questions ❓

Vous pouvez bien évidemment nous contacter ! Notre préférence : par email (contact@intim-idees.fr), ou sur notre page Facebook « Intim’idées » ou sur nos autres réseaux sociaux.
Merci d'avoir lu toutes ces informations nécessaires et à très bientôt !

La team'idées ❤
