### Exercice de qualité d'écoute
5 minutes : théorie et cadre de la pratique
2x10 minutes : pratique à tour de rôle en duo
4 minutes : vos impressions par groupe de 4
1 minute : intention pour la journée

1. la théorie : différentier les posutres
  - **écoute attentionnée "curiosité sincère"** avec l'intention de rejoindre l'autre pour comprendre où iel se trouve
  - **écoute réactive ou défensive** "tout ce que tu dit sera retenu contre toi" avec l'intention de répondre pour se justifier si on se sent attaqué, ou pour mettre l'autre en défaut si on cherche à avoir raison. **Attention, piège** : l'envie compulsive de conseiller est une forme de réaction (**écoute orienté solution**, qui n'est pas toujours bienvenue).
  - l'**incapassité d'écoute** "un mot de plus et j'explose (ou j'implose)"

  - écoute indiférente "j'ai mieux à faire, mais cause toujours" avec une ou des intentions sans liens avec ce que dit l'autre
2. l'objectif : apprendre à **distinguer** où vous en êtes, et à **revenir en écoute attentionnée**.
3. **Le jeu** : pour l'une des personnes s'exprimer sur quel est **LA meilleur solution pour sauver la planète** (choisissez une approche ciblée qui vous semble prioritaire et développez-la, pas une approche flou qui face consensus) et pour l'autre, d'essayer de rester en curiosité sincère, et de s'observer en sortir, interompre d'un **ciseau gestuel**, essayer d'identifier ce qui vous as affecté, puis tenter de revenir en curiosité sincère (**demander du soutien** émotionnel si vous n'y arrivez pas), et reprendre l'écoute... Si vous senter l'explosion venir, vous êtes probablement déjà loin de l'écoute attentionnée. Demandez du soutien !
4. l'**intension pour la journée** : garder cette vigilence de posture ou qualité de présence à vous et à l'autre, et demander des pauses avant d'être à bout, mais plutôt dès que vous sentez que vous passez en posture réactive/défensive.
