title: Jeux'thèmes
-------------------
![bannière d'illustration](img/bannieres/jeux-themes.jpg)

# Soirée jeux'thèmes
_**jeudi 02 Juin à partir de 19h**_

## Objectif :

Faire réfléchir de manière ludique et pratique sur des thèmes qui nous tiennent à cœur chez intim'idées :
Pour cette édition, l'esprit critique.
Puis au travers de ces expériences ludique, mener à des prises de consciences et des changements de comportements.

## Présentation de la soirée :

Salut à toustes,

Nous voici de retour pour jeux'thèmes, 3ème édition !

Le concept est toujours le même :
Après un temps d'auberge espagnole, une soirée légère¹ et ludique
avec des jeux choisis pour explorer certaines thématiques.

¹ légère dans notre référentiel avec son organisation simplifiée et son format court.
Ceci dit, les thèmes abordés ne sont pas forcément léger
et les remises en question qui peuvent en découler non-plus.
C'est pourquoi, comme à chaque événement intim'idées, du soutien émotionnel est à disposition
pour accueillir et traverser les vécus inconfortables dans de bonnes conditions.
C'est un filet de sécurité que nous proposons
pour aider à vivre le moment avec authenticité, en osant être vulnérable
et sortir de sa zone de confort.

### 🎲 Jeux & animations 🎲
Pour cette édition, c'est **l'esprit critique** qui est à l'honneur, animé par Sam et David,
de 20h à 23h.

Le jeu principal est : [Éleusis](https://fr.wikipedia.org/wiki/%C3%89leusis_(jeu))
quelques animation complémentaire à base notament d'effet d'optique serons proposé et peutêtre le jeu Argumentum.

### ⏱ Horaires et déroulé ⏱
- 19h : **Auberge espagnole** (sans alcool)
  où chacun.e apporte quelque chose (pour un menu varié,
  [voici un pad pour vous auto-organiser](https://pad.aquilenet.fr/p/intim-idees-auberge-espagnole-YLYrkil).
- 20h : Mini présentation et début des animations.
- selon les jeux, l'ambiance et la fatigue des animataires, fin des animations entre 22h et minuit pour continuer en auto-gestion.
- fermeture du lieu entre 23h et 2h du matin, selon l'ambiance et la fatigue des animataires.

### 💶 Et combien ça coûte ? 💶

Pour qu'organiser, animer et héberger cet événement et les suivants reste joyeux et enthousiaste,
nous avons besoin de soutiens et d'encouragements.
Nous serions heureu⋅ses⋅x d'entendre ce que l'événement vous a apporté,
mais aussi d'avoir votre aide pour couvrir nos frais.
À cet effet nous proposons une participation à **prix libre** avec comme grille suggérée :
- PaF solidaire : 0 à 5 €
- PaF équilibre : 6 à 10 €
- Contribution soutien : 11 à 20 €
- Contribution justice sociale & soutien des futurs évènements : 21 € et bien au dela !

À quoi va servir cet argent ? À acheter ou fabriquer de [super jeux](2022-04-21-jeux-themes-compta.md)
à vous faire découvrir dans les prochaines éditions jeux'thèmes ainsi qu'à soutenir le lieu d'accueil et les orgas.
Pour plus d'infos et de transparence, voici la page des finances dédiées à l'événement :
https://intim-idees.fr/2022-06-02-jeux-themes-compta.html

### 😷 Informations COVID 😷

Pour prendre soin des enjeux de santé virale comme mentale et psychologique
ainsi que des enjeux politique de préservation des liberté individuelles, d'auto-détermination et de cohésion sociale,
nous faisons le choix pour cet événement de nous contenter de demander aux personnes malades
de rester se reposer chez elles.

Pour les autres nous vous recommandons d'adapter vos prises de risques en conscience de ce que vous êtes prêt à assumer
qu'il s'agisse d'attraper ou de transmettre à autrui le covid ou quoi que ce soit d'autres d'ailleurs.

Pour avoir conscience des risques covid et identifier les mesures les plus efficaces
pour les réduire, nous vous recommandons le simulateur [microCovid](https://www.microcovid.org/),
logiciel libre réalisé par des passionné⋅e⋅s en s'appuyant sur des chiffres issus de publication scientifique.

En résumé :

| 💉 Dose de vaccin |  0  |  1  |   2   |   3   |
|-------------------| --- | --- | ----- | ----- |
| Risque            | x 1 | x 1 | x 0.8 | x0.25 |

| 😷 Type de masque      | tissu  | chirurgical | FFP2   |
|------------------------|--------|-------------|--------|
| que tu portes          | x 0,66 | x 0,5       | x 0.33 |
| que les autres portent | x 0,33 | x 0,25      | x 0.16 |

- Donc si tu portes un masque FFP2 en étant vacciné⋅e avec 3 doses, tu réduis tes risques par 12 soit 8,3% du risque initial.
- Si les autres font de même, le risque pour chacun⋅e est réduit par 72 soit 1,4% du risque initial.

Information inconnues :
- taux de réduction du risque (RdR) de faire un dépistage systématique en arrivant sur place.

Pour la soirée, nous seront en intérieur, fenêtres ouvertes si la météo le permet.
D'experience, rares sont les personnes masquées.

## Inscription :

Inscris-toi en envoyant par sms au 0 770 772 770 (mobile non surtaxé) :

**&lt;TitreÉvénement&gt; &lt;DateÉvénement&gt; &lt;TonPrénom&gt; &lt;PaF¹&gt; €**<br/>
¹ Participation aux Frais (à quelle hauteur comptes-tu contribuer financièrement)

Exemple : **jeux'themes 02/06 Clem 10 €**<br/>


Nous te répondrons "Inscription OK, paiement en ligne ou sur place ?" ou ta position en liste d'attente.

[Pour plus d'infos sur la liste d'attente, consultez la page détaillant les inscriptions sms.](https://intim-idees.fr/inscription-sms.html)

Nous espérons vous voir nombreu⋅x⋅ses et que ce format d'événement vous plaira 🙂
