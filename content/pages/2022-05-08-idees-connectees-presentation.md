title: Présentation détaillée de la soirée idées'connectées du 8 mai 2022
-------------------
# Présentation détaillée de la soirée idées'connectées du 8 mai 2022

Bienvenue à tous⋅tes dans l’univers du collectif « Intim'idées » !

Nous sommes heureux⋅ses de vous inviter à une nouvelle édition d’ « Idées'connectées » !

Cet évènement est complémentaire à l’évènement « Idées'couvrir » :
https://intim-idees.fr/2022-05-08-idees-couvrir.html


### 🥰 Qu’est-ce qu’une « Idées'connectées » ? 🥰

Une « Idées'connectées » est un évènement conçu pour explorer des espaces de consentement et de connexion consciente aux autres.

L'intention de ce temps est orientée partages, connections... par les mots ou par les corps.
La nudité sera possible seulement si l’ensemble des participant.e.s est conscentant.e.s, la sensualité sera autorisée (câlins, caresses non génitales, massages, …) mais il n'y aura pas de sexualité.


### 📜 Le cadre de l'évènement📜

Si vous souhaitez avoir des informations sur le cadre pour cet évènement, suivez ce lien :
https://intim-idees.fr/2022-04-09-idees-couvrir-cadre.htm


### 🦄 Qui peut participer ?☝️

Cet événement est destiné prioritairement aux personnes ayant déjà fait une « Idées'couvrir », puis aux personnes ayant fait un autre de nos évènements ayant des ateliers de consentement (Soirée Aire’éthique ou Laborizontale).


### Où ?

Cet évènement aura lieu dans une maison à Bègles, à 300 mètres de l’arrêt La Belle Rose du tram C.


### 🕟 Le déroulement 🕟

Entre 19h00 et 20h00 - Accueil
Temps pour te changer si besoin (il y a un vestiaire sur place), grignoter et bavarder hors cadre. Tu es également invité.e à amener ce que tu souhaites manger ou boire (sans alcool). De quoi grignoter sera disponible tout au long de la soirée mais pas de quoi faire un repas.

20h00 – Fermeture des portes
Nous choisissons de ne pas accepter les personnes après 20h00 car nous avons à cœur que tout le monde assiste au rappel du cadre, donc pour les retardataires chroniques retenez 19h00, pas 20h00.

20h05 – Cercle d’ouverture
Un cercle d’ouverture permettra à tout le monde de se présenter et d'entrer doucement dans l’évènement. Ce sera aussi l'occasion pour nous de répondre aux questions. Nous présenterons à ce moment-là le cadre et les propositions de l’évènement.

21h15 - Ateliers
Ce sera un temps d’exploration libre en autogestion via notamment des ateliers proposés (mais sans sexualité) aussi bien par les organisataires que les participant.e.s (mais aucun ne sera obligatoire).
Par exemple, nous pourrions proposer certains de ces ateliers :
- Explorer de façon plus approfondie des ateliers fait aux « Idées’couvrir » : roue du consentement, exercices de consentement non-verbal, exercices de consentement verbal, …
- Adoration
- Succubes
- Kinks-mouvant
- Playfight
- Morsures
- Electro-play

00h00 - Cercle de clôture
Ce temps permettra de partager les différentes expériences et prendre le temps d'explorer ce qui est vivant en nous et ce que l'on désire apprendre de ça.
Tu pourras bien sûr quitter l’évènement dès que tu le souhaites.


### 👥 Qui animera l'évènement ? 👥

Millicent, Sam et peut-être Céliane seront les animataires principales de l’événement.
Les principaux actaires du collectif intim’idées sont actuellement : Céliane, Loïc, Millicent, Myrha et Sam.


### 💸 Participation financière 💸

La participation aux frais (PaF) permet de payer la nourriture et de couvrir le prix du matériel (matelas, son, décoration, etc.). Vos contributions au-delà couvrent l’hébergement et son entretien, ainsi que tout le travail d’organisation de l’évènement, voire peuvent faciliter de futurs évènements et, dans la mesure du possible, de rémunérer les organisataires de l'événement.

Le prix est libre, et nous proposons plusieurs fourchettes de prix pour vous guider :
- PaF solidaire : 1 à 10 €
- PaF équilibre : 10 à 20 €
- Contribution soutien : 20 à 40 €
- Contribution justice sociale & soutien des futurs évènements : 40 à 80 € (ou plus)

Le prix que vous pouvez mettre ne fait pas partie des critères de sélections, tant que ça ne met pas collectivement en péril la viabilité de l’évènement.

La transparence sur le budget de l'événement sera disponible sur le site Intim'idées dans les jours qui suivent l'événement.


### 😷 Information COVID 😷

Nous tenons à rappeler que le virus est toujours parmi nous et que venir à cet événement comporte des risques, d’autant plus que le cadre ne permettra pas le respect des gestes barrières.
Le port du masque ne sera pas obligatoire.

Avant la tenue de l’évènement :
- Il est demandé de ne pas venir en cas de symptôme ou de cas contact.
- Nous vous demanderons de nous fournir le résultat d’un test antigénique négatif à votre nom, datant de moins de 24h avant le début de l’évènement ou de venir avec un autotest à faire sur place.

Après :
- Merci de prévenir l’orga si des symptômes apparaissaient dans les 7 jours après l’événement afin que chaque participant⋅e puisse se faire tester et s’isoler si nécessaire.


### ✉️ Comment je m’inscris ? ✉️

En répondant à ce formulaire de pré-inscription :
https://framaforms.org/pre-inscription-ideesconnectees-du-8-mai-2022-1651150479

Si vous rencontrez des difficultés pour remplir ce questionnaire, n’hésitez pas à nous contacter :
contact@intim-idees.fr ou par sms : 0 770 772 770

PS : nous aurons beaucoup à faire durant les 48 heures précédant l’événement.
Il n’est pas sûr que nous ayons le temps d’étudier ta pré-inscription si tu nous l’envoi seulement en dernière minute.
On fera au mieux.

Il est possible que tu ne sois pas retenu⋅e pour participer cette fois-ci car :
- Le groupe sera constitué de 30 personnes maximum, pour fournir la qualité de cadre et d’animation à laquelle nous tenons.
- Nous choisirons les personnes afin de favoriser un équilibre et une cohérence entre les personnes présentes lors de l’événement, avec une vigilance à l’inclusivité des personnes appartenant à des groupes discriminés.
  L'âge n’est pas un critère de sélection mais il est à noter que l’âge moyen des participant.e.s se situe entre 20 et 45 ans (si cette information est importante pour toi.)


### ❓ Des questions ❓

Tu peux bien évidemment nous contacter ! Notre préférence : par email (contact@intim-idees.fr), sur notre page Facebook « Intim’idées » ou sur nos autres réseaux sociaux.

Merci d’avoir lu toutes ces informations nécessaires et à très bientôt !

La team’idées ❤


PS : Une partie de ce texte est inspirée de ce que proposent "La Bulle", "Love Experience", "La Licornerie " et "Les Félinades", n'hésitez pas à regarder ce qu'iels proposent !





