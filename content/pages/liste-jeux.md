title: Liste des jeux pour les soirées jeux'thèmes
-------------------
# Liste des jeux pour les soirées jeux'thèmes

| Nom du jeu                  | Thèmes                               | Description                                                                                                                                                                                                                                                                                                                                      | Propriétaire         | Lieu de stockage           |
|-----------------------------|--------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------|----------------------------|
| Jeu me connecte             | émotion, authenticité, CNV           | [🛒](https://apprentie-girafe.com/produit/coffret-connecte/ "boutique") ~ Réalisé par l'[apprentie girafe](https://apprentie-girafe.com/) : rencontre tes émotions et apprivoise tes besoins ! [Lire les règles (pdf)](https://apprentie-girafe.com/wp-content/uploads/2021/06/Regle-jeu-me-connecte-apprentie-girafe.pdf)                       | Céliane              | Laborizon                  |
| On veut du vrai             | émotion, authenticité                | [🛒](https://web.archive.org/web/20160409100200/http://onveutduvrai.com/jeu.html) ~ à partir d’un jeu de cartes, oser se dire, oser parler de soi, écouter dans le respect, et découvrir à quel(s) point(s) on se rejoint. [DiY](https://englishfrench.ca/activites/on-veut-du-vrai/)                                                            | Nathalie & Dieudonné | Laborizon ou La soleillade |
| We are not really strangers | émotion, authenticité                | [🛒](https://www.werenotreallystrangers.com/collections/games) We're Not Really Strangers is a purpose driven card game and movement all about empowering meaningful connections. Three carefully crafted levels of questions and wildcards that allow you to deepen your existing relationships and create new ones.                            | Doriane              | Laborizon                  |
| Faut qu'on parle            | émotion, authenticité                | [🛒](https://doerswave.com/products/il-faut-quon-parle) Jeu de questions, dont certaines sont à trier/modifier pour éviter ses aspects normatifs. [Règles du jeu](https://doerswave.com/pages/regles-il-faut-quon-parle)                                                                                                                         | Clara                | Laborizon                  |
| Ask*hole                    | tabous, dilemmes moraux, philosophie | [🛒](https://www.askhole.io/) That's a nice social boundary you've got there. Would be a shame if something… happened to it. Filled with inappropriate, deeply controversial, philosophical, and vulnerable inquiries, this deck of unusual questions will keep your night full of nervous laughter and your relationships somewhat distressing. | Loïc                 | Laborizon                  |
| Ask*hole FR                 | tabous, dilemmes moraux, philosophie | [DiY](https://lite.framacalc.org/9qqn-yc6won6i1a) Un concentré de questions gênantes, aux réponses souvent difficiles à assumer.                                                                                                                                                                                                                 | ?                    | Laborizon                  |


- [Moi c'est Madame](https://www.jeux-cooperatifs.com/shop/moi-cest-madame/) pour lutter contre le sexisme ordinaire.
- [Bloc by Bloc](https://outofordergames.com/blocbybloc/) un jeu pour s'essayer à contrer efficacement les dérives autoritaires de nos sociétés. Pour en savoir plus : [un article francophone qui en parle](https://evasions.blackblogs.org/2018/08/07/linsurrection-nest-pas-un-jeu/).
- [Regards sur le porno](https://www.jeux-cooperatifs.com/shop/regards-sur-le-porno/) pour en parler sans se juger et pouvoir oser en faire un vrai sujet.
- [Consentement, t’en dis quoi ?](https://www.jeux-cooperatifs.com/shop/consentement-ten-dis-quoi/) pour questionner notre rapport au consentement sur le plan interpersonnel, mais aussi socioculturel.
- [Mon nom est Clitoris](https://www.jeux-cooperatifs.com/shop/mon-nom-est-clitoris/) pour parler sexualité et plaisir.
- [Européennes en Quête de Droits](https://www.jeux-cooperatifs.com/shop/europeennes-en-quete-de-droits/) pour explorer l'état des inégalités sociale et patriarcales.



## Jeux mis à disposition par Laborizon

## Jeux prêtés à intim'idées

- [Laborizon]
