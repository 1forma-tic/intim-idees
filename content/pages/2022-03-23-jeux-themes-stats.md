title: Stats Jeux'thèmes du 23 Mars 2022
-------------------
# Stats Jeux'thèmes du 23 Mars 2022

Actualisé le : 04/04/2022

## Nombre de personnes
- Inscriptions reçues : 29
- Présent⋅e⋅s : 22 + 5 orga.
- Désistements : 7 (dont 3 covid)
- Reste en liste d'attente : 0

## Finances :
- Participations à prix libre reçues : 28,90 €
- Soit 1,3 € par personne en moyenne.

### Communication autour du prix libre et mode de paiement :
- Prix libre annoncé dans la description de l'événement
- Une urne laissée à disposition pour collecter les participations.
- Présentation de l'usage prévu du paiement et de la présence de l'urne durant la présentation à 20h.
